CREATE TABLE IF NOT EXISTS marca (
	idMarca	INTEGER PRIMARY KEY auto_increment,
	nombreMarca	varchar(20) NOT NULL UNIQUE,
	fechaModificadaMarca	varchar(10) NOT NULL
);
INSERT INTO `marca` (`idMarca`,`nombreMarca`,`fechaModificadaMarca`) VALUES (1,'DELL','2019-06-02');
INSERT INTO `marca` (`idMarca`,`nombreMarca`,`fechaModificadaMarca`) VALUES (2,'HP','2019-06-02');
INSERT INTO `marca` (`idMarca`,`nombreMarca`,`fechaModificadaMarca`) VALUES (3,'TOSHIBA','2019-06-02');