CREATE TABLE IF NOT EXISTS marca (
	idMarca	INTEGER PRIMARY KEY auto_increment,
	nombreMarca	varchar(20) NOT NULL UNIQUE,
	fechaModificadaMarca	timestamp NOT NULL
);
INSERT INTO `marca` (`idMarca`,`nombreMarca`,`fechaModificadaMarca`) VALUES (1,'DELL','2019-06-02 18:14:30');
INSERT INTO `marca` (`idMarca`,`nombreMarca`,`fechaModificadaMarca`) VALUES (2,'HP','2019-06-02 18:14:32');
INSERT INTO `marca` (`idMarca`,`nombreMarca`,`fechaModificadaMarca`) VALUES (3,'TOSHIBA','2019-06-02 18:14:35');

CREATE TABLE IF NOT EXISTS tipoequipo (
    idTipoEquipo	INTEGER PRIMARY KEY auto_increment,
    nombreTipoEquipo	VARCHAR(20) NOT NULL UNIQUE,
    fechaModificadaTipoEquipo	timestamp NOT NULL
    );

INSERT INTO `tipoequipo` (`idTipoEquipo`,`nombreTipoEquipo`,`fechaModificadaTipoEquipo`) VALUES (1,'LAPTOP','2019-06-02 18:14:30');
INSERT INTO `tipoequipo` (`idTipoEquipo`,`nombreTipoEquipo`,`fechaModificadaTipoEquipo`) VALUES (2,'CANON','2019-06-02 18:14:35');


CREATE TABLE IF NOT EXISTS idioma (
	codIdioma	INTEGER PRIMARY KEY auto_increment,
	nombreIdioma	varchar(20) NOT NULL UNIQUE,
	fechaModificadaIdioma	timestamp NOT NULL
);
INSERT INTO `idioma` (`codIdioma`,`nombreIdioma`,`fechaModificadaIdioma`) VALUES (1,'ESPANOL','2019-06-02 18:14:30');
INSERT INTO `idioma` (`codIdioma`,`nombreIdioma`,`fechaModificadaIdioma`) VALUES (2,'INGLES','2019-06-02 18:14:32');
INSERT INTO `idioma` (`codIdioma`,`nombreIdioma`,`fechaModificadaIdioma`) VALUES (3,'FRANCES','2019-06-02 18:14:35');


CREATE TABLE IF NOT EXISTS tipodocumento (
    idTipoDocumento	INTEGER PRIMARY KEY auto_increment,
    nombreTipoDocumento	VARCHAR(20) NOT NULL UNIQUE,
    fechaModificadaTipoDocumento	timestamp NOT NULL
    );

INSERT INTO `tipodocumento` (`idTipoDocumento`,`nombreTipoDocumento`,`fechaModificadaTipoDocumento`) VALUES (1,'TESIS','2019-06-02 18:14:30');
INSERT INTO `tipodocumento` (`idTipoDocumento`,`nombreTipoDocumento`,`fechaModificadaTipoDocumento`) VALUES (2,'LIBRO','2019-06-02 18:14:35');


CREATE TABLE editorial (
	idEditorial	INTEGER PRIMARY KEY auto_increment,
	nombreEditorial	varchar(20) NOT NULL UNIQUE,
    fechaModificadaEditorial	timestamp NOT NULL
);

INSERT INTO `editorial` (`idEditorial`,`nombreEditorial`,`fechaModificadaEditorial`) VALUES (1,'ACANTILADO','2019-06-02 18:14:30');
INSERT INTO `editorial` (`idEditorial`,`nombreEditorial`,`fechaModificadaEditorial`) VALUES (2,'ALBA','2019-06-02 18:14:35');