package edu.ues.fia.inventarioues.Vistas.Documento;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.R;

public class DocumentoEditarActivity extends AppCompatActivity {

    EditText editBusca;

    EditText editIdDocumento,editIsbnDocumento,editNombreDocumento,editEdicionDocumento,editFechaPublicacion;
    Spinner spTipoDocumento,spEditorial,spIdioma;
    Button btnBuscar;
    Calendar calendar=Calendar.getInstance();
    String clave=null;
    Button btAutor;
    Integer valorSelectTipoDocumento, valorSelectEditorial,valorSelectIdioma ,valorSelectOpcion=null,opcion;// prestado

    //testing
    final List<Integer> editorialIdList = new ArrayList<>();
    final List<Integer> tipoDocumentoIdList = new ArrayList<>();
    final List<Integer> idiomaIdList = new ArrayList<>();


    TextView txtAutores;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_editar);
        TablasV helper= TablasV.getInstance(getApplicationContext());

        editIdDocumento =findViewById(R.id.DocumentoEditarIdDocumento);
        editIsbnDocumento = findViewById(R.id.DocumentoEditarIsbnDocumento);
        editNombreDocumento = findViewById(R.id.DocumentoEditarNombre);
        editEdicionDocumento = findViewById(R.id.DocumentoEditarEdicionDocumento);
        editFechaPublicacion = findViewById(R.id.DocumentoEditarPublicacionDocumento);
        txtAutores=findViewById(R.id.DocumentoEditarAutores);
        btAutor=findViewById(R.id.DocumentoEditarBtnInsertAutores);


        spTipoDocumento = findViewById(R.id.DocumentoEditarIdTipoDocumento);
        spEditorial = findViewById(R.id.DocumentoEditarIdEditorial);
        spIdioma = findViewById(R.id.DocumentoEditarCodIdioma);


        editBusca = findViewById(R.id.DocumentoEditarSearch);
        btnBuscar =findViewById(R.id.DocumentoEditarBtnConsultar);

        Bundle bundle=getIntent().getExtras();
        Cursor tipoDocumento = helper.obtenerTipoDocumentos();
        //final List<Integer> tipoDocumentoIdList = new ArrayList<>();
        tipoDocumentoIdList.add(0);
        List<String> tipoDocumentoNombreList = new ArrayList<>();

        tipoDocumentoNombreList.add(getResources().getString(R.string.documentoCampo3s));

        while (tipoDocumento.moveToNext()) {
            tipoDocumentoIdList.add(tipoDocumento.getInt(0));
            tipoDocumentoNombreList.add(tipoDocumento.getString(1));
        }
        spTipoDocumento.setAdapter(new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, tipoDocumentoIdList));
        spTipoDocumento.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, tipoDocumentoNombreList));
        spTipoDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    valorSelectTipoDocumento = null;
                } else {

                    valorSelectTipoDocumento = tipoDocumentoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectTipoDocumento = null;

            }
        });

        Cursor editorial = helper.obtenerEditoriales();
        // final List<Integer> editorialIdList = new ArrayList<>();
        editorialIdList.add(0);
        List<String> editorialNombreList = new ArrayList<>();
        editorialNombreList.add(getResources().getString(R.string.documentoCampo4s));
        while (editorial.moveToNext()) {
            editorialIdList.add(editorial.getInt(0));
            editorialNombreList.add(editorial.getString(1));
        }
        spEditorial.setAdapter(new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, editorialIdList));
        spEditorial.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, editorialNombreList));
        spEditorial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    valorSelectEditorial = null;
                } else {
                    valorSelectEditorial= editorialIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectEditorial = null;

            }
        });
        Cursor idioma = helper.obtenerIdiomas();
        //final List<Integer> idiomaIdList = new ArrayList<>();
        idiomaIdList.add(0);
        List<String> idiomaNombreList = new ArrayList<>();
        idiomaNombreList.add(getResources().getString(R.string.documentoCampo5s));
        while (idioma.moveToNext()) {
            idiomaIdList.add(idioma.getInt(0));
            idiomaNombreList.add(idioma.getString(1));
        }
        spIdioma.setAdapter(new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, idiomaIdList));
        spIdioma.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, idiomaNombreList));
        spIdioma.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    valorSelectIdioma = null;
                } else {
                    valorSelectIdioma= idiomaIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectIdioma = null;

            }
        });


        
        if (bundle==null){
            editIdDocumento.setEnabled(false);
            editIsbnDocumento.setEnabled(false);
            spTipoDocumento.setEnabled(false);
            spEditorial.setEnabled(false);
            spIdioma.setEnabled(false);
            editNombreDocumento.setEnabled(false);
            editEdicionDocumento.setEnabled(false);
            editFechaPublicacion.setEnabled(false);
            opcion=0;
            txtAutores.setEnabled(false);
            btAutor.setEnabled(false);

        }else{
            editBusca.setVisibility(View.INVISIBLE);
            btnBuscar.setVisibility(View.INVISIBLE);
            opcion=1;

            editIdDocumento.setText(bundle.getString("idDocumento"));
            editIsbnDocumento.setText(bundle.getString("isbn"));
            editNombreDocumento.setText(bundle.getString("nombre"));
            editEdicionDocumento.setText(bundle.getString("edicion"));
            editFechaPublicacion.setText(bundle.getString("publicacion"));

            int posicionEnListaTipoDocumento = tipoDocumentoIdList.indexOf(bundle.getInt("idTipoDocumento"));
            spTipoDocumento.setSelection(posicionEnListaTipoDocumento);

            int posicionEnListaEditorial = editorialIdList.indexOf(bundle.getInt("editorial"));
            spEditorial.setSelection(posicionEnListaEditorial);
            valorSelectEditorial =bundle.getInt("editorial");
            System.out.println("POSICION!!!!!!!!!!!!!!!!!!!"+ String.valueOf(posicionEnListaEditorial));


            int posicionEnListaIdioma = idiomaIdList.indexOf(bundle.getInt("codIdioma"));
            spIdioma.setSelection(posicionEnListaIdioma);


            txtAutores.setText(bundle.getString("autores"));


            clave=bundle.getString("idDocumento");


            //POSIBLE ERROR
            //prestado=bundle.getInt("prestamo");
        }
        editFechaPublicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(DocumentoEditarActivity.this,fechaPublicacion, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    DatePickerDialog.OnDateSetListener fechaPublicacion = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            actualizarFechaPublicacion();
        }
    };

    private void actualizarFechaPublicacion(){
        String formatoFecha = "dd/MM/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.getDefault());
        editFechaPublicacion.setText(format.format(calendar.getTime()));
    }

    public void buscaDocumentoEditar(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());

        if(editBusca.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }

        else{
            helper.open();
            Documento documento =helper.documentoConsultar(Integer.parseInt(editBusca.getText().toString()));
            helper.close();
            if (documento==null){
                Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
            }else {

                editIdDocumento.setEnabled(true);
                editIsbnDocumento.setEnabled(true);
                spTipoDocumento.setEnabled(true);
                spEditorial.setEnabled(true);
                spIdioma.setEnabled(true);
                editNombreDocumento.setEnabled(true);
                editEdicionDocumento.setEnabled(true);
                editFechaPublicacion.setEnabled(true);
                btAutor.setEnabled(true);

                editIdDocumento.setText(String.valueOf(documento.getIdDocumento()));
                editIsbnDocumento.setText(documento.getIsbnDocumento());

                int posicionEnListaTipoDocumento = tipoDocumentoIdList.indexOf(documento.getIdTipoDocumento());
                spTipoDocumento.setSelection(posicionEnListaTipoDocumento);


                int posicionEnListaEditorial = editorialIdList.indexOf(documento.getIdEditorial());
                spEditorial.setSelection(posicionEnListaEditorial);

                int posicionEnListaIdioma = idiomaIdList.indexOf(documento.getCodIdioma());
                spIdioma.setSelection(posicionEnListaIdioma);

                editNombreDocumento.setText(documento.getNombreDocumento());
                editEdicionDocumento.setText(String.valueOf(documento.getEdicionDocumento()));
                editFechaPublicacion.setText(documento.getPublicacionDocumento());
                btAutor.setEnabled(true);
                txtAutores.setText(documento.getAutoresDocumento());
                txtAutores.setEnabled(true);


                // prestado = equipo.getPrestamo();

                clave=editBusca.getText().toString().toUpperCase();

                editBusca.setEnabled(false);
                btnBuscar.setEnabled(false);
            }
        }


    }

    public void autoresDocumentoEditar(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(DocumentoEditarActivity.this);
        TablasV helper = TablasV.getInstance(getApplicationContext());
        Cursor cAutor=helper.obtenerAutores();
        String[] autores = new String[cAutor.getCount()];
        final boolean[] checkedAutores = new boolean[cAutor.getCount()];
        Integer contador=0;
        while (cAutor.moveToNext()){
            autores[contador]=cAutor.getString(2)+" "+cAutor.getString(3);
            checkedAutores[contador]=false;
            contador++;
        }
        final List<String>autoresList= Arrays.asList(autores);
        builder.setMultiChoiceItems(autores, checkedAutores, new DialogInterface.OnMultiChoiceClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedAutores[which] = isChecked;

                // Get the current focused item
                String currentItem = autoresList.get(which);

                // Notify the current action
                Toast.makeText(getApplicationContext(),
                        currentItem, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle(getResources().getString(R.string.documentoCampo9d));

        // Set the positive/yes button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                txtAutores.setText("");
                int ultimo1=0, ultimo2=0;
                for (int i = 0; i<checkedAutores.length; i++){
                    boolean checked = checkedAutores[i];
                    if (checked) {
                        txtAutores.setText(txtAutores.getText() + autoresList.get(i) + "\n");
                    }
                }
            }
        });



        // Set the neutral/cancel button click listener
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the neutral button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }

    public void actualizarDocumento(View v){
        String isbn, nombreDocumento,publicacionDocumento, mensaje;
        int idDocumento,idTipoDocumento,idEditorial,codIdioma,prestadoDocumento,edicionDocumento;

        /*
        idDocumento =Integer.parseInt(editIdDocumento.getText().toString());
        isbn =editIsbnDocumento.getText().toString();

        idTipoDocumento = valorSelectTipoDocumento;
        idEditorial = valorSelectEditorial;
        codIdioma =valorSelectIdioma;

        nombreDocumento = editNombreDocumento.getText().toString();
        prestadoDocumento = valorSelectOpcion;//radio button
        publicacionDocumento = editFechaPublicacion.getText().toString();
        edicionDocumento =Integer.parseInt(editEdicionDocumento.getText().toString());
*/

        if (editIdDocumento.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {

            if(editIsbnDocumento.getText().toString().isEmpty() || editEdicionDocumento.getText().toString().isEmpty() ||
            editNombreDocumento.getText().toString().isEmpty())
            {
                Toast.makeText(this,getResources().getString(R.string.documentoMsj1),Toast.LENGTH_LONG).show();
            }
            //tal vez falte prestadoDocumento
            else{
                idDocumento =Integer.parseInt(editIdDocumento.getText().toString());
                isbn =editIsbnDocumento.getText().toString();

                idTipoDocumento = valorSelectTipoDocumento;
                idEditorial = valorSelectEditorial;
                codIdioma =valorSelectIdioma;

                nombreDocumento = editNombreDocumento.getText().toString().toUpperCase().replaceFirst("\\s++$", "");
                publicacionDocumento = editFechaPublicacion.getText().toString();
                edicionDocumento =Integer.parseInt(editEdicionDocumento.getText().toString());


                TablasV helper= TablasV.getInstance(getApplicationContext());
                Documento documento = new Documento();

                documento.setIdDocumento(idDocumento);
                documento.setIsbnDocumento(isbn);
                documento.setIdTipoDocumento(idTipoDocumento);
                documento.setIdEditorial(idEditorial);
                documento.setCodIdioma(codIdioma);
                documento.setNombreDocumento(nombreDocumento);
                documento.setEdicionDocumento(edicionDocumento);
                documento.setPublicacionDocumento(publicacionDocumento);
                documento.setAutoresDocumento(txtAutores.getText().toString());

                helper.open();
                mensaje=helper.actualizarDocumento(documento,clave.toUpperCase());
                helper.close();
                if (opcion==1){
                    Intent intOp1=new Intent(this,DocumentoMenuActivity.class);
                    this.startActivity(intOp1);
                }else {
                    Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();

                    editIdDocumento.setEnabled(false);
                    editIdDocumento.setText("");

                    editIsbnDocumento.setEnabled(false);
                    editIsbnDocumento.setText("");

                    spTipoDocumento.setEnabled(false);
                    spTipoDocumento.setSelection(0);

                    spEditorial.setEnabled(false);
                    spEditorial.setSelection(0);

                    spIdioma.setEnabled(false);
                    spIdioma.setSelection(0);

                    editNombreDocumento.setEnabled(false);
                    editNombreDocumento.setText("");

                    editEdicionDocumento.setEnabled(false);
                    editEdicionDocumento.setText("");

                    editFechaPublicacion.setEnabled(false);
                    editFechaPublicacion.setText("");


                    editBusca.setText("");
                    clave="";
                    editBusca.setEnabled(true);
                    btnBuscar.setEnabled(true);
                    txtAutores.setEnabled(false);
                    txtAutores.setText("");
                    btAutor.setEnabled(false);
                }
            }

        }
    }









    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_documento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_documento_menu_principal:
                Intent intentP = new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_documento_menu_insertar:
                Intent intent0 = new Intent(this, DocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_documento_menu_consultar:
                Intent intent1 = new Intent(this, DocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_documento_menu_Editar:
                Intent intente = new Intent(this, DocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_documento_menu_eliminar:
                Intent intent2 = new Intent(this, DocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_documento_listar:
                Intent intent3 = new Intent(this, DocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
