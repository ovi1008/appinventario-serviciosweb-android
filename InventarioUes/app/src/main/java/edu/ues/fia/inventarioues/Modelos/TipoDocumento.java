package edu.ues.fia.inventarioues.Modelos;

public class TipoDocumento {

    int idTipoDocumento;
    String nombreTipoDocumento,fechaModificadaTipoDocumento;;

    public TipoDocumento() {
    }

    public TipoDocumento(int idTipoDocumento, String nombreTipoDocumento, String fechaModificadaTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
        this.nombreTipoDocumento = nombreTipoDocumento;
        this.fechaModificadaTipoDocumento = fechaModificadaTipoDocumento;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNombreTipoDocumento() {
        return nombreTipoDocumento;
    }

    public void setNombreTipoDocumento(String nombreTipoDocumento) {
        this.nombreTipoDocumento = nombreTipoDocumento;
    }

    public String getFechaModificadaTipoDocumento() {
        return fechaModificadaTipoDocumento;
    }

    public void setFechaModificadaTipoDocumento(String fechaModificadaTipoDocumento) {
        this.fechaModificadaTipoDocumento = fechaModificadaTipoDocumento;
    }
}
