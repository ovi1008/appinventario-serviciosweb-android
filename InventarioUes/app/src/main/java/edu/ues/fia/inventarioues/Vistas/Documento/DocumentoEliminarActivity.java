package edu.ues.fia.inventarioues.Vistas.Documento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.R;

public class DocumentoEliminarActivity extends AppCompatActivity {
    EditText editIdDocumento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_eliminar);
        editIdDocumento = findViewById(R.id.DocumentoEliminarNombre1);
    }

    public void eliminarDeleteDocumento(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if (editIdDocumento.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.documentoMsj1a),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Documento documento= new Documento();
            documento.setIdDocumento(Integer.parseInt(editIdDocumento.getText().toString()));
            String mensaje= helper.eliminar1Documento(documento);

            //int mensaje2 =Integer.parseInt(helper.checarDependenciaDetalleDocumento(documento));
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,DocumentoEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                //intentEliminar.putExtra("msj2",mensaje2);
                intentEliminar.putExtra("nombre", documento.getIdDocumento());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editIdDocumento.setText("");
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_documento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_documento_menu_principal:
                Intent intentP = new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_documento_menu_insertar:
                Intent intent0 = new Intent(this, DocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_documento_menu_consultar:
                Intent intent1 = new Intent(this, DocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_documento_menu_Editar:
                Intent intente = new Intent(this, DocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_documento_menu_eliminar:
                Intent intent2 = new Intent(this, DocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_documento_listar:
                Intent intent3 = new Intent(this, DocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
