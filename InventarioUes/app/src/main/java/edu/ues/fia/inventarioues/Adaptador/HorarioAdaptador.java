package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.Horario;
import edu.ues.fia.inventarioues.R;

public class HorarioAdaptador extends ArrayAdapter<Horario> {

    public HorarioAdaptador(@NonNull Context context, @NonNull List<Horario> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_horario,parent,false);
        }
        TextView idHorario=convertView.findViewById(R.id.horarioListarId);
        TextView tiempoHorario=convertView.findViewById(R.id.horarioListarNombre);

        Horario horario=getItem(position);
        idHorario.setText(String.valueOf(horario.getIdHorario()));
        tiempoHorario.setText(horario.getTiempoHorario());

        return convertView;
    }
}

