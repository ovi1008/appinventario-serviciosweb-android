package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoDocumentoInsertarActivity extends AppCompatActivity {

    EditText editNombreTipoDocumento;
    private static final int recordImput=1111;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_tipo_documento_insertar);
        editNombreTipoDocumento=(EditText)findViewById(R.id.TipoDocumentoInsertarNombre);
    }

    public void insertarTipoDocumento(View v){
        String nombre, mensaje;
        TablasM helper= TablasM.getInstance(getApplicationContext());
        nombre=editNombreTipoDocumento.getText().toString().toUpperCase();
        if (nombre.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.tipoDocumentoMsj1), Toast.LENGTH_LONG).show();
        }else{
            TipoDocumento tipoDocumento= new TipoDocumento();
            tipoDocumento.setNombreTipoDocumento(nombre);
            helper.open();
            mensaje=helper.insertarTipoDocumento(tipoDocumento);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editNombreTipoDocumento.setText("");
        }
    }

    public void iniciarVozTipoDocumento(View v){
        Intent intentVoz= new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intentVoz.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intentVoz.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intentVoz.putExtra(RecognizerIntent.EXTRA_PROMPT, "Dicta el tipo de documento");
        try {
            startActivityForResult(intentVoz,recordImput);
        }catch (ActivityNotFoundException e){
            e.getStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case recordImput:{
                if(resultCode==RESULT_OK && null!=data){
                    ArrayList<String> result=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editNombreTipoDocumento.setText(result.get(0));
                }
                break;
            }
        }
    }

    public void insertarTipoDocumentoServidor(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreTipoDocumento.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_insert.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_insert.php?";

                String url = url1+ip+url3+
                        "nombreTipoDocumento="+editNombreTipoDocumento.getText().toString().toUpperCase();
                String msj= ServiciosWeb.insertarServidorLocal(url, this);
                Toast.makeText(this, msj+"el tipo documento: "+editNombreTipoDocumento.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();
                editNombreTipoDocumento.setText("");
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_tipo_documento_listarServidor:
                Intent intent4= new Intent(this, TipoDocumentoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_documento_listarSincronizar:
                Intent intent5= new Intent(this, TipoDocumentoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}