package edu.ues.fia.inventarioues.Vistas;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;

import java.io.File;
import java.net.URI;

import edu.ues.fia.inventarioues.Controladores.DatabaseOpenHelper;
import edu.ues.fia.inventarioues.R;

public class CorreoActivity extends AppCompatActivity {
    EditText correo, asusnto, cuerpo, tablas;
    File file;
    private static final int PICK_FROM_GALLERY = 101;
    int columnIndex;
    Uri URI = null;
    String attachmentFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_correo);
        if (Build.VERSION.SDK_INT>=23){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},2);
        }
        correo=findViewById(R.id.correoEnviar);
        asusnto=findViewById(R.id.correoAsusnto);
        cuerpo=findViewById(R.id.correoCuerpo);

    }


    public void enviarCorreo(View v){
        String email,subject,message;
        email = correo.getText().toString();
        subject = asusnto.getText().toString();
        message = cuerpo.getText().toString();
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { email });
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,subject);
        emailIntent.putExtra(Intent.EXTRA_STREAM, URI);
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
        this.startActivity(Intent.createChooser(emailIntent,"Enviando correo InventarioUes..."));
        correo.setText("");
        asusnto.setText("");
        cuerpo.setText("");

    }


}
