package edu.ues.fia.inventarioues.Vistas.Materia;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Materia;
import edu.ues.fia.inventarioues.R;


public class MateriaBuscarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{
    EditText editId,editNombre;
    SearchView searchConsultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_buscar);
        searchConsultar=(SearchView) findViewById(R.id.Materia_searchConsultar);//los del xml
        editId=(EditText)findViewById(R.id.MateriaConsultarId);
        editNombre=(EditText)findViewById(R.id.MateriaConsultarNombre);
        searchConsultar.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String newText) {//buscar
        TablasL helper= TablasL.getInstance(getApplicationContext());
        Materia materia= new Materia();
        helper.open();
        materia=helper.materiaConsultar(newText.toUpperCase());
        helper.close();
        if (materia==null){
            editId.setText("");
            editNombre.setText("");
        }else {
            editId.setText(materia.getIdMateria());
            editNombre.setText(materia.getNombreMateria());
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void buscarEditarMateria(View v){
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            Intent intentEditar= new Intent(this, MateriaEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarMateria(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Materia materia= new Materia();
            materia.setIdMateria(editId.getText().toString().toUpperCase());
            materia.setNombreMateria(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1Materia(materia);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,MateriaEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("id", materia.getIdMateria());//no estoy segura
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_materia,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_materia_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_materia_menu_insertar:
                Intent intent0= new Intent(this, MateriaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_materia_menu_Editar:
                Intent intente= new Intent(this, MateriaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_materia_menu_consultar:
                Intent intent1= new Intent(this, MateriaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_materia_menu_eliminar:
                Intent intent2= new Intent(this, MateriaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_materia_listar:
                Intent intent3= new Intent(this, MateriaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
