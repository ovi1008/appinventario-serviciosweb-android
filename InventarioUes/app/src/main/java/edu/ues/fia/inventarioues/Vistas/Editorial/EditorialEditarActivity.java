package edu.ues.fia.inventarioues.Vistas.Editorial;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class EditorialEditarActivity extends AppCompatActivity {

    EditText editId, editNombre, editBusca,editNombreServer;
    String clave="";
    int opcion=0;
    Button busca,buscaServer;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorial_editar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editId = (EditText) findViewById(R.id.EditorialEditarId);
        editNombre = (EditText) findViewById(R.id.EditorialEditarNombre);
        editBusca=(EditText) findViewById(R.id.EditorialEditarSearch);
        busca=(Button)findViewById(R.id.EditorialEditarBtnConsultar);
        buscaServer=findViewById(R.id.EditorialEditarBusBtnServer);
        editNombreServer=findViewById(R.id.EditorialEditarBusqNombreServidor);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            editBusca.setVisibility(View.INVISIBLE);
            busca.setVisibility(View.INVISIBLE);
            editNombreServer.setVisibility(View.INVISIBLE);
            buscaServer.setVisibility(View.INVISIBLE);

        }
    }
    public void buscarEditorialServidor(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.editorialMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/editorial/ws_editorial_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/editorial/ws_editorial_query.php?";

                String url = url1+ip+url3+
                        "nombreEditorial="+editNombreServer.getText().toString().toUpperCase();
                String jsonEditorial= ServiciosWeb.consultarServidorLocal(url, this);
                if (jsonEditorial==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datosEditorial=jsonEditorial.split(",");
                    String []idEditorial=datosEditorial[0].split(":");
                    String []nombreEditorial=datosEditorial[1].split(":");
                    Editorial editorialS = new Editorial();
                    editorialS.setIdEditorial(Integer.valueOf(idEditorial[1]));
                    editorialS.setNombreEditorial(nombreEditorial[1]);
                    editId.setText(String.valueOf(editorialS.getIdEditorial()));
                    editNombre.setText(editorialS.getNombreEditorial());
                    editNombre.setEnabled(true);
                    clave=editNombreServer.getText().toString().toUpperCase();
                    editBusca.setEnabled(false);
                    busca.setEnabled(false);
                    editNombreServer.setEnabled(false);
                    buscaServer.setEnabled(false);
                }


            }

        }
    }

    public void editarEditorialServidor(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/editorial/ws_editorial_update.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/editorial/ws_editorial_update.php?";

        String url = url1+ip+url3+
                "nombreEditorial="+editNombreServer.getText().toString().toUpperCase()+
                "&nombreEditorialN="+editNombre.getText().toString().toUpperCase();
        String msj= ServiciosWeb.editarServidorLocal(url, this);
        Toast.makeText(this,msj+"la editorial: "+editNombreServer.getText().toString().toUpperCase(),Toast.LENGTH_LONG).show();
        editId.setText("");
        editNombre.setText("");
        editBusca.setText("");
        clave="";
        editId.setEnabled(false);
        editNombre.setEnabled(false);
        busca.setEnabled(true);
        editBusca.setEnabled(true);
        editNombreServer.setText("");
        editNombreServer.setEnabled(true);
        buscaServer.setEnabled(true);


    }

    public void buscaEditorialEditar(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        Editorial editorial=helper.editorialConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (editorial==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(editorial.getIdEditorial()));
            editNombre.setText(editorial.getNombreEditorial());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    public void actualizarEditorial(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasV helper= TablasV.getInstance(getApplicationContext());
            Editorial editorial = new Editorial();
            editorial.setIdEditorial(Integer.valueOf(editId.getText().toString()));
            editorial.setNombreEditorial(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarEditorial(editorial,clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intOp1=new Intent(this,EditorialMenuActivity.class);
                this.startActivity(intOp1);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);
            }
        }


    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editorial,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_editorial_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_editorial_menu_insertar:
                Intent intent0= new Intent(this, EditorialInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_editorial_menu_consultar:
                Intent intent1= new Intent(this, EditorialBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_editorial_menu_Editar:
                Intent intente= new Intent(this, EditorialEditarActivity.class);
                this.startActivity(intente);
                return true;

            case R.id.action_editorial_menu_eliminar:
                Intent intent2= new Intent(this, EditorialEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_editorial_listar:
                Intent intent3= new Intent(this, EditorialMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_editorial_listarServidor:
                Intent intent4= new Intent(this, EditorialListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_editorial_listarSincronizar:
                Intent intent5= new Intent(this, EditorialSincronizacionActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
