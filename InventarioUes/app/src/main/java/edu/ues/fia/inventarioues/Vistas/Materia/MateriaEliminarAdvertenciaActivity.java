package edu.ues.fia.inventarioues.Vistas.Materia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Materia;
import edu.ues.fia.inventarioues.R;

import edu.ues.fia.inventarioues.Vistas.Marca.MarcaMenuActivity;

public class MateriaEliminarAdvertenciaActivity extends AppCompatActivity {
    TextView msj;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_eliminar_advertencia);
        msj=(TextView)findViewById(R.id.MateriaEliminarAdvMens);
        Bundle bundle=getIntent().getExtras();
        msj.setText(getResources().getString(R.string.materiaMsj2)+" "+
                bundle.getString("id")+"\n"+getResources().getString(R.string.materiaMsj3)+" "
                +bundle.getString("msj"));
    }

    public void eliminarAdvMateria(View v){
        Bundle bundle=getIntent().getExtras();
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        Materia materia= new Materia();
        materia.setIdMateria(bundle.getString("id"));
        String mensaje= helper.eliminar2Materia(materia);
        helper.close();
        Intent intentM= new Intent(this,MateriaMenuActivity.class);
        this.startActivity(intentM);
    }

    public void elimincarCancelarMateria(View v){
        Intent intentCancelar = new Intent(this, MateriaMenuActivity.class);
        this.startActivity(intentCancelar);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_materia,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_materia_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_materia_menu_insertar:
                Intent intent0= new Intent(this, MateriaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_materia_menu_consultar:
                Intent intent1= new Intent(this, MateriaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_materia_menu_Editar:
                Intent intente= new Intent(this, MateriaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_materia_menu_eliminar:
                Intent intent2= new Intent(this, MateriaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_materia_listar:
                Intent intent3= new Intent(this, MateriaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
