package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Marca.MarcaEliminarActivity;

public class MarcaAdaptador extends ArrayAdapter<Marca> {

    public MarcaAdaptador(@NonNull Context context, @NonNull List<Marca> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable  View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_marca,parent,false);
        }
        TextView idMarca=convertView.findViewById(R.id.marcaListarId);
        TextView nombreMarca=convertView.findViewById(R.id.marcaListarNombre);

        Marca marca=getItem(position);
        idMarca.setText(String.valueOf(marca.getIdMarca()));
        nombreMarca.setText(marca.getNombreMarca());

        return convertView;
    }
}
