package edu.ues.fia.inventarioues.Vistas.Equipo;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.EquipoAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.R;

public class EquipoMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_menu);
        ListView listView =(ListView)findViewById(R.id.listEquipo);
        List<Equipo> lista= listadoEquipos();
        EquipoAdaptador equipoAdaptador=new EquipoAdaptador(this,lista);
        listView.setAdapter(equipoAdaptador);
    }

    public List<Equipo> listadoEquipos(){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        Cursor c =helper.obtenerEquipos();
        List<Equipo> equipoList= new ArrayList<>();
        while (c.moveToNext()){
            Equipo equipo=new Equipo();
            //{"serialEquipo", "idTipoEquipo", "idEquipo", "descripcionEquipo", "estadoEquipo",
            // "fechaEquipo", "fechaInactivoEquipo"};
            equipo.setSerialEquipo(c.getString(0));
            equipo.setIdTipoEquipo(c.getInt(1));
            equipo.setIdMarca(c.getInt(2));
            equipoList.add(equipo);
        }
        return equipoList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_equipo_menu_insertar:
                Intent intent0= new Intent(this, EquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_equipo_menu_consultar:
                Intent intent1= new Intent(this, EquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_equipo_menu_Editar:
                Intent intente= new Intent(this, EquipoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_equipo_menu_eliminar:
                Intent intent2= new Intent(this, EquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_equipo_listar:
                Intent intent3= new Intent(this, EquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    
}
