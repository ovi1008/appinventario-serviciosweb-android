package edu.ues.fia.inventarioues.Vistas.Equipo;

import android.Manifest;
import net.glxn.qrgen.android.QRCode;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


import com.blikoon.qrcodescanner.QrCodeActivity;

import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.NuevoCodigoActivity;
import edu.ues.fia.inventarioues.R;

public class EquipoConsultarActivity extends AppCompatActivity  {

    EditText editLocal,editSerial, editMarca, editTipo, editEstado, editDescripcion, editFechaIngreso, editFechaInactividad;
    SearchView searchEquipo;
    Integer valorSelectMarca, valorSelectTipo, valorSelectOpcion , valorPrestamo;
    TextView txtEstado;
    TextToSpeech toSpeech;
    int result;
    ImageView previewImg;

    private static final int REQUEST_CODE_QR_SCAN = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_consultar);
        txtEstado=(TextView)findViewById(R.id.EquipoConsultarPrestamo);
        editSerial=(EditText)findViewById(R.id.EquipoConsultarSerial);
        editMarca=(EditText)findViewById(R.id.EquipoConsultarMarcaId);
        editTipo=(EditText)findViewById(R.id.EquipoConsultarTipoId);
        editEstado=(EditText)findViewById(R.id.EquipoConsultarEstado);
        editDescripcion=(EditText)findViewById(R.id.EquipoConsultarDescripcion);
        editFechaIngreso=(EditText)findViewById(R.id.EquipoConsultarFechaIngreso);
        editFechaInactividad=(EditText)findViewById(R.id.EquipoConsultarFechaInactividad);
        editLocal=findViewById(R.id.equipoBusquedaSerial);
        previewImg=findViewById(R.id.EquipoConsultarImg);



        toSpeech=new TextToSpeech(EquipoConsultarActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    result=toSpeech.setLanguage(Locale.getDefault());
                }else{
                    Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
                }
            }
        });

    }



    public void playEquipo(View v){
        if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
            Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
        }else {
            String textoHablar;
            if (editSerial.getText().toString().isEmpty()){
                textoHablar="Realice una consulta primero";
            }else {
                textoHablar="Se consulta el siguiente equipo:\n\n" +
                        "El equipo esta: "+txtEstado.getText().toString()+"\n\n"+
                        "El serial del equipo es: " +editSerial.getText().toString()+"\n\n"+
                        "El equipo es de tipo : "+editTipo.getText().toString()+"\n\n"+
                "El equipo es de marca: "+editMarca.getText().toString()+"\n\n"+
                "La descripcion del equipo es: "+editDescripcion.getText()+"\n\n"+
                "El estado del equipo es: "+editEstado.getText().toString()+"\n\n"+
                "Fecha de ingreso del equipo es: "+editFechaIngreso.getText().toString()+"\n\n";
                if (editFechaInactividad.getText().toString().isEmpty()){}
                else {
                    textoHablar=textoHablar+"Fecha de inactividad del equipo es:"+editFechaInactividad.getText().toString();
                }
            }
            toSpeech.speak(textoHablar,TextToSpeech.QUEUE_FLUSH,null);
        }
    }

    public void stopEquipo(View v){
        if (toSpeech!=null){
            toSpeech.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (toSpeech!=null){
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }

    public void consultarEquipoLocal(View v) {
        String newText=editLocal.getText().toString().toUpperCase();
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        Equipo equipo= new Equipo();
        helper.open();
        equipo=helper.equipoConsultar(newText.toUpperCase());
        helper.close();
        if (equipo==null){
            editSerial.setText("");
            editMarca.setText("");
            editTipo.setText("");
            editEstado.setText("");
            editDescripcion.setText("");
            editFechaIngreso.setText("");
            editFechaInactividad.setText("");
            txtEstado.setText(getResources().getString(R.string.equipoCampo5a));
            txtEstado.setTextColor(Color.rgb(164,164,164));
            previewImg.setImageResource(R.drawable.no_disponible);
        }else {
            if (equipo.getPrestamo()==1){
                txtEstado.setText(getResources().getString(R.string.equipoCampo10d));
                txtEstado.setTextColor(Color.rgb(100,221,23));
                valorPrestamo=1;
            }else {
                txtEstado.setText(getResources().getString(R.string.equipoCampo11d));
                txtEstado.setTextColor(Color.rgb(213,0,0));
                valorPrestamo=0;
            }
            editSerial.setText(equipo.getSerialEquipo());
            Cursor tipoEquipo = helper.obtenerTiposEquipo();
            while (tipoEquipo.moveToNext() && editTipo.getText().toString().isEmpty()){
                if (tipoEquipo.getInt(0)==equipo.getIdTipoEquipo()){
                    editTipo.setText(tipoEquipo.getString(1));
                    valorSelectTipo=tipoEquipo.getInt(0);
                }
            }
            Cursor marca=helper.obtenerMarcas();
            while (marca.moveToNext() && editMarca.getText().toString().isEmpty()){
                if (marca.getInt(0)==equipo.getIdMarca()){
                    editMarca.setText(marca.getString(1));
                    valorSelectMarca=marca.getInt(0);
                }
            }
            if (equipo.getEstadoEquipo()==1){
                editEstado.setText(getResources().getString(R.string.equipoCampo8r));
                valorSelectOpcion=1;
            }else {
                editEstado.setText(getResources().getString(R.string.equipoCampo9r));
                valorSelectOpcion=0;
            }
            editDescripcion.setText(equipo.getDescripcionEquipo());
            editFechaIngreso.setText(equipo.getFechaEquipo());
            editFechaInactividad.setText(equipo.getFechaInactivoEquipo());
            if (equipo.getFotoEquipo()==null){
            }else {
                Bitmap bitmapConsultada= BitmapFactory.decodeByteArray(equipo.getFotoEquipo(),0,equipo.getFotoEquipo().length);
                previewImg.setImageBitmap(bitmapConsultada);
            }
        }
    }

    public void generarCodigoEquipo(View v){
        String textoCodigo;
        if (editSerial.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            textoCodigo=
                    "El serial del equipo es :" +editSerial.getText().toString()+",\n\n"+
                    "El equipo es de tipo : "+editTipo.getText().toString()+"\n\n"+
                    "El equipo es de marca: "+editMarca.getText().toString()+"\n\n"+
                    "El estado del equipo es: "+editEstado.getText().toString()+"\n\n";
            if (editFechaInactividad.getText().toString().isEmpty()){}
            else {
                textoCodigo=textoCodigo+"Fecha de inactividad del equipo es:"+editFechaInactividad.getText().toString();
            }
            Intent codigo = new Intent(this, NuevoCodigoActivity.class);
            codigo.putExtra("codigoQr",textoCodigo);
            codigo.putExtra("titulo", "Equipo");
            this.startActivity(codigo);
        }
    }

    public void scanearCodigoEquipo(View v){


        Intent i = new Intent(EquipoConsultarActivity.this, QrCodeActivity.class);
        startActivityForResult(i, REQUEST_CODE_QR_SCAN);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(getApplicationContext(), "No se pudo obtener una respuesta", Toast.LENGTH_SHORT).show();
            String resultado = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (resultado != null) {
                Toast.makeText(getApplicationContext(), "No se pudo escanear el código QR", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data != null) {
                String lectura = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
                String[] resultadoDatos=lectura.split(",");
                String[] datoCodigo=resultadoDatos[0].split(":");
                String validacion=lectura.substring(0,9);
                if (validacion.equals("El serial")){
                    editLocal.setText(datoCodigo[1]);
                    Toast.makeText(getApplicationContext(), "Leído: " + lectura, Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(), "Codigio Qr no valido", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public void equipoConsultarEdicion(View v){
        if(editSerial.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            String serial,descripcion, ingreso, inactividad, mensaje;
            serial=editSerial.getText().toString().toUpperCase();
            descripcion=editDescripcion.getText().toString().toUpperCase();
            ingreso=editFechaIngreso.getText().toString();
            inactividad=editFechaInactividad.getText().toString();
            Intent intentEditar= new Intent(this, EquipoEditarActivity.class);
            intentEditar.putExtra("serial",serial);
            intentEditar.putExtra("descripcion", descripcion);
            intentEditar.putExtra("ingreso",ingreso);
            intentEditar.putExtra("inactividad",inactividad);
            intentEditar.putExtra("marca",valorSelectMarca);
            intentEditar.putExtra("tipo",valorSelectTipo);
            intentEditar.putExtra("estado",valorSelectOpcion);
            intentEditar.putExtra("prestamo",valorPrestamo);
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarEquipo(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        if(editSerial.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Equipo equipo= new Equipo();
            equipo.setSerialEquipo(editSerial.getText().toString());
            String mensaje= helper.eliminar1Equipo(equipo);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,EquipoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", equipo.getSerialEquipo());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_equipo_menu_insertar:
                Intent intent0= new Intent(this, EquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_equipo_menu_consultar:
                Intent intent1= new Intent(this, EquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_equipo_menu_Editar:
                Intent intente= new Intent(this, EquipoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_equipo_menu_eliminar:
                Intent intent2= new Intent(this, EquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_equipo_listar:
                Intent intent3= new Intent(this, EquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
