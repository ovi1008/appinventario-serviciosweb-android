package edu.ues.fia.inventarioues.Vistas.Prestamo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Prestamo;
import edu.ues.fia.inventarioues.R;

public class PrestamoEliminarActivity extends AppCompatActivity {

    Integer vista;
    EditText editIdPrestamo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prestamo_eliminar);
        Bundle bundle=getIntent().getExtras();
        vista= bundle.getInt("vista");
        if (vista==1){
            TextView titulo=findViewById(R.id.PrestamoEliminarIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud4P1));
        }else {
            TextView titulo=findViewById(R.id.PrestamoEliminarIntro);
            titulo.setText(getResources().getString(R.string.prestamoCrud4P2));
        }
        editIdPrestamo=findViewById(R.id.PrestamoEliminarId);
    }

    public void eliminarDeletePrestamo(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        if (editIdPrestamo.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.prestamoMsj1a),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Prestamo prestamoVista=helper.prestamoConsultar(Integer.valueOf(editIdPrestamo.getText().toString()));
            helper.close();
            if (prestamoVista==null){
                Toast.makeText(this,getResources().getString(R.string.msj2Estad),Toast.LENGTH_LONG).show();
            }
            else {
                if (vista==1){
                    if (prestamoVista.getSerialEquipo()==null){
                        Prestamo prestamo= new Prestamo();
                        prestamo.setIdMovimientoInventario(Integer.valueOf(editIdPrestamo.getText().toString()));
                        helper.open();
                        String mensaje= helper.eliminarPrestamo(prestamo);
                        helper.close();
                        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                    }else {
                        String mensaje=getResources().getString(R.string.msjEsPrestamo02);
                        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    if (prestamoVista.getSerialEquipo()==null){
                        String mensaje=getResources().getString(R.string.msjEsPrestamo01);
                        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                    }else {
                        Prestamo prestamo = new Prestamo();
                        prestamo.setIdMovimientoInventario(Integer.valueOf(editIdPrestamo.getText().toString()));
                        helper.open();
                        String mensaje = helper.eliminarPrestamo(prestamo);
                        helper.close();
                        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
                    }
                }
            }
            editIdPrestamo.setText("");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (vista==1){
            getMenuInflater().inflate(R.menu.menu_prestamo001,menu);
        }else {
            getMenuInflater().inflate(R.menu.menu_prestamo002,menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (vista==1){
            switch (item.getItemId()){
                case R.id.action_prestamo001_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",1);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo001_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",1);
                    this.startActivity(intentDev);
                    return true;

                case R.id.action_prestamo001_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",1);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo001_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",1);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo001_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",1);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo001_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",1);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo001_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",1);
                    this.startActivity(intent3);
                    return true;
            }

        }else{
            switch (item.getItemId()){
                case R.id.action_prestamo002_menu_principal:
                    Intent intentP= new Intent(this, PrestamoActivity.class);
                    intentP.putExtra("vista",2);
                    this.startActivity(intentP);
                    return true;

                case R.id.action_prestamo002_menu_entregas:
                    Intent intentDev= new Intent(this, PrestamoDevolverActivity.class);
                    intentDev.putExtra("vista",2);
                    this.startActivity(intentDev);
                    return true;


                case R.id.action_prestamo002_menu_insertar:
                    Intent intent0= new Intent(this, PrestamoInsertarActivity.class);
                    intent0.putExtra("vista",2);
                    this.startActivity(intent0);
                    return true;

                case R.id.action_prestamo002_menu_consultar:
                    Intent intent1= new Intent(this, PrestamoConsultarActivity.class);
                    intent1.putExtra("vista",2);
                    this.startActivity(intent1);
                    return true;

                case R.id.action_prestamo002_menu_Editar:
                    Intent intente= new Intent(this, PrestamoEditarActivity.class);
                    intente.putExtra("vista",2);
                    this.startActivity(intente);
                    return true;


                case R.id.action_prestamo002_menu_eliminar:
                    Intent intent2= new Intent(this, PrestamoEliminarActivity.class);
                    intent2.putExtra("vista",2);
                    this.startActivity(intent2);
                    return true;

                case R.id.action_prestamo002_listar:
                    Intent intent3= new Intent(this,PrestamoMenuActivity.class);
                    intent3.putExtra("vista",2);
                    this.startActivity(intent3);
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
