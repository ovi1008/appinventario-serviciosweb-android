package edu.ues.fia.inventarioues.Vistas.Autor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Autor;
import edu.ues.fia.inventarioues.R;

public class AutorEliminarAdvertenciaActivity extends AppCompatActivity {

    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autor_eliminar_advertencia);

        msj=(TextView)findViewById(R.id.AutorEliminarAdvMens);
        Bundle bundle=getIntent().getExtras();
        msj.setText(getResources().getString(R.string.autorMsj2) +" "+
                bundle.getString("nombre") +" "
                +bundle.getString("apellido")+
                "\n\n"+getResources().getString(R.string.autorMsj3)+" "
                +bundle.getString("msj"));
    }


    public void eliminarAdvAutor(View v){
        Bundle bundle=getIntent().getExtras();
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        Autor autor= new Autor();
        autor.setNombreAutor(bundle.getString("nombre"));
        autor.setApellidoAutor(bundle.getString("apellido"));
        String mensaje= helper.eliminar2Autor(autor);
        helper.close();
        Intent intentM= new Intent(this,AutorMenuActivity.class);
        this.startActivity(intentM);
    }

    public void elimincarCancelarAutor(View v){
        Intent intentCancelar = new Intent(this, AutorMenuActivity.class);
        this.startActivity(intentCancelar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;



            case R.id.action_autor_menu_insertar:
                Intent intent0= new Intent(this, AutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_autor_menu_Editar:
                Intent intente= new Intent(this, AutorEditarActivity.class);
                this.startActivity(intente);
                return true;

            case R.id.action_autor_menu_consultar:
                Intent intent1= new Intent(this, AutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_autor_menu_eliminar:
                Intent intent2= new Intent(this, AutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_autor_listar:
                Intent intent3= new Intent(this, AutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
