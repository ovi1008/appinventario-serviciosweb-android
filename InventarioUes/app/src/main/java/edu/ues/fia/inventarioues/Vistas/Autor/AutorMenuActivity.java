package edu.ues.fia.inventarioues.Vistas.Autor;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.AutorAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Autor;
import edu.ues.fia.inventarioues.R;

public class AutorMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autor_menu);
        ListView listView =(ListView)findViewById(R.id.listAutor);
        List<Autor> lista= listadoAutores();
        AutorAdaptador autorAdaptador=new AutorAdaptador(this,lista);
        listView.setAdapter(autorAdaptador);
    }


    public List<Autor> listadoAutores(){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        Cursor c =helper.obtenerAutores();
        List<Autor> autorList= new ArrayList<>();
        while (c.moveToNext()){
            Autor autor=new Autor();
            /*
            {"idAutor", "idTipoAutor",
"nombreAutor","apellidoAutor","direccionAutor","telefonoAutor"};
            * */
            autor.setIdAutor(c.getInt(0));
            autor.setIdTipoAutor(c.getInt(1));
            autor.setNombreAutor(c.getString(2));
            autor.setApellidoAutor(c.getString(3));
            autor.setDireccionAutor(c.getString(4));
            autor.setTelefonoAutor(c.getString(5));

            autorList.add(autor);
        }
        return autorList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_autor_menu_insertar:
                Intent intent0= new Intent(this, AutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_autor_menu_consultar:
                Intent intent1= new Intent(this, AutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_autor_menu_Editar:
                Intent intente= new Intent(this, AutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_autor_menu_eliminar:
                Intent intent2= new Intent(this, AutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_autor_listar:
                Intent intent3= new Intent(this, AutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
