package edu.ues.fia.inventarioues.Modelos;

public class Autor {

    /*
    private  static final String [] camposAutor = new String[]{"idAutor", "idTipoAutor",
"nombreAutor","apellidoAutor","direccionAutor","telefonoAutor"};

     */
    int idAutor;
    int    idTipoAutor;
    String nombreAutor;
    String apellidoAutor;
    String direccionAutor;
    String telefonoAutor;

    public Autor() {
    }

    public Autor(int idAutor, int idTipoAutor, String nombreAutor, String apellidoAutor, String direccionAutor, String telefonoAutor) {
        this.idAutor = idAutor;
        this.idTipoAutor = idTipoAutor;
        this.nombreAutor = nombreAutor;
        this.apellidoAutor = apellidoAutor;
        this.direccionAutor = direccionAutor;
        this.telefonoAutor = telefonoAutor;
    }

    public int getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(int idAutor) {
        this.idAutor = idAutor;
    }

    public int getIdTipoAutor() {
        return idTipoAutor;
    }

    public void setIdTipoAutor(int idTipoAutor) {
        this.idTipoAutor = idTipoAutor;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public String getApellidoAutor() {
        return apellidoAutor;
    }

    public void setApellidoAutor(String apellidoAutor) {
        this.apellidoAutor = apellidoAutor;
    }

    public String getDireccionAutor() {
        return direccionAutor;
    }

    public void setDireccionAutor(String direccionAutor) {
        this.direccionAutor = direccionAutor;
    }

    public String getTelefonoAutor() {
        return telefonoAutor;
    }

    public void setTelefonoAutor(String telefonoAutor) {
        this.telefonoAutor = telefonoAutor;
    }
}
