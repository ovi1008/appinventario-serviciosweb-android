package edu.ues.fia.inventarioues.Vistas.Horario;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Horario;
import edu.ues.fia.inventarioues.R;

public class HorarioEditarActivity extends AppCompatActivity {

    EditText editId, editHorarioInicio, editHorarioFinal, editBusca;
    String clave="";
    Button busca;
    int opcion=0;
    Integer hora1=25, min1=61;
    String CERO = "0", DOS_PUNTOS = ":";
    boolean is24HourView=true;
    Calendar calendar=Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario_editar);
        editId=(EditText)findViewById(R.id.HorarioEditarId);
        editHorarioInicio=(EditText)findViewById(R.id.HorarioEditarHoraInicio);
        editHorarioFinal=(EditText)findViewById(R.id.HorarioEditarHoraFinal);
        editBusca=(EditText) findViewById(R.id.HorarioEditarSearch);
        busca=(Button)findViewById(R.id.HorarioEditarBtnBuscar);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editHorarioInicio.setEnabled(false);
            editHorarioFinal.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            String hora=bundle.getString("nombre");
            String [] horaS=hora.split("-");
            editHorarioInicio.setText(horaS[0]);
            editHorarioFinal.setText(horaS[1]);
            clave=bundle.getString("id");
            opcion=1;
            busca.setVisibility(View.INVISIBLE);
            editBusca.setVisibility(View.INVISIBLE);

        }

        editHorarioInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(HorarioEditarActivity.this,inicioHora, calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),is24HourView).show();
            }
        });

        editHorarioFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(HorarioEditarActivity.this,inicioFinal, calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),is24HourView).show();
            }
        });

    }

    TimePickerDialog.OnTimeSetListener inicioHora= new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            hora1=hourOfDay;
            min1=minute;
            //Formateo el hora obtenido: antepone el 0 si son menores de 10
            String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
            //Formateo el minuto obtenido: antepone el 0 si son menores de 10
            String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
            //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
            String AM_PM;
            if(hourOfDay < 12) {
                AM_PM = "a.m.";
            } else {
                AM_PM = "p.m.";
            }
            editHorarioInicio.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
        }
    };

    TimePickerDialog.OnTimeSetListener inicioFinal= new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            //Formateo el hora obtenido: antepone el 0 si son menores de 10
            String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
            //Formateo el minuto obtenido: antepone el 0 si son menores de 10
            String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
            //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
            String AM_PM;
            if(hourOfDay < 12) {
                AM_PM = "a.m.";
            } else {
                AM_PM = "p.m.";
            }
            editHorarioFinal.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            if (hourOfDay<=hora1){
                if (hourOfDay==hora1){
                    if (minute<=min1){
                        editHorarioFinal.setText("");
                    }else {}
                }else {
                    editHorarioFinal.setText("");
                }
            }
        }
    };

    public void actualizarHorario(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            String nombre=editHorarioInicio.getText().toString()+"-"+editHorarioFinal.getText().toString();
            TablasM helper= TablasM.getInstance(getApplicationContext());
            Horario horario = new Horario();
            horario.setIdHorario(Integer.valueOf(editId.getText().toString()));
            horario.setTiempoHorario(nombre);
            helper.open();
            String mensaje=helper.actualizarHorario(horario, clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intentEdi=new Intent(this, HorarioMenuActivity.class);
                this.startActivity(intentEdi);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editHorarioInicio.setText("");
                editHorarioFinal.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editHorarioInicio.setEnabled(false);
                editHorarioFinal.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);

            }
        }

    }

    public void buscarHorarioEditar(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        Horario horario=helper.horarioConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (horario==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(horario.getIdHorario()));

            String hora=horario.getTiempoHorario();
            String [] horaS=hora.split("-");
            editHorarioInicio.setText(horaS[0]);
            editHorarioFinal.setText(horaS[1]);

            editId.setEnabled(true);
            editHorarioInicio.setEnabled(true);
            editHorarioFinal.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_horario,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_horario_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_horario_menu_insertar:
                Intent intent0= new Intent(this, HorarioInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_horario_menu_consultar:
                Intent intent1= new Intent(this, HorarioConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_horario_menu_Editar:
                Intent intente= new Intent(this, HorarioEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_horario_menu_eliminar:
                Intent intent2= new Intent(this, HorarioEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_horario_listar:
                Intent intent3= new Intent(this, HorarioMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
