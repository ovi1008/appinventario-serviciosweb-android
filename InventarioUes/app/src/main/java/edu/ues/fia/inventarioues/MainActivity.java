package edu.ues.fia.inventarioues;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.security.keystore.KeyGenParameterSpec;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import edu.ues.fia.inventarioues.Controladores.FingerprintHandler;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.Usuario;
import edu.ues.fia.inventarioues.Vistas.Prestamo.PrestamoDevolverActivity;

public class MainActivity extends Activity {

    EditText editUserName, editPassworUsuario;
    Locale locale;
    Configuration config = new Configuration();
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;

    private KeyStore keyStore;
    private Cipher cipher;
    private String KEY_NAME = "AndroidKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editUserName=(EditText)findViewById(R.id.userUsuario);
        editPassworUsuario=(EditText)findViewById(R.id.passwordUsuario);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if(!fingerprintManager.isHardwareDetected()){

                //mParaLabel.setText("Fingerprint Scanner not detected in Device");

            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED){

                //mParaLabel.setText("Permission not granted to use Fingerprint Scanner");


            } else if (!keyguardManager.isKeyguardSecure()){

                //mParaLabel.setText("Add Lock to your Phone in Settings");
                Toast.makeText(this,"Add Lock to your Phone in Settings",Toast.LENGTH_LONG).show();

            } else if (!fingerprintManager.hasEnrolledFingerprints()){

                //mParaLabel.setText("You should add atleast 1 Fingerprint to use this Feature");
                Toast.makeText(this,"You should add atleast 1 Fingerprint to use this Feature",Toast.LENGTH_LONG).show();

            } else {

                //mParaLabel.setText("Place your Finger on Scanner to Access the App.");
                //Toast.makeText(this,"Puedes poner tu huella digital en el escaner" +
                  //      "para poder acceder si eres administrador",Toast.LENGTH_LONG).show();

                final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);

                // Set a message/question for alert dialog
                builder.setMessage("Puedes poner tu huella digital en el escaner, para poder acceder si eres administrador.");

                // Specify the dialog is not cancelable
                builder.setCancelable(false);

                // Set a title for alert dialog
                builder.setTitle("Login por huella digital");

                // Set the positive/yes button click click listener
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do something when click positive button
                        //rl.setBackgroundColor(Color.parseColor("#FFA4E098"));
                    }
                });


                android.support.v7.app.AlertDialog dialog = builder.create();
                // Display the alert dialog on interface
                dialog.show();

                generateKey();

                if (cipherInit()){

                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                    FingerprintHandler fingerprintHandler = new FingerprintHandler(this);
                    fingerprintHandler.startAuth(fingerprintManager, cryptoObject);

                }


            }

        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }


        try {

            keyStore.load(null);

            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);

            cipher.init(Cipher.ENCRYPT_MODE, key);

            return true;

        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void generateKey() {

        try {

            keyStore = KeyStore.getInstance("AndroidKeyStore");
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();

        } catch (KeyStoreException | IOException | CertificateException
                | NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | NoSuchProviderException e) {

            e.printStackTrace();

        }

    }

    public void consultarUsuario(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        Usuario usuario=helper.usuarioConsultar(editUserName.getText().toString(), editPassworUsuario.getText().toString());
        helper.close();
        if(usuario==null){
            Toast.makeText(this,"Datos de usuario y contraseña incorrectos o no exiten.",Toast.LENGTH_LONG).show();
        }else {
            Intent inte = new Intent(this, MenuPrincipalActivity.class);
            this.startActivity(inte);
            editUserName.setText("");
            editPassworUsuario.setText("");
        }
    }

    public void idiomaMain(View v){

        // Build an AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        // Set a title for alert dialog
        builder.setTitle(R.string.loginIdioma);

        // Initializing an array of idiomas
        final String[] idiomas = getResources().getStringArray(R.array.idiomas);

        // Set the list of items for alert dialog
        builder.setItems(idiomas, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String selectedIdioma = Arrays.asList(idiomas).get(which);
                // Set the layout background color as user selection
                //rl.setBackgroundColor(Color.parseColor(selectedIdioma));
                if (selectedIdioma.equals(idiomas[0])){
                    locale = new Locale("es");
                    config.locale =locale;
                    getResources().updateConfiguration(config, null);
                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);

                }
                if (selectedIdioma.equals(idiomas[1])){
                    locale = new Locale("en");
                    config.locale =locale;
                    getResources().updateConfiguration(config, null);
                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);
                }
                if (selectedIdioma.equals(idiomas[2])){
                    locale = new Locale("it");
                    config.locale =locale;
                    getResources().updateConfiguration(config, null);
                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);
                }
                if (selectedIdioma.equals(idiomas[3])){
                    locale = new Locale("pt");
                    config.locale =locale;
                    getResources().updateConfiguration(config, null);
                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);
                }

            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();

    }

    @Override
    public void onBackPressed() {
    }
}
