package edu.ues.fia.inventarioues.Vistas.UnidadAdministrativa;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.UnidadAdministrativaAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasL;

import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoConsultarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoEditarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoEliminarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoInsertarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoMenuActivity;

public class UnidadAdministrativaMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unidad_administrativa_menu);
        ListView listView =(ListView)findViewById(R.id.listUnidadesAdministrativas);
        List<UnidadAdministrativa> lista= listadoUnidadAdministrativa();
        UnidadAdministrativaAdaptador unidadAdministrativaAdaptador=new UnidadAdministrativaAdaptador(this,lista);
        listView.setAdapter(unidadAdministrativaAdaptador);
    }
    public List<UnidadAdministrativa> listadoUnidadAdministrativa(){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        Cursor c =helper.obtenerUnidadesAdministrativas();
        List<UnidadAdministrativa> unidadAdministrativasList= new ArrayList<>();
        while (c.moveToNext()){
            UnidadAdministrativa unidadAdministrativa=new UnidadAdministrativa();

            unidadAdministrativa.setIdUnidadAdministrativa(c.getInt(0));
            unidadAdministrativa.setNombreUnidadAdministrativa(c.getString(1));
            unidadAdministrativasList.add(unidadAdministrativa);
        }
        return unidadAdministrativasList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unidad_administrativa,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_unidad_administrativa_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_unidad_administrativa_menu_insertar:
                Intent intent0= new Intent(this, UnidadAdministrativaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_unidad_administrativa_menu_consultar:
                Intent intent1= new Intent(this, UnidadAdministrativaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_unidad_administrativa_menu_Editar:
                Intent intente= new Intent(this, UnidadAdministrativaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_unidad_administrativa_menu_eliminar:
                Intent intent2= new Intent(this, UnidadAdministrativaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_unidad_administrativa_listar:
                Intent intent3= new Intent(this, UnidadAdministrativaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
