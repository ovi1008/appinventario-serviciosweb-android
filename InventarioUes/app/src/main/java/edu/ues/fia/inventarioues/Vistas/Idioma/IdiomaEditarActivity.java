package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;
@SuppressLint("NewApi")
public class IdiomaEditarActivity extends AppCompatActivity {

    EditText editId, editNombre, editBusca,editNombreServer;
    String clave="";
    Button busca,buscarServidor;
    int opcion=0;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idioma_editar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editId=(EditText)findViewById(R.id.IdiomaEditarId);
        editNombre=(EditText)findViewById(R.id.IdiomaEditarNombre);
        editBusca=(EditText) findViewById(R.id.IdiomaEditarSearch);
        busca=(Button)findViewById(R.id.IdiomaEditarBtnBuscar);
        buscarServidor=findViewById(R.id.IdiomaEditarBtnServer);
        editNombreServer=findViewById(R.id.IdiomaEditarNombreServidor);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            editNombreServer.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            busca.setVisibility(View.INVISIBLE);
            editBusca.setVisibility(View.INVISIBLE);
            buscarServidor.setVisibility(View.INVISIBLE);
            editNombreServer.setVisibility(View.INVISIBLE);

        }
    }


    public void editarBuscarIdioma(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.idiomaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/idioma/ws_idioma_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/idioma/ws_idioma_query.php?";

                String url = url1+ip+url3+
                        "nombreIdioma="+editNombreServer.getText().toString().toUpperCase();
                String jsonIdioma= ServiciosWeb.consultarServidorLocal(url, this);
                if (jsonIdioma==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datoIdioma=jsonIdioma.split(",");
                    String []idIdioma=datoIdioma[0].split(":");
                    String []nombreIdioma=datoIdioma[1].split(":");
                    Idioma idiomaS = new Idioma();
                    idiomaS.setCodIdioma(Integer.valueOf(idIdioma[1]));
                    idiomaS.setNombreIdioma(nombreIdioma[1]);
                    editId.setText(String.valueOf(idiomaS.getCodIdioma()));
                    editNombre.setText(idiomaS.getNombreIdioma());

                    editId.setEnabled(true);
                    editNombre.setEnabled(true);
                    clave=editBusca.getText().toString().toUpperCase();
                    editBusca.setEnabled(false);
                    busca.setEnabled(false);
                    editNombreServer.setEnabled(false);
                    buscarServidor.setEnabled(false);
                }


            }

        }
    }

    public void editarIdiomaServidor(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/idioma/ws_idioma_update.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/idioma/ws_idioma_update.php?";

        String url = url1+ip+url3+
                "nombreIdioma="+editNombreServer.getText().toString().toUpperCase()+
                "&nombreIdiomaN="+editNombre.getText().toString().toUpperCase();
        String msj= ServiciosWeb.editarServidorLocal(url, this);
        Toast.makeText(this,msj+"el idioma: "+editNombreServer.getText().toString().toUpperCase(),Toast.LENGTH_LONG).show();
        editId.setText("");
        editNombre.setText("");
        editBusca.setText("");
        clave="";
        editId.setEnabled(false);
        editNombre.setEnabled(false);
        busca.setEnabled(true);
        editBusca.setEnabled(true);
        editNombreServer.setText("");
        editNombreServer.setEnabled(true);
        buscarServidor.setEnabled(true);


    }

    public void actualizarIdioma(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasL helper= TablasL.getInstance(getApplicationContext());
            Idioma idioma = new Idioma();
            idioma.setCodIdioma(Integer.valueOf(editId.getText().toString()));
            idioma.setNombreIdioma(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarIdioma(idioma, clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intentEdi=new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intentEdi);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);

            }
        }

    }
    public void buscarIdiomaEditar(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        Idioma idioma=helper.idiomaConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (idioma==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(idioma.getCodIdioma()));
            editNombre.setText(idioma.getNombreIdioma());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_idioma_listarServidor:
                Intent intent4= new Intent(this, IdiomaListarServidor2Activity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_idioma_listarSincronizar:
                Intent intent5= new Intent(this, IdiomaSincronizar2Activity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
