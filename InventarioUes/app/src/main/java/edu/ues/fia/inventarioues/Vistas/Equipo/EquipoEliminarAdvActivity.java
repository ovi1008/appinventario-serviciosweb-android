package edu.ues.fia.inventarioues.Vistas.Equipo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Marca.MarcaMenuActivity;

public class EquipoEliminarAdvActivity extends AppCompatActivity {

    TextView txtMsj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_eliminar_adv);
        txtMsj=(TextView)findViewById(R.id.EquipoEliminarAdvMens);
        Bundle bundle=getIntent().getExtras();
        txtMsj.setText(getResources().getString(R.string.equipoMsj2)+" "
                +bundle.getString("nombre")+
                "\n\n"+getResources().getString(R.string.equipoMsj3)+" "
                +bundle.getString("msj"));
    }

    public void eliminarAdvEquipo(View v){
        Bundle bundle=getIntent().getExtras();
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        Equipo equipo= new Equipo();
        equipo.setSerialEquipo(bundle.getString("nombre"));
        String mensaje= helper.eliminar2Equipo(equipo);
        helper.close();
        Intent intentM= new Intent(this,EquipoMenuActivity.class);
        this.startActivity(intentM);
    }



    public void elimincarCancelarEquipo(View v){
        Intent intentCancelar = new Intent(this, EquipoMenuActivity.class);
        this.startActivity(intentCancelar);
    }

}
