package edu.ues.fia.inventarioues.Vistas.Graficos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Prestamo.PrestamoMenuActivity;

public class TendenciasActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    String [] menu={"Equipos Tendencia", "Documento Tendencia"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tendencias);

        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, menu);
        ListView listView = findViewById(R.id.tendenciasId);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                Intent intentEquipo = new Intent(this, GraficosActivity.class);
                String sentencia1="SELECT serialEquipo, COUNT(serialEquipo)\n" +
                        "FROM movimientoInventario\n" +
                        "GROUP BY serialEquipo\n" +
                        "ORDER BY COUNT(serialEquipo) DESC\n" +
                        "LIMIT 5;";
                intentEquipo.putExtra("sentencia",sentencia1);
                intentEquipo.putExtra("nombre","Equipo");
                this.startActivity(intentEquipo);
                break;

            case 1:
                Intent intentDocumento = new Intent(this, GraficosActivity.class);
                String sentencia2="SELECT idDocumento, COUNT(idDocumento)\n" +
                        "FROM movimientoInventario\n" +
                        "GROUP BY idDocumento\n" +
                        "ORDER BY COUNT(idDocumento) DESC\n" +
                        "LIMIT 5;";
                intentDocumento.putExtra("sentencia",sentencia2);
                intentDocumento.putExtra("nombre","Documento");
                this.startActivity(intentDocumento);
                break;

        }
    }
}
