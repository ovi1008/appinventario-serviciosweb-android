package edu.ues.fia.inventarioues.Vistas.TipoAutor;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.TipoAutorAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoAutor;
import edu.ues.fia.inventarioues.R;

public class TipoAutorMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_autor_menu);
        ListView listView =(ListView)findViewById(R.id.listTipoAutor);
        List<TipoAutor> lista= listadoTipoAutores();
        TipoAutorAdaptador tipoAutorAdaptador=new TipoAutorAdaptador(this,lista);
        listView.setAdapter(tipoAutorAdaptador);
    }


    public List<TipoAutor> listadoTipoAutores(){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        Cursor c =helper.obtenerTipoAutores();
        List<TipoAutor> tipoAutorList= new ArrayList<>();
        while (c.moveToNext()){
            TipoAutor tipoAutor = new TipoAutor();
            
            tipoAutor.setIdTipoAutor(c.getInt(0));
            tipoAutor.setNombreTipoAutor(c.getString(1));
            tipoAutorList.add(tipoAutor);
        }
        return tipoAutorList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_autor_menu_insertar:
                Intent intent0= new Intent(this, TipoAutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_autor_menu_consultar:
                Intent intent1= new Intent(this, TipoAutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_autor_menu_Editar:
                Intent intente= new Intent(this, TipoAutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_autor_menu_eliminar:
                Intent intent2= new Intent(this, TipoAutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_autor_listar:
                Intent intent3= new Intent(this, TipoAutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
