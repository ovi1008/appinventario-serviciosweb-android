package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.Docente;
import edu.ues.fia.inventarioues.R;
public class DocenteAdaptador extends ArrayAdapter<Docente> {

    public DocenteAdaptador(@NonNull Context context, @NonNull List<Docente> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(edu.ues.fia.inventarioues.R.layout.listado_personalizado_docente,parent,false);
        }
        TextView carnetDocente=convertView.findViewById(R.id.docenteListarId);
        TextView nombreDocente=convertView.findViewById(R.id.docenteListarNombre);
        TextView apellidoDocente=convertView.findViewById(R.id.docenteListarApellido);

        Docente docente=getItem(position);
        carnetDocente.setText(docente.getCarnetDocente());
        nombreDocente.setText(docente.getNombreDocente());
        apellidoDocente.setText(docente.getApellidoDocente());

        return convertView;
    }
}