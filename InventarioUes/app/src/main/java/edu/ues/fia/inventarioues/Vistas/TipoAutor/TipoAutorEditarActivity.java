package edu.ues.fia.inventarioues.Vistas.TipoAutor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoAutor;
import edu.ues.fia.inventarioues.R;

public class TipoAutorEditarActivity extends AppCompatActivity {

    EditText editId, editNombre, editBusca;
    String clave="";
    int opcion=0;
    Button busca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_autor_editar);

        editId = (EditText) findViewById(R.id.TipoAutorEditarId);
        editNombre = (EditText) findViewById(R.id.TipoAutorEditarNombre);
        editBusca=(EditText) findViewById(R.id.TipoAutorEditarSearch);
        busca=(Button)findViewById(R.id.TipoAutorEditarBtnConsultar);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            editBusca.setVisibility(View.INVISIBLE);
            busca.setVisibility(View.INVISIBLE);
        }
    }

    public void buscaTipoAutorEditar(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        TipoAutor tipoAutor=helper.tipoAutorConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (tipoAutor==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(tipoAutor.getIdTipoAutor()));
            editNombre.setText(tipoAutor.getNombreTipoAutor());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }

    public void actualizarTipoAutor(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasV helper= TablasV.getInstance(getApplicationContext());
            TipoAutor tipoAutor = new TipoAutor();
            tipoAutor.setIdTipoAutor(Integer.valueOf(editId.getText().toString()));
            tipoAutor.setNombreTipoAutor(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarTipoAutor(tipoAutor,clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intOp1=new Intent(this,TipoAutorMenuActivity.class);
                this.startActivity(intOp1);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);
            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_autor_menu_insertar:
                Intent intent0= new Intent(this, TipoAutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_autor_menu_consultar:
                Intent intent1= new Intent(this, TipoAutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_autor_menu_Editar:
                Intent intente= new Intent(this, TipoAutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_autor_menu_eliminar:
                Intent intent2= new Intent(this, TipoAutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_autor_listar:
                Intent intent3= new Intent(this, TipoAutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
