package edu.ues.fia.inventarioues.Vistas.TipoEquipo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoEquipoEditarActivity extends AppCompatActivity {
    EditText editId, editNombre, editBusca,editNombreServer;
    String clave="";
    Button busca, buscarServidor;
    int opcion=0;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_equipo_editar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editId=(EditText)findViewById(R.id.TipoEquipoEditarId);
        editNombre=(EditText)findViewById(R.id.TipoEquipoEditarNombre);
        editBusca=(EditText) findViewById(R.id.TipoEquipoEditarSearch);
        busca=(Button)findViewById(R.id.TipoEquipoEditarBtnBuscar);
        buscarServidor=findViewById(R.id.TipoEquipoEditarBtnServer);
        editNombreServer=findViewById(R.id.TipoEquipoEditarNombreServidor);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            editNombreServer.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            busca.setVisibility(View.INVISIBLE);
            editBusca.setVisibility(View.INVISIBLE);
            buscarServidor.setVisibility(View.INVISIBLE);
            editNombreServer.setVisibility(View.INVISIBLE);

        }
    }

    public void editarBuscarTipoEquipoServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoEquipo/ws_tipoEquipo_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_query.php?";

                String url = url1+ip+url3+
                        "nombreTipoEquipo="+editNombreServer.getText().toString().toUpperCase();
                String jsonTipoEquipo= ServiciosWeb.consultarServidorLocal(url, this);
                if (jsonTipoEquipo==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datoTipoEquipo=jsonTipoEquipo.split(",");
                    String []idTipoEquipo=datoTipoEquipo[0].split(":");
                    String []nombreTipoEquipo=datoTipoEquipo[1].split(":");
                    TipoEquipo marcaS = new TipoEquipo();
                    marcaS.setIdTipoEquipo(Integer.valueOf(idTipoEquipo[1]));
                    marcaS.setNombreTipoEquipo(nombreTipoEquipo[1]);
                    editId.setText(String.valueOf(marcaS.getIdTipoEquipo()));
                    editNombre.setText(marcaS.getNombreTipoEquipo());

                    editId.setEnabled(true);
                    editNombre.setEnabled(true);
                    clave=editBusca.getText().toString().toUpperCase();
                    editBusca.setEnabled(false);
                    busca.setEnabled(false);
                    editNombreServer.setEnabled(false);
                    buscarServidor.setEnabled(false);
                }


            }

        }
    }

    public void editarTipoEquipoServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/tipoEquipo/ws_tipoEquipo_update.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_update.php?";

        String url = url1+ip+url3+
                "nombreTipoEquipo="+editNombreServer.getText().toString().toUpperCase()+
                "&nombreTipoEquipoN="+editNombre.getText().toString().toUpperCase();
        String msj= ServiciosWeb.editarServidorLocal(url, this);
        Toast.makeText(this,msj+"el tipo equipo: "+editNombreServer.getText().toString().toUpperCase(),Toast.LENGTH_LONG).show();
        editId.setText("");
        editNombre.setText("");
        editBusca.setText("");
        clave="";
        editId.setEnabled(false);
        editNombre.setEnabled(false);
        busca.setEnabled(true);
        editBusca.setEnabled(true);
        editNombreServer.setText("");
        editNombreServer.setEnabled(true);
        buscarServidor.setEnabled(true);


    }

    public void buscaTipoEquipoEditar(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        TipoEquipo tipoEquipoa=helper.tipoEquipoConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (tipoEquipoa==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(tipoEquipoa.getIdTipoEquipo()));
            editNombre.setText(tipoEquipoa.getNombreTipoEquipo());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    public void actualizarTipoEquipo(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper= TablasJ.getInstance(getApplicationContext());
            TipoEquipo tipoEquipo = new TipoEquipo();
            tipoEquipo.setIdTipoEquipo(Integer.valueOf(editId.getText().toString()));
            tipoEquipo.setNombreTipoEquipo(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarTipoEquipo(tipoEquipo, clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intentEdi=new Intent(this,TipoEquipoMenuActivity.class);
                this.startActivity(intentEdi);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);

            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_equipo_menu_insertar:
                Intent intent0= new Intent(this, TipoEquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_equipo_menu_consultar:
                Intent intent1= new Intent(this, TipoEquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_menu_menu_Editar:
                Intent intentEd= new Intent(this, TipoEquipoEditarActivity.class);
                this.startActivity(intentEd);
                return true;


            case R.id.action_tipo_equipo_menu_eliminar:
                Intent intent2= new Intent(this, TipoEquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_equipo_listar:
                Intent intent3= new Intent(this, TipoEquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
            case R.id.action_tipo_equipo_listarServidor:
                Intent intent4= new Intent(this, TipoEquipoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_equipo_listarSincronizar:
                Intent intent5= new Intent(this, TipoEquipoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
