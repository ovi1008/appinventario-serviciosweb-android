package edu.ues.fia.inventarioues.Controladores;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.media.Image;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.R;

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context context;

    public FingerprintHandler(Context context){

        this.context = context;

    }

    public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject){

        CancellationSignal cancellationSignal = new CancellationSignal();
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);

    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {

        this.update("There was an Auth Error. " + errString, false);

    }

    @Override
    public void onAuthenticationFailed() {

        this.update("Auth Failed.", false);

    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {

        this.update("Error: " + helpString, false);

    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

        this.update("You can now access the app.", true);

    }

    private void update(String s, boolean b) {

        //TextView paraLabel = (TextView) ((Activity)context).findViewById(R.id.userUsuario);

        //paraLabel.setText(s);

        if (s.equals("Auth Failed.")){
            Toast.makeText(context,"Usted no es administrador",Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper = TablasJ.getInstance(context);
            helper.open();
            Usuario usuario=helper.usuarioConsultar("ovi10", "MA13021");
            helper.close();
            if(usuario==null){
                Toast.makeText(context,"Datos de usuario y contraseña incorrectos o no exiten.",Toast.LENGTH_LONG).show();
            }else {
                Intent inte = new Intent(context, MenuPrincipalActivity.class);
                context.startActivity(inte);
            }
        }





    }
}