package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Editorial.EditorialBuscarActivity;

@SuppressLint("NewApi")
public class TipoDocumentoConsultarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    EditText editId, editNombre,editNombreServer;
    SearchView searchConsultar;
    TextToSpeech toSpeech;
    int result;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_consultar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editId=(EditText)findViewById(R.id.TipoDocumentoConsultarId);
        editNombre=(EditText)findViewById(R.id.TipoDocumentoConsultarNombre);
        searchConsultar = (SearchView) findViewById(R.id.TipoDocumento_searchConsultar);
        editNombreServer=findViewById(R.id.TipoDocumentoConsultarNombreServidor);
        searchConsultar.setOnQueryTextListener(this);

        toSpeech=new TextToSpeech(TipoDocumentoConsultarActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    result=toSpeech.setLanguage(Locale.getDefault());
                }else{
                    Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void playTipoDocumento(View v){
        if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
            Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
        }else {
            String textoHablar;
            if (editId.getText().toString().isEmpty()){
                textoHablar="Realice una consulta primero";
            }else {
                textoHablar="Se consulta el siguiente tipo documento:\n\n" +
                        "Codigo del tipo documento es: " +editId.getText().toString()+"\n\n"+
                        "El nombre del tipo documento es: "+editNombre.getText().toString();
            }
            toSpeech.speak(textoHablar,TextToSpeech.QUEUE_FLUSH,null);
        }
    }

    public void stopTipoDocumento(View v){
        if (toSpeech!=null){
            toSpeech.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (toSpeech!=null){
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }



    public void consultarTipoDocumentoServidor(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                    String url1="http://";
                    String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_query.php?";
//                  String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_query.php?";

                    String url = url1+ip+url3+
                        "nombreTipoDocumento="+editNombreServer.getText().toString().toUpperCase();
                    String jsonTipoDocumento= ServiciosWeb.consultarServidorLocal(url, this);
                    editNombreServer.setText("");
                    if (jsonTipoDocumento==null)
                    {
                     Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(this,  getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                        String []datoTipoDocumento=jsonTipoDocumento.split(",");
                        String []idTipoDocumento=datoTipoDocumento[0].split(":");
                        String []nombreTipoDocumento=datoTipoDocumento[1].split(":");
                        TipoDocumento td = new TipoDocumento();
                        td.setIdTipoDocumento(Integer.valueOf(idTipoDocumento[1]));
                        td.setNombreTipoDocumento(nombreTipoDocumento[1]);
                        editId.setText(String.valueOf(td.getIdTipoDocumento()));
                        editNombre.setText(td.getNombreTipoDocumento());

                    }

                }

        }
    }

    public void eliminarConsultarTipoDocumentoServidor(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombre.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_delete.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_delete.php?";

                String url = url1+ip+url3+
                        "nombreTipoDocumento="+editNombre.getText().toString().toUpperCase();
                String msj= ServiciosWeb.eliminarServidorLocal(url, this);

                Toast.makeText(this, msj+"el tipo equipo: "+editNombre.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();

                editNombre.setText("");
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasM helper= TablasM.getInstance(getApplicationContext());
        TipoDocumento tipoDocumento= new TipoDocumento();
        helper.open();
        tipoDocumento=helper.tipoDocumentoConsultar(newText.toUpperCase());
        helper.close();
        if (tipoDocumento==null){
            editNombre.setText("");
            editId.setText("");
        }else {
            editId.setText(String.valueOf(tipoDocumento.getIdTipoDocumento()));
            editNombre.setText(tipoDocumento.getNombreTipoDocumento());
        }
        return false;
    }

    public void buscarEditarTipoDocumento(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else{
            Intent intentEditar= new Intent(this, TipoDocumentoEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarTipoDocumento(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper = TablasM.getInstance(getApplicationContext());
            helper.open();
            TipoDocumento tipoDocumento= new TipoDocumento();
            tipoDocumento.setIdTipoDocumento(Integer.valueOf(editId.getText().toString()));
            tipoDocumento.setNombreTipoDocumento(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1TipoDocumento(tipoDocumento);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, TipoDocumentoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", tipoDocumento.getNombreTipoDocumento());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_tipo_documento_listarServidor:
                Intent intent4= new Intent(this, TipoDocumentoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_documento_listarSincronizar:
                Intent intent5= new Intent(this, TipoDocumentoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}