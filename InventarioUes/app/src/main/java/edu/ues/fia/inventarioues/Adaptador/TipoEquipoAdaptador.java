package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;

public class TipoEquipoAdaptador extends ArrayAdapter<TipoEquipo> {

    public TipoEquipoAdaptador(@NonNull Context context, @NonNull List<TipoEquipo> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_tipo_equipo,parent,false);
        }
        TextView idTipoEquipo=convertView.findViewById(R.id.ListarTipoEquipoId);
        TextView nombreTipoEquipo=convertView.findViewById(R.id.ListarTipoEquipoNombre);

        TipoEquipo tipoEquipo=getItem(position);
        idTipoEquipo.setText(String.valueOf(tipoEquipo.getIdTipoEquipo()));
        nombreTipoEquipo.setText(tipoEquipo.getNombreTipoEquipo());

        return convertView;
    }
}
