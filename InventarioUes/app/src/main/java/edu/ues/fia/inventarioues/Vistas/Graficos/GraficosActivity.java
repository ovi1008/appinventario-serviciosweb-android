package edu.ues.fia.inventarioues.Vistas.Graficos;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.R;

public class GraficosActivity extends AppCompatActivity {
    BarChart graficaBarras;
    String msj="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graficos);
        graficaBarras=findViewById(R.id.graficaL);

        Bundle bundle=getIntent().getExtras();

        TablasJ helper =TablasJ.getInstance(getApplicationContext());
        helper.open();
        Cursor c=helper.obtenerConteo(bundle.getString("sentencia"));
        List<BarEntry> b=new ArrayList<>();
        ArrayList xVals= new ArrayList();
        List<String> nomEjex=new ArrayList<>();
        Integer i=0, j=1;
        while (c.moveToNext()){
            nomEjex.add(c.getString(0));
            b.add(new BarEntry(i,c.getInt(1)));
            i++;
        }
        for (String llave:nomEjex){
            msj=msj+"h"+String.valueOf(j)+": "+llave+"\n\n";
            j++;
        }
        helper.close();

        BarDataSet datos=new BarDataSet(b,"Graficas de barra"+bundle.getString("nombre"));
        BarData data=new BarData(datos);
        datos.setColors(ColorTemplate.COLORFUL_COLORS);
        data.setBarWidth(0.9f);
        graficaBarras.setData(data);
        graficaBarras.setFitBars(true);
        graficaBarras.invalidate();

        xVals.add("h1");
        xVals.add("h2");
        xVals.add("h3");
        xVals.add("h4");
        xVals.add("h5");

        Legend l = graficaBarras.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(true);
        l.setYOffset(20f);
        l.setXOffset(0f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);

        XAxis xAxis = graficaBarras.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        xAxis.setAxisMaximum(6);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));

        graficaBarras.getAxisRight().setEnabled(false);
        YAxis leftAxis = graficaBarras.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f);

    }

    public void mostrarLabel(View v){
        Toast.makeText(this,msj,Toast.LENGTH_LONG).show();
    }
}
