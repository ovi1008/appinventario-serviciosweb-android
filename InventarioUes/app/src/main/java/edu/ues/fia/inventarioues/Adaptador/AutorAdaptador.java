package edu.ues.fia.inventarioues.Adaptador;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.Autor;

import edu.ues.fia.inventarioues.R;


public class AutorAdaptador extends ArrayAdapter<Autor> {

    public AutorAdaptador(@NonNull Context context, @NonNull List<Autor> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_autor,parent,false);
        }
        TextView idAutor=convertView.findViewById(R.id.autorlistarId);
        TextView nombreAutor=convertView.findViewById(R.id.autorListarNombre);

        Autor autor=getItem(position);
        idAutor.setText(String.valueOf(autor.getIdAutor()));
        nombreAutor.setText(autor.getNombreAutor());

        return convertView;
    }
}
