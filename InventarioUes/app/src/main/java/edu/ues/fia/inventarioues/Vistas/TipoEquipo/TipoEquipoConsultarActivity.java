package edu.ues.fia.inventarioues.Vistas.TipoEquipo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Marca.MarcaBuscarActivity;

@SuppressLint("NewApi")
public class TipoEquipoConsultarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    EditText editId, editNombre, editNombreServer;
    SearchView searchConsultar;
    TextToSpeech toSpeech;
    int result;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_equipo_consultar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editId=(EditText)findViewById(R.id.TipoEquipoConsultarId);
        editNombre=(EditText)findViewById(R.id.TipoEquipoConsultarNombre);
        searchConsultar = (SearchView) findViewById(R.id.TipoEquipo_searchConsultar);
        editNombreServer=findViewById(R.id.TipoEquipoConsultarNombreServidor);
        searchConsultar.setOnQueryTextListener(this);

        toSpeech=new TextToSpeech(TipoEquipoConsultarActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    result=toSpeech.setLanguage(Locale.getDefault());
                }else{
                    Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void playTipoEquipo(View v){
        if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
            Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
        }else {
            String textoHablar;
            if (editId.getText().toString().isEmpty()){
                textoHablar="Realice una consulta primero";
            }else {
                textoHablar="Se consulta el siguiente tipo equipo:\n\n" +
                        "Codigo del tipo de equipo es: " +editId.getText().toString()+"\n\n"+
                        "El nombre del tipo de equipo es: "+editNombre.getText().toString();
            }
            toSpeech.speak(textoHablar,TextToSpeech.QUEUE_FLUSH,null);
        }
    }

    public void stopTipoEquipo(View v){
        if (toSpeech!=null){
            toSpeech.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (toSpeech!=null){
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }


    public void consultarTipoEquipoServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoEquipo/ws_tipoEquipo_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_query.php?";

                String url = url1+ip+url3+
                        "nombreTipoEquipo="+editNombreServer.getText().toString().toUpperCase();
                String jsonTipoEquipo= ServiciosWeb.consultarServidorLocal(url, this);
                editNombreServer.setText("");
                if (jsonTipoEquipo==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datoTipoEquipo=jsonTipoEquipo.split(",");
                    String []idTipoEquipo=datoTipoEquipo[0].split(":");
                    String []nombreTipoEquipo=datoTipoEquipo[1].split(":");
                    TipoEquipo marcaS = new TipoEquipo();
                    marcaS.setIdTipoEquipo(Integer.valueOf(idTipoEquipo[1]));
                    marcaS.setNombreTipoEquipo(nombreTipoEquipo[1]);
                    editId.setText(String.valueOf(marcaS.getIdTipoEquipo()));
                    editNombre.setText(marcaS.getNombreTipoEquipo());
                }


            }

        }
    }

    public void eliminarConsultarTipoEquipoServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombre.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoEquipo/ws_tipoEquipo_delete.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_delete.php?";

                String url = url1+ip+url3+
                        "nombreTipoEquipo="+editNombre.getText().toString().toUpperCase();
                String msj= ServiciosWeb.eliminarServidorLocal(url, this);

                Toast.makeText(this, msj+"el tipo equipo: "+editNombre.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();

                editNombre.setText("");
            }

        }
    }



    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        TipoEquipo tipoEquipo= new TipoEquipo();
        helper.open();
        tipoEquipo=helper.tipoEquipoConsultar(newText.toUpperCase());
        helper.close();
        if (tipoEquipo==null){
            editNombre.setText("");
            editId.setText("");
        }else {
            editId.setText(String.valueOf(tipoEquipo.getIdTipoEquipo()));
            editNombre.setText(tipoEquipo.getNombreTipoEquipo());
        }
        return false;
    }

    public void buscarEditarTipoEquipo(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else{
            Intent intentEditar= new Intent(this, TipoEquipoEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarTipoEquipo(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper = TablasJ.getInstance(getApplicationContext());
            helper.open();
            TipoEquipo tipoEquipo= new TipoEquipo();
            tipoEquipo.setIdTipoEquipo(Integer.valueOf(editId.getText().toString()));
            tipoEquipo.setNombreTipoEquipo(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1TipoEquipo(tipoEquipo);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, TipoEquipoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", tipoEquipo.getNombreTipoEquipo());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_equipo_menu_insertar:
                Intent intent0= new Intent(this, TipoEquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_equipo_menu_consultar:
                Intent intent1= new Intent(this, TipoEquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_menu_menu_Editar:
                Intent intentEd= new Intent(this, TipoEquipoEditarActivity.class);
                this.startActivity(intentEd);
                return true;


            case R.id.action_tipo_equipo_menu_eliminar:
                Intent intent2= new Intent(this, TipoEquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_equipo_listar:
                Intent intent3= new Intent(this, TipoEquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
            case R.id.action_tipo_equipo_listarServidor:
                Intent intent4= new Intent(this, TipoEquipoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_equipo_listarSincronizar:
                Intent intent5= new Intent(this, TipoEquipoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
