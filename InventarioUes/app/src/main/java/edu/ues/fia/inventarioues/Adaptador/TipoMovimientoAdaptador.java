package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.Modelos.TipoMovimiento;
import edu.ues.fia.inventarioues.R;

public class TipoMovimientoAdaptador extends ArrayAdapter<TipoMovimiento> {

    public TipoMovimientoAdaptador(@NonNull Context context, @NonNull List<TipoMovimiento> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_tipo_movimiento,parent,false);
        }
        TextView idTipoMovimiento=convertView.findViewById(R.id.ListarTipoMovimientoId);
        TextView nombreTipoMovimiento=convertView.findViewById(R.id.ListarTipoMovimientoNombre);

        TipoMovimiento tipoMovimiento=getItem(position);
        idTipoMovimiento.setText(String.valueOf(tipoMovimiento.getIdTipoMovimiento()));
        nombreTipoMovimiento.setText(tipoMovimiento.getNombreTipoMovimiento());

        return convertView;
    }
}