package edu.ues.fia.inventarioues.Vistas.TipoAutor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoAutor;
import edu.ues.fia.inventarioues.R;

public class TipoAutorEliminarAdvertenciaActivity extends AppCompatActivity {

    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_autor_eliminar_advertencia);

        msj=(TextView)findViewById(R.id.TipoAutorEliminarAdvMens);
        Bundle bundle=getIntent().getExtras();
        msj.setText(getResources().getString(R.string.tipoAutorMsj2)+" "+
                bundle.getString("nombre")+"\n" +getResources().getString(R.string.tipoAutorMsj3)+" "+
                bundle.getString("msj"));

    }

    public void eliminarAdvTipoAutor(View v){
        Bundle bundle=getIntent().getExtras();
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        TipoAutor tipoAutor= new TipoAutor ();
        tipoAutor.setNombreTipoAutor(bundle.getString("nombre"));
        String mensaje= helper.eliminar2TipoAutor(tipoAutor);
        helper.close();
        Intent intentM= new Intent(this,TipoAutorMenuActivity.class);
        this.startActivity(intentM);
    }

    public void elimincarCancelarTipoAutor(View v){
        Intent intentCancelar = new Intent(this, TipoAutorMenuActivity.class);
        this.startActivity(intentCancelar);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_autor_menu_insertar:
                Intent intent0= new Intent(this, TipoAutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_autor_menu_consultar:
                Intent intent1= new Intent(this, TipoAutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_autor_menu_Editar:
                Intent intente= new Intent(this, TipoAutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_autor_menu_eliminar:
                Intent intent2= new Intent(this, TipoAutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_autor_listar:
                Intent intent3= new Intent(this, TipoAutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
