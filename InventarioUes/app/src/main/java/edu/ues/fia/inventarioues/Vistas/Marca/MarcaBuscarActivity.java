package edu.ues.fia.inventarioues.Vistas.Marca;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class MarcaBuscarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    EditText editId,editNombre, editNombreServer;
    SearchView searchConsultar;
    TextToSpeech toSpeech;
    int result;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marca_buscar);
        searchConsultar=(SearchView) findViewById(R.id.Marca_searchConsultar);
        editId=(EditText)findViewById(R.id.MarcaConsultarId);
        editNombreServer=findViewById(R.id.MarcaConsultarNombreServidor);
        editNombre=(EditText)findViewById(R.id.MarcaConsultarNombre);
        searchConsultar.setOnQueryTextListener(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        toSpeech=new TextToSpeech(MarcaBuscarActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    result=toSpeech.setLanguage(Locale.getDefault());
                }else{
                    Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void playMarca(View v){
        if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
            Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
        }else {
            String textoHablar;
            if (editId.getText().toString().isEmpty()){
                textoHablar="Realice una consulta primero";
            }else {
                textoHablar="Se consulta la siguiente marca:\n\n" +
                        "Codigo de la marca es: " +editId.getText().toString()+"\n\n"+
                        "El nombre de la marca es: "+editNombre.getText().toString();
            }
            toSpeech.speak(textoHablar,TextToSpeech.QUEUE_FLUSH,null);
        }
    }

    public void stopMarca(View v){
        if (toSpeech!=null){
            toSpeech.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (toSpeech!=null){
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }

    public void consultarMarcaServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/marca/ws_marca_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_query.php?";

                String url = url1+ip+url3+
                        "nombreMarca="+editNombreServer.getText().toString().toUpperCase();
                String jsonMarca= ServiciosWeb.consultarServidorLocal(url, this);
                editNombreServer.setText("");
                if (jsonMarca==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datosMarca=jsonMarca.split(",");
                    String []idMarca=datosMarca[0].split(":");
                    String []nombreMarca=datosMarca[1].split(":");
                    Marca marcaS = new Marca();
                    marcaS.setIdMarca(Integer.valueOf(idMarca[1]));
                    marcaS.setNombreMarca(nombreMarca[1]);
                    editId.setText(String.valueOf(marcaS.getIdMarca()));
                    editNombre.setText(marcaS.getNombreMarca());
                }


            }

        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        Marca marca= new Marca();
        helper.open();
        marca=helper.marcaConsultar(newText.toUpperCase());
        helper.close();
        if (marca==null){
            editId.setText("");
            editNombre.setText("");
        }else {
            editId.setText(String.valueOf(marca.getIdMarca()));
            editNombre.setText(marca.getNombreMarca());
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void buscarEditarMarca(View v){
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            Intent intentEditar= new Intent(this, MarcaEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarMarca(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Marca marca= new Marca();
            marca.setIdMarca(Integer.valueOf(editId.getText().toString()));
            marca.setNombreMarca(editNombre.getText().toString());
            String mensaje= helper.eliminar1Marca(marca);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,MarcaEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", marca.getNombreMarca());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }

    public void eliminarBuscarMarcaServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombre.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/marca/ws_marca_delete.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_delete.php?";

                String url = url1+ip+url3+
                        "nombreMarca="+editNombre.getText().toString().toUpperCase();
                String msj= ServiciosWeb.eliminarServidorLocal(url, this);

                Toast.makeText(this, msj+"la marca: "+editNombre.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();

                editNombre.setText("");
                editId.setText("");
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_marca,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_marca_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_marca_menu_insertar:
                Intent intent0= new Intent(this, MarcaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_marca_menu_Editar:
                Intent intente= new Intent(this, MarcaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_marca_menu_consultar:
                Intent intent1= new Intent(this, MarcaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_marca_menu_eliminar:
                Intent intent2= new Intent(this, MarcaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_marca_listar:
                Intent intent3= new Intent(this, MarcaMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_marca_listarServidor:
                Intent intent4= new Intent(this, MarcaListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_marca_listarSincronizar:
                Intent intent5= new Intent(this, MarcaSincronizacionActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
