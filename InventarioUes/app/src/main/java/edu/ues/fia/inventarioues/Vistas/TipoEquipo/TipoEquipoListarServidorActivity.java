package edu.ues.fia.inventarioues.Vistas.TipoEquipo;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import edu.ues.fia.inventarioues.Adaptador.TipoEquipoAdaptador;
import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoEquipoListarServidorActivity extends AppCompatActivity {

    EditText editFecha;
    ListView listTipoEquipo;
    NumberPicker horaMinima, minutoMinimo, segundoMinimo;
    String h="0",m="0",s="0";
    String CERO = "0", DOS_PUNTOS = ":";
    Calendar calendar=Calendar.getInstance();
    List<TipoEquipo> listadoTipoEquipo=new ArrayList<>();
    List<TipoEquipo> vaciar=new ArrayList<>();

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_equipo_listar_servidor);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        editFecha=findViewById(R.id.TipoEquipoServidorListarFecha);
        horaMinima=findViewById(R.id.TipoEquipoServidorListarHoraMinima);
        minutoMinimo=findViewById(R.id.TipoEquipoServidorListarMinutoMinima);
        segundoMinimo=findViewById(R.id.TipoEquipoServidorListaSegundoMinima);

        listTipoEquipo=findViewById(R.id.listTipoEquipoServidor);

        editFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(TipoEquipoListarServidorActivity.this,fechaMinima, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //Hora
        horaMinima.setMinValue(0);
        horaMinima.setMaxValue(23);
        horaMinima.setWrapSelectorWheel(true);
        horaMinima.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                h =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });

        //Minuto
        minutoMinimo.setMinValue(0);
        minutoMinimo.setMaxValue(59);
        minutoMinimo.setWrapSelectorWheel(true);
        minutoMinimo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                m =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });
        //Segundo
        segundoMinimo.setMinValue(0);
        segundoMinimo.setMaxValue(59);
        segundoMinimo.setWrapSelectorWheel(true);
        segundoMinimo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                s =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });


    }



    DatePickerDialog.OnDateSetListener fechaMinima = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            String formatoFecha = "yyyy-MM-dd";
            SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.getDefault());
            editFecha.setText(format.format(calendar.getTime()));
        }
    };

    public void consultarTipoEquipoPorFechaServidor(View v){
        listadoTipoEquipo.removeAll(vaciar);
        String tiempoFormateado="%20"+h+":"+m+":"+s;
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editFecha.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.tipoDeEquipoMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoEquipo/ws_tipoEquipo_queryDate.php?";
                //            String urlLocal = "http://192.168.0.11/inventarioUes/tipoEquipo/ws_tipoEquipo_queryDate.php?";

                String url = url1+ip+url3+
                        "fechaModificadaTipoEquipo="+editFecha.getText().toString()+tiempoFormateado;
                String json= ServiciosWeb.consultarFechaLocal(url,this);
                if (json.length()==0){}else{
                    String[]datosTipoEquipo=json.split("&&");
                    for (String dato:datosTipoEquipo){
                        String []datoP=dato.split(",");
                        String []idTipoEquipo=datoP[0].split(":");
                        String []nombreTipoEquipo=datoP[1].split(":");
                        TipoEquipo tipoEquipo=new TipoEquipo();
                        //camposTipoEquipo= new String[]{"idTipoEquipo", "nombreTipoEquipo"}
                       tipoEquipo.setIdTipoEquipo(Integer.valueOf(idTipoEquipo[1]));
                       tipoEquipo.setNombreTipoEquipo(nombreTipoEquipo[1]);
                        listadoTipoEquipo.add(tipoEquipo);
                        vaciar.add(tipoEquipo);
                    }
                    TipoEquipoAdaptador tipoEquipoAdaptador=new TipoEquipoAdaptador(this,listadoTipoEquipo);
                    listTipoEquipo.setAdapter(tipoEquipoAdaptador);

                }
            }

        }
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_equipo_menu_insertar:
                Intent intent0= new Intent(this, TipoEquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_menu_menu_Editar:
                Intent intentEd= new Intent(this, TipoEquipoEditarActivity.class);
                this.startActivity(intentEd);
                return true;

            case R.id.action_tipo_equipo_menu_consultar:
                Intent intent1= new Intent(this, TipoEquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_tipo_equipo_menu_eliminar:
                Intent intent2= new Intent(this, TipoEquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_equipo_listar:
                Intent intent3= new Intent(this, TipoEquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
            case R.id.action_tipo_equipo_listarServidor:
                Intent intent4= new Intent(this, TipoEquipoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_equipo_listarSincronizar:
                Intent intent5= new Intent(this, TipoEquipoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
