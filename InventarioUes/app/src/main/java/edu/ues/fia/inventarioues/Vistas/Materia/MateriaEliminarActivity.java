package edu.ues.fia.inventarioues.Vistas.Materia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Materia;
import edu.ues.fia.inventarioues.R;


public class MateriaEliminarActivity extends AppCompatActivity {
    EditText editNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_eliminar);
        editNombre=(EditText)findViewById(R.id.MateriaEliminarNombre1);
    }

    public void eliminarDeleteMateria(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.materiaMsj1a),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Materia materia= new Materia();
            materia.setIdMateria(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1Materia(materia);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, MateriaEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("id", materia.getIdMateria());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editNombre.setText("");
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_materia,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_materia_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_materia_menu_insertar:
                Intent intent0= new Intent(this, MateriaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_materia_menu_consultar:
                Intent intent1= new Intent(this, MateriaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_materia_menu_Editar:
                Intent intente= new Intent(this, MateriaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_materia_menu_eliminar:
                Intent intent2= new Intent(this, MateriaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_materia_listar:
                Intent intent3= new Intent(this, MateriaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
