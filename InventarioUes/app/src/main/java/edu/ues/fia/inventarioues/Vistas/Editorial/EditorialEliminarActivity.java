package edu.ues.fia.inventarioues.Vistas.Editorial;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class EditorialEliminarActivity extends AppCompatActivity {

    EditText editNombreEditorial;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorial_eliminar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editNombreEditorial=(EditText)findViewById(R.id.EditorialEliminarNombre1);

    }

    public void eliminarEditorialServidor(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreEditorial.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.editorialMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/editorial/ws_editorial_delete.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/editorial/ws_editorial_delete.php?";

                String url = url1+ip+url3+
                        "nombreEditorial="+editNombreEditorial.getText().toString().toUpperCase();
                String msj= ServiciosWeb.eliminarServidorLocal(url, this);
                Toast.makeText(this, msj+"la editorial: "+editNombreEditorial.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();
                editNombreEditorial.setText("");
            }

        }
    }


    public void eliminarDeleteEditorial(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if (editNombreEditorial .getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.editorialMsj1),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Editorial editorial= new Editorial();
            editorial.setNombreEditorial(editNombreEditorial.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1Editorial(editorial);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,EditorialEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre",editorial.getNombreEditorial());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editNombreEditorial.setText("");
            }
        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editorial,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_editorial_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;



            case R.id.action_editorial_menu_insertar:
                Intent intent0= new Intent(this, EditorialInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_editorial_menu_Editar:
                Intent intente= new Intent(this, EditorialEditarActivity.class);
                this.startActivity(intente);
                return true;

            case R.id.action_editorial_menu_consultar:
                Intent intent1= new Intent(this, EditorialBuscarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_editorial_menu_eliminar:
                Intent intent2= new Intent(this, EditorialEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_editorial_listar:
                Intent intent3= new Intent(this, EditorialMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_editorial_listarServidor:
                Intent intent4= new Intent(this, EditorialListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_editorial_listarSincronizar:
                Intent intent5= new Intent(this, EditorialSincronizacionActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
