package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


import edu.ues.fia.inventarioues.Modelos.Motivo;
import edu.ues.fia.inventarioues.R;

public class MotivoAdaptador extends ArrayAdapter<Motivo> {


    public MotivoAdaptador(@NonNull Context context, @NonNull List<Motivo> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_motivo,parent,false);
        }
        TextView idMotivo=convertView.findViewById(R.id.motivoListarId);
        TextView nombreMotivo=convertView.findViewById(R.id.motivoListarNombre);

        Motivo motivo=getItem(position);
        idMotivo.setText(String.valueOf(motivo.getIdMotivo()));
        nombreMotivo.setText(motivo.getNombreMotivo());

        return convertView;
    }
}
