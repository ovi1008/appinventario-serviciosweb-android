package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;
import edu.ues.fia.inventarioues.R;

public class UnidadAdministrativaAdaptador extends ArrayAdapter<UnidadAdministrativa> {

    public UnidadAdministrativaAdaptador(@NonNull Context context, @NonNull List<UnidadAdministrativa> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_unidad_administrativa,parent,false);
        }
        TextView idUnidadAdministrativa=convertView.findViewById(R.id.ListarUnidadAdministrativaId);
        TextView nombreUnidadAdministrativa=convertView.findViewById(R.id.ListarUnidadAdministrativaNombre);

        UnidadAdministrativa unidadAdministrativa=getItem(position);
        idUnidadAdministrativa.setText(String.valueOf(unidadAdministrativa.getIdUnidadAdministrativa()));
        nombreUnidadAdministrativa.setText(unidadAdministrativa.getNombreUnidadAdministrativa());

        return convertView;
    }
}