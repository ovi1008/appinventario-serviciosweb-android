package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoDocumentoEditarActivity extends AppCompatActivity {

    EditText editId, editNombre, editBusca, editNombreServer;;
    String clave="";
    Button busca,buscarServidor;
    int opcion=0;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_editar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editId=(EditText)findViewById(R.id.TipoDocumentoEditarId);
        editNombre=(EditText)findViewById(R.id.TipoDocumentoEditarNombre);
        editBusca=(EditText) findViewById(R.id.TipoDocumentoEditarSearch);
        busca=(Button)findViewById(R.id.TipoDocumentoEditarBtnBuscar);
        buscarServidor=findViewById(R.id.TipoDocumentoEditarBtnServer);
        editNombreServer=findViewById(R.id.TipoDocumentoEditarNombreServidor);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            editNombreServer.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            busca.setVisibility(View.INVISIBLE);
            editBusca.setVisibility(View.INVISIBLE);
            buscarServidor.setVisibility(View.INVISIBLE);
            editNombreServer.setVisibility(View.INVISIBLE);

        }
    }

    public void editarBuscarTipoDocumentoServidor(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_query.php?";

                String url = url1+ip+url3+
                        "nombreTipoDocumento="+editNombreServer.getText().toString().toUpperCase();
                String jsonTipoDocumento= ServiciosWeb.consultarServidorLocal(url, this);
                if (jsonTipoDocumento==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datoTipoDocumento=jsonTipoDocumento.split(",");
                    String []idTipoDocumento=datoTipoDocumento[0].split(":");
                    String []nombreTipoDocumento=datoTipoDocumento[1].split(":");
                    TipoDocumento td = new TipoDocumento();
                    td.setIdTipoDocumento(Integer.valueOf(idTipoDocumento[1]));
                    td.setNombreTipoDocumento(nombreTipoDocumento[1]);
                    editId.setText(String.valueOf(td.getIdTipoDocumento()));
                    editNombre.setText(td.getNombreTipoDocumento());

                    editId.setEnabled(true);
                    editNombre.setEnabled(true);
                    clave=editBusca.getText().toString().toUpperCase();
                    editBusca.setEnabled(false);
                    busca.setEnabled(false);
                    editNombreServer.setEnabled(false);
                    buscarServidor.setEnabled(false);
                }


            }

        }
    }

    public void editarTipoDocumentoServidor(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_update.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_update.php?";

        String url = url1+ip+url3+
                "nombreTipoDocumento="+editNombreServer.getText().toString().toUpperCase()+
                "&nombreTipoDocumentoN="+editNombre.getText().toString().toUpperCase();
        String msj= ServiciosWeb.editarServidorLocal(url, this);
        Toast.makeText(this,msj+"el tipo documento: "+editNombreServer.getText().toString().toUpperCase(),Toast.LENGTH_LONG).show();
        editId.setText("");
        editNombre.setText("");
        editBusca.setText("");
        clave="";
        editId.setEnabled(false);
        editNombre.setEnabled(false);
        busca.setEnabled(true);
        editBusca.setEnabled(true);
        editNombreServer.setText("");
        editNombreServer.setEnabled(true);
        buscarServidor.setEnabled(true);


    }



    public void actualizarTipoDocumento(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper= TablasM.getInstance(getApplicationContext());
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setIdTipoDocumento(Integer.valueOf(editId.getText().toString()));
            tipoDocumento.setNombreTipoDocumento(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarTipoDocumento(tipoDocumento, clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intentEdi=new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intentEdi);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);

            }
        }

    }

    public void buscarTipoDocumentoEditar(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        TipoDocumento tipoDocumento=helper.tipoDocumentoConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (tipoDocumento==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(tipoDocumento.getIdTipoDocumento()));
            editNombre.setText(tipoDocumento.getNombreTipoDocumento());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_tipo_documento_listarServidor:
                Intent intent4= new Intent(this, TipoDocumentoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_documento_listarSincronizar:
                Intent intent5= new Intent(this, TipoDocumentoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}