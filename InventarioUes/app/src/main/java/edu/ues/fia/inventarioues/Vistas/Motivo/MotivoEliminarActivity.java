package edu.ues.fia.inventarioues.Vistas.Motivo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Motivo;
import edu.ues.fia.inventarioues.R;

public class MotivoEliminarActivity extends AppCompatActivity {

    EditText editNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motivo_eliminar);
        editNombre=(EditText)findViewById(R.id.MotivoEliminarNombre1);
    }

    public void eliminarDeleteMotivo(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.motivoMsj1a),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper = TablasM.getInstance(getApplicationContext());
            helper.open();
            Motivo motivo= new Motivo();
            motivo.setIdMotivo(Integer.valueOf(editNombre.getText().toString().toUpperCase()));
            String mensaje= helper.eliminar1Motivo(motivo);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, MotivoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", motivo.getIdMotivo());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editNombre.setText("");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_motivo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_motivo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_motivo_menu_insertar:
                Intent intent0= new Intent(this, MotivoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_motivo_menu_consultar:
                Intent intent1= new Intent(this, MotivoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_motivo_menu_Editar:
                Intent intente= new Intent(this, MotivoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_motivo_menu_eliminar:
                Intent intent2= new Intent(this, MotivoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_motivo_listar:
                Intent intent3= new Intent(this, MotivoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
