package edu.ues.fia.inventarioues.Modelos;

public class TipoEquipo {
    //"idTipoEquipo", "nombreTipoEquipo"
    int idTipoEquipo;
    String nombreTipoEquipo, fechaModificadaTipoEquipo;

    public TipoEquipo() {
    }

    public TipoEquipo(int idTipoEquipo, String nombreTipoEquipo, String fechaModificadaTipoEquipo) {
        this.idTipoEquipo = idTipoEquipo;
        this.nombreTipoEquipo = nombreTipoEquipo;
        this.fechaModificadaTipoEquipo = fechaModificadaTipoEquipo;
    }

    public String getFechaModificadaTipoEquipo() {
        return fechaModificadaTipoEquipo;
    }

    public void setFechaModificadaTipoEquipo(String fechaModificadaTipoEquipo) {
        this.fechaModificadaTipoEquipo = fechaModificadaTipoEquipo;
    }

    public int getIdTipoEquipo() {
        return idTipoEquipo;
    }

    public void setIdTipoEquipo(int idTipoEquipo) {
        this.idTipoEquipo = idTipoEquipo;
    }

    public String getNombreTipoEquipo() {
        return nombreTipoEquipo;
    }

    public void setNombreTipoEquipo(String nombreTipoEquipo) {
        this.nombreTipoEquipo = nombreTipoEquipo;
    }
}
