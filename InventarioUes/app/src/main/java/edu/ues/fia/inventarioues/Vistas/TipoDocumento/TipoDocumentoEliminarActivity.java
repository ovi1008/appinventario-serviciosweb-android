package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoDocumentoEliminarActivity extends AppCompatActivity {

    EditText editNombre;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_eliminar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editNombre=(EditText)findViewById(R.id.TipoDocumentoEliminarNombre1);
    }

    public void eliminarTipoDocumentoServidor(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombre.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_delete.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_delete.php?";

                String url = url1+ip+url3+
                        "nombreTipoDocumento="+editNombre.getText().toString().toUpperCase();
                String msj= ServiciosWeb.eliminarServidorLocal(url, this);

                Toast.makeText(this, msj+"el tipo documento: "+editNombre.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();

                editNombre.setText("");
            }

        }
    }

    public void eliminarConsultarTipoDocumentoServidor(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombre.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_delete.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_delete.php?";

                String url = url1+ip+url3+
                        "nombreTipoDocumento="+editNombre.getText().toString().toUpperCase();
                String msj= ServiciosWeb.eliminarServidorLocal(url, this);

                Toast.makeText(this, msj+"el tipo documento: "+editNombre.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();

                editNombre.setText("");
            }

        }
    }


    public void eliminarDeleteTipoDocumento(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.tipoDocumentoMsj1),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper = TablasM.getInstance(getApplicationContext());
            helper.open();
            TipoDocumento tipoDocumento= new TipoDocumento();
            tipoDocumento.setNombreTipoDocumento(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1TipoDocumento(tipoDocumento);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, TipoDocumentoEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", tipoDocumento.getNombreTipoDocumento());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editNombre.setText("");
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_tipo_documento_listarServidor:
                Intent intent4= new Intent(this, TipoDocumentoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_documento_listarSincronizar:
                Intent intent5= new Intent(this, TipoDocumentoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}