package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.R;

public class IdiomaAdaptador extends ArrayAdapter<Idioma> {
    //{"codIdioma", "nombreIdioma"};
    public IdiomaAdaptador(@NonNull Context context, @NonNull List<Idioma> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_idioma,parent,false);
        }
        TextView codIdioma=convertView.findViewById(R.id.ListarIdiomaId);
        TextView nombreIdioma=convertView.findViewById(R.id.ListarIdiomaNombre);

        Idioma idioma=getItem(position);
        codIdioma.setText(String.valueOf(idioma.getCodIdioma()));
        nombreIdioma.setText(idioma.getNombreIdioma());

        return convertView;
    }
}