package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.R;

public class IdiomaSincronizar2Activity extends AppCompatActivity {
    @SuppressLint("NewApi")
    TextView txtTitulo;
    ListView listSincro;
    Button local, servidor;
    List<String> idiomasNoRepetidas=new ArrayList<>();

    @SuppressLint("NewApi")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idioma_sincronizar2);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        txtTitulo=findViewById(R.id.IdiomaSincronizarTitulo);
        listSincro=findViewById(R.id.listIdiomaSincro);
        local=findViewById(R.id.IdiomaSincronizarGuardarL);
        servidor=findViewById(R.id.IdiomaSincronizarGuardarS);
    }

    public void IdiomaSincroLocal(View v){
        TablasL helper= TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>idiomasLocal=listadoIdiomasLocal();
            List<String>idiomasServidor=listadoIdiomasServidor();
            idiomasNoRepetidas=idiomasServidor;
            idiomasNoRepetidas.removeAll(idiomasLocal);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, idiomasNoRepetidas);
            listSincro.setAdapter(adapter);
            if (idiomasNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                local.setVisibility(View.VISIBLE);
                servidor.setVisibility(View.INVISIBLE);
            }
        }
    }


    public void IdiomaSincroServidor(View v){
        TablasL helper= TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>idiomasLocal=listadoIdiomasLocal();
            List<String>idiomasServidor=listadoIdiomasServidor();
            idiomasNoRepetidas=idiomasLocal;
            idiomasNoRepetidas.removeAll(idiomasServidor);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, idiomasNoRepetidas);
            listSincro.setAdapter(adapter);
            if (idiomasNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                servidor.setVisibility(View.VISIBLE);
                local.setVisibility(View.INVISIBLE);
            }
        }
    }

    public List<String> listadoIdiomasLocal(){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        Cursor c =helper.obtenerIdiomas();
        List<String> idiomaNombreList= new ArrayList<>();
        while (c.moveToNext()){
            //camposIdioma= new String[]{"idIdioma", "nombreIdioma"}
            idiomaNombreList.add(c.getString(1));
        }
        return idiomaNombreList;
    }

    public List<String> listadoIdiomasServidor(){
        TablasL helper= TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/idioma/ws_idioma_queryAll.php";
//            String urlLocal = "http://192.168.0.11/inventarioUes/idioma/ws_idioma_queryDate.php?";

        String url = url1+ip+url3;
        String json= ServiciosWeb.consultarFechaLocal(url,this);
        List<String> idiomaNombreList= new ArrayList<>();
        if (json.length()==0){}else{
            String[]datosIdioma=json.split("&&");
            for (String dato:datosIdioma){
                String []datoP=dato.split(",");
                String []nombreIdioma=datoP[1].split(":");
                //camposIdioma= new String[]{"idIdioma", "nombreIdioma"}
                idiomaNombreList.add(nombreIdioma[1]);
            }
        }
        return idiomaNombreList;
    }

    public void guardarIdiomasLocal(View v){
        TablasL helper=TablasL.getInstance(getApplicationContext());
        helper.open();
        for (String idioma:idiomasNoRepetidas){
            Idioma idiomaNlocal=new Idioma();
            idiomaNlocal.setNombreIdioma(idioma);
            helper.insertarIdioma(idiomaNlocal);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        idiomasNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        local.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    public void guardarIdiomasServidor(View v){
        TablasL helper=TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        for (String idioma:idiomasNoRepetidas){
            Idioma idiomaNservidor=new Idioma();
            idiomaNservidor.setNombreIdioma(idioma);
            String url1="http://";
            String url3="/inventarioUes/idioma/ws_idioma_insert.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/idioma/ws_idioma_insert.php?";

            String url = url1+ip+url3+
                    "nombreIdioma="+idiomaNservidor.getNombreIdioma();
            ServiciosWeb.insertarServidorLocal(url, this);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        idiomasNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        servidor.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_idioma_listarServidor:
                Intent intent4= new Intent(this, IdiomaListarServidor2Activity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_idioma_listarSincronizar:
                Intent intent5= new Intent(this, IdiomaSincronizar2Activity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
