package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Editorial.EditorialBuscarActivity;

@SuppressLint("NewApi")
public class IdiomaBuscarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    EditText editId, editNombre, editNombreServer;
    SearchView searchConsultar;
    TextToSpeech toSpeech;
    int result;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idioma_buscar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editId=(EditText)findViewById(R.id.IdiomaConsultarId);
        editNombre=(EditText)findViewById(R.id.IdiomaConsultarNombre);
        searchConsultar = (SearchView) findViewById(R.id.Idioma_searchConsultar);
        editNombreServer=findViewById(R.id.IdiomaConsultarNombreServidor);
        searchConsultar.setOnQueryTextListener(this);

        toSpeech=new TextToSpeech(IdiomaBuscarActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    result=toSpeech.setLanguage(Locale.getDefault());
                }else{
                    Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void playIdioma(View v){
        if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
            Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
        }else {
            String textoHablar;
            if (editId.getText().toString().isEmpty()){
                textoHablar="Realice una consulta primero";
            }else {
                textoHablar="Se consulta el siguiente idiomal:\n\n" +
                        "Codigo de idioma es: " +editId.getText().toString()+"\n\n"+
                        "El nombre del idioma es: "+editNombre.getText().toString();
            }
            toSpeech.speak(textoHablar,TextToSpeech.QUEUE_FLUSH,null);
        }
    }

    public void stopIdioma(View v){
        if (toSpeech!=null){
            toSpeech.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (toSpeech!=null){
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }

    public void consultarIdiomaServidor(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.idiomaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/idioma/ws_idioma_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/idioma/ws_idioma_query.php?";

                String url = url1+ip+url3+
                        "nombreIdioma="+editNombreServer.getText().toString().toUpperCase();
                String jsonIdioma= ServiciosWeb.consultarServidorLocal(url, this);
                editNombreServer.setText("");
                if (jsonIdioma==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datoIdioma=jsonIdioma.split(",");
                    String []idIdioma=datoIdioma[0].split(":");
                    String []nombreIdioma=datoIdioma[1].split(":");
                    Idioma idiomaS = new Idioma();
                    idiomaS.setCodIdioma(Integer.valueOf(idIdioma[1]));
                    idiomaS.setNombreIdioma(nombreIdioma[1]);
                    editId.setText(String.valueOf(idiomaS.getCodIdioma()));
                    editNombre.setText(idiomaS.getNombreIdioma());
                }


            }

        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasL helper= TablasL.getInstance(getApplicationContext());
        Idioma idioma= new Idioma();
        helper.open();
        idioma=helper.idiomaConsultar(newText.toUpperCase());
        helper.close();
        if (idioma==null){
            editNombre.setText("");
            editId.setText("");
        }else {
            editId.setText(String.valueOf(idioma.getCodIdioma()));
            editNombre.setText(idioma.getNombreIdioma());
        }
        return false;
    }
    public void buscarEditarIdioma(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else{
            Intent intentEditar= new Intent(this, IdiomaEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarIdioma(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasL helper = TablasL.getInstance(getApplicationContext());
            helper.open();
            Idioma idioma= new Idioma();
            idioma.setCodIdioma(Integer.valueOf(editId.getText().toString()));
            idioma.setNombreIdioma(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1Idioma(idioma);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, IdiomaEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", idioma.getNombreIdioma());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_idioma_listarServidor:
                Intent intent4= new Intent(this, IdiomaListarServidor2Activity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_idioma_listarSincronizar:
                Intent intent5= new Intent(this, IdiomaSincronizar2Activity.class);
                this.startActivity(intent5);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
