package edu.ues.fia.inventarioues.Vistas.Docente;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Docente;
import edu.ues.fia.inventarioues.R;

public class DocenteEliminarAdvertenciaActivity extends AppCompatActivity {

    TextView msj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docente_eliminar_advertencia);

        msj=(TextView)findViewById(R.id.DocenteEliminarAdvMens);

        Bundle bundle=getIntent().getExtras();

        msj.setText(getResources().getString(R.string.docenteMsj2)+" "
                +bundle.getString("carnet")+
                "\n\n"+getResources().getString(R.string.docenteMsj3)+" "
                +bundle.getString("msj"));
    }

    public void eliminarAdvDocente(View v){
        Bundle bundle=getIntent().getExtras();
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        Docente docente= new Docente();
        docente.setCarnetDocente(bundle.getString("carnet"));
        String mensaje= helper.eliminar2Docente(docente);
        helper.close();
        Intent intentM= new Intent(this,DocenteMenuActivity.class);
        this.startActivity(intentM);
    }

    public void eliminarCancelarDocente(View v){
        Intent intentCancelar = new Intent(this, DocenteMenuActivity.class);
        this.startActivity(intentCancelar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_docente,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_docente_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_docente_menu_insertar:
                Intent intent0= new Intent(this, DocenteInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_docente_menu_Editar:
                Intent intente= new Intent(this, DocenteEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_docente_menu_consultar:
                Intent intent1= new Intent(this, DocenteBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_docente_menu_eliminar:
                Intent intent2= new Intent(this, DocenteEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_docente_listar:
                Intent intent3= new Intent(this, DocenteMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
