package edu.ues.fia.inventarioues.Vistas.TipoEquipo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoEquipoSincronizarActivity extends AppCompatActivity {

    TextView txtTitulo;
    ListView listSincro;
    Button local, servidor;
    List<String> tipoEquiposNoRepetidas=new ArrayList<>();

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_equipo_sincronizar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        txtTitulo=findViewById(R.id.TipoEquipoSincronizarTitulo);
        listSincro=findViewById(R.id.listTipoEquipoSincro);
        local=findViewById(R.id.TipoEquipoSincronizarGuardarL);
        servidor=findViewById(R.id.TipoEquipoSincronizarGuardarS);
    }

    public void TipoEquipoSincroLocal(View v){
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>tipoEquiposLocal=listadoTipoEquiposLocal();
            List<String>tipoEquiposServidor=listadoTipoEquiposServidor();
            tipoEquiposNoRepetidas=tipoEquiposServidor;
            tipoEquiposNoRepetidas.removeAll(tipoEquiposLocal);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, tipoEquiposNoRepetidas);
            listSincro.setAdapter(adapter);
            if (tipoEquiposNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                local.setVisibility(View.VISIBLE);
                servidor.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void TipoEquipoSincroServidor(View v){
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>tipoEquiposLocal=listadoTipoEquiposLocal();
            List<String>tipoEquiposServidor=listadoTipoEquiposServidor();
            tipoEquiposNoRepetidas=tipoEquiposLocal;
            tipoEquiposNoRepetidas.removeAll(tipoEquiposServidor);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, tipoEquiposNoRepetidas);
            listSincro.setAdapter(adapter);
            if (tipoEquiposNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                servidor.setVisibility(View.VISIBLE);
                local.setVisibility(View.INVISIBLE);
            }
        }
    }

    public List<String> listadoTipoEquiposLocal(){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        Cursor c =helper.obtenerTiposEquipo();
        List<String> tipoEquipoNombreList= new ArrayList<>();
        while (c.moveToNext()){
            //camposTipoEquipo= new String[]{"idTipoEquipo", "nombreTipoEquipo"}
            tipoEquipoNombreList.add(c.getString(1));
        }
        return tipoEquipoNombreList;
    }

    public List<String> listadoTipoEquiposServidor(){
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/tipoEquipo/ws_tipoEquipo_queryAll.php";
        //            String urlLocal = "http://192.168.0.11/inventarioUes/tipoEquipo/ws_tipoEquipo_queryDate.php?";

        String url = url1+ip+url3;
        String json= ServiciosWeb.consultarFechaLocal(url,this);
        List<String> tipoEquipoNombreList= new ArrayList<>();
        if (json.length()==0){}else{
            String[]datosTipoEquipo=json.split("&&");
            for (String dato:datosTipoEquipo){
                String []datoP=dato.split(",");
                String []nombreTipoEquipo=datoP[1].split(":");
                //camposTipoEquipo= new String[]{"idTipoEquipo", "nombreTipoEquipo"}
                tipoEquipoNombreList.add(nombreTipoEquipo[1]);
            }
        }
        return tipoEquipoNombreList;
    }

    public void guardarTipoEquiposLocal(View v){
        TablasJ helper=TablasJ.getInstance(getApplicationContext());
        helper.open();
        for (String tipoEquipo:tipoEquiposNoRepetidas){
            TipoEquipo tipoEquipoNlocal=new TipoEquipo();
            tipoEquipoNlocal.setNombreTipoEquipo(tipoEquipo);
            helper.insertarTipoEquipo(tipoEquipoNlocal);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        tipoEquiposNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        local.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    public void guardarTipoEquiposServidor(View v){
        TablasJ helper=TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        for (String tipoEquipo:tipoEquiposNoRepetidas){
            TipoEquipo tipoEquipoNservidor=new TipoEquipo();
            tipoEquipoNservidor.setNombreTipoEquipo(tipoEquipo);
            String url1="http://";
            String url3="/inventarioUes/tipoEquipo/ws_tipoEquipo_insert.php?";
            //            String urlLocal = "http://192.168.0.11/inventarioUes/tipoEquipo/ws_tipoEquipo_insert.php?";

            String url = url1+ip+url3+
                    "nombreTipoEquipo="+tipoEquipoNservidor.getNombreTipoEquipo();
            ServiciosWeb.insertarServidorLocal(url, this);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        tipoEquiposNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        servidor.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_equipo_menu_insertar:
                Intent intent0= new Intent(this, TipoEquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_menu_menu_Editar:
                Intent intentEd= new Intent(this, TipoEquipoEditarActivity.class);
                this.startActivity(intentEd);
                return true;

            case R.id.action_tipo_equipo_menu_consultar:
                Intent intent1= new Intent(this, TipoEquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_tipo_equipo_menu_eliminar:
                Intent intent2= new Intent(this, TipoEquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_equipo_listar:
                Intent intent3= new Intent(this, TipoEquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
            case R.id.action_tipo_equipo_listarServidor:
                Intent intent4= new Intent(this, TipoEquipoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_equipo_listarSincronizar:
                Intent intent5= new Intent(this, TipoEquipoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
