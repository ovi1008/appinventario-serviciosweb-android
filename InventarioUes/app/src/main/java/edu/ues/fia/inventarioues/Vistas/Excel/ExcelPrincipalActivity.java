package edu.ues.fia.inventarioues.Vistas.Excel;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import edu.ues.fia.inventarioues.R;

public class ExcelPrincipalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excel_principal);
    }

    public void dirigirExport(View v){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (ActivityCompat.checkSelfPermission(ExcelPrincipalActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)
                startActivity(new Intent(getApplicationContext(), ExcelExportActivity.class));
            else
                ActivityCompat.requestPermissions(ExcelPrincipalActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        else
            startActivity(new Intent(getApplicationContext(), ExcelExportActivity.class));
    }

    public void dirigirImport(View v){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (ActivityCompat.checkSelfPermission(ExcelPrincipalActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)
                startActivity(new Intent(getApplicationContext(), ExcelImportActivity.class));
            else
                ActivityCompat.requestPermissions(ExcelPrincipalActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        else
            startActivity(new Intent(getApplicationContext(), ExcelImportActivity.class));
    }
}
