package edu.ues.fia.inventarioues.Vistas.Documento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.Modelos.Autor;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.R;

public class DocumentoEliminarAdvertenciaActivity extends AppCompatActivity {

    TextView txtMsj,txtMsj2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_eliminar_advertencia);
        txtMsj = findViewById(R.id.DocumentoEliminarAdvMens1);
        //txtMsj2 = findViewById(R.id.DocumentoEliminarAdvMens2);
        Bundle bundle=getIntent().getExtras();
        txtMsj.setText(getResources().getString(R.string.documentoMsj2)+" "
                + bundle.getInt("nombre")+" " +
                "\n\n"+getResources().getString(R.string.documentoMsj3)+" "
                +bundle.getString("msj"));
        //txtMsj2.setText("Ademas posee: "+bundle.getInt("msj2")+" detallles de documento asociados. Seguro desea eliminar?");
    }

    public void eliminarAdvDocumento(View v){
        Bundle bundle=getIntent().getExtras();
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        Documento documento = new Documento();
        documento.setIdDocumento(bundle.getInt("nombre"));
        String mensaje= helper.eliminar2Documento(documento);
        helper.close();
        Intent intentM= new Intent(this,DocumentoMenuActivity.class);
        this.startActivity(intentM);
    }



    public void eliminarCancelarDocumento(View v){
        Intent intentCancelar = new Intent(this, DocumentoMenuActivity.class);
        this.startActivity(intentCancelar);
    }
}
