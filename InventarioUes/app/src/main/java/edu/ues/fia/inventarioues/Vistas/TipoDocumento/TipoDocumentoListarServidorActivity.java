package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import edu.ues.fia.inventarioues.Adaptador.TipoDocumentoAdaptador;
import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoDocumentoListarServidorActivity extends AppCompatActivity {

    EditText editFecha;
    ListView listTipoDocumento;
    NumberPicker horaMinima, minutoMinimo, segundoMinimo;
    String h="0",m="0",s="0";
    String CERO = "0", DOS_PUNTOS = ":";
    Calendar calendar=Calendar.getInstance();
    List<TipoDocumento> listadoTipoDocumento=new ArrayList<>();
    List<TipoDocumento> vaciar=new ArrayList<>();


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_listar_servidor);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        editFecha=findViewById(R.id.TipoDocumentoServidorListarFecha);
        horaMinima=findViewById(R.id.TipoDocumentoServidorListarHoraMinima);
        minutoMinimo=findViewById(R.id.TipoDocumentoServidorListarMinutoMinima);
        segundoMinimo=findViewById(R.id.TipoDocumentoServidorListaSegundoMinima);

        listTipoDocumento=findViewById(R.id.listTipoDocumentoServidor);

        editFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(TipoDocumentoListarServidorActivity.this,fechaMinima, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        //Hora
        horaMinima.setMinValue(0);
        horaMinima.setMaxValue(23);
        horaMinima.setWrapSelectorWheel(true);
        horaMinima.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                h =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });

        //Minuto
        minutoMinimo.setMinValue(0);
        minutoMinimo.setMaxValue(59);
        minutoMinimo.setWrapSelectorWheel(true);
        minutoMinimo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                m =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });
        //Segundo
        segundoMinimo.setMinValue(0);
        segundoMinimo.setMaxValue(59);
        segundoMinimo.setWrapSelectorWheel(true);
        segundoMinimo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                s =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });

    }

    DatePickerDialog.OnDateSetListener fechaMinima = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            String formatoFecha = "yyyy-MM-dd";
            SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.getDefault());
            editFecha.setText(format.format(calendar.getTime()));
        }
    };

    public void consultarTipoDocumentoPorFechaServidor(View v){
        listadoTipoDocumento.removeAll(vaciar);
        String tiempoFormateado="%20"+h+":"+m+":"+s;
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editFecha.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.tipoDocumentoMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_queryDate.php?";
                //            String urlLocal = "http://192.168.0.11/inventarioUes/tipoDocumento/ws_tipoDocumento_queryDate.php?";

                String url = url1+ip+url3+
                        "fechaModificadaTipoDocumento="+editFecha.getText().toString()+tiempoFormateado;
                String json= ServiciosWeb.consultarFechaLocal(url,this);
                if (json.length()==0){}else{
                    String[]datosTipoDocumento=json.split("&&");
                    for (String dato:datosTipoDocumento){
                        String []datoP=dato.split(",");
                        String []idTipoDocumento=datoP[0].split(":");
                        String []nombreTipoDocumento=datoP[1].split(":");
                        TipoDocumento tipoDocumento=new TipoDocumento();
                        //camposTipoDocumento= new String[]{"idTipoDocumento", "nombreTipoDocumento"}
                        tipoDocumento.setIdTipoDocumento(Integer.valueOf(idTipoDocumento[1]));
                        tipoDocumento.setNombreTipoDocumento(nombreTipoDocumento[1]);
                        listadoTipoDocumento.add(tipoDocumento);
                        vaciar.add(tipoDocumento);
                    }
                    TipoDocumentoAdaptador tipoDocumentoAdaptador=new TipoDocumentoAdaptador(this,listadoTipoDocumento);
                    listTipoDocumento.setAdapter(tipoDocumentoAdaptador);

                }
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_tipo_documento_listarServidor:
                Intent intent4= new Intent(this, TipoDocumentoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_documento_listarSincronizar:
                Intent intent5= new Intent(this, TipoDocumentoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
