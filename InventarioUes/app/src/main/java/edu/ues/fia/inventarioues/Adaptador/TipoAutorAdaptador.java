package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.TipoAutor;
import edu.ues.fia.inventarioues.R;

public class TipoAutorAdaptador extends ArrayAdapter<TipoAutor> {

    public TipoAutorAdaptador(@NonNull Context context, @NonNull List<TipoAutor> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_tipo_autor,parent,false);
        }
        TextView idTipoAutor=convertView.findViewById(R.id.tipoAutorListarId);
        TextView nombreTipoAutor=convertView.findViewById(R.id.tipoAutorListarNombre);

        TipoAutor tipoAutor=getItem(position);
        idTipoAutor.setText(String.valueOf(tipoAutor.getIdTipoAutor()));
        nombreTipoAutor.setText(tipoAutor.getNombreTipoAutor());

        return convertView;
    }
}
