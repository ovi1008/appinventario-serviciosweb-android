package edu.ues.fia.inventarioues.Modelos;

public class TipoMovimiento {

    int idTipoMovimiento;
    String nombreTipoMovimiento;

    public TipoMovimiento() {
    }

    public TipoMovimiento(int idTipoMovimiento, String nombreTipoMovimiento) {
        this.idTipoMovimiento = idTipoMovimiento;
        this.nombreTipoMovimiento = nombreTipoMovimiento;
    }

    public int getIdTipoMovimiento() {
        return idTipoMovimiento;
    }

    public void setIdTipoMovimiento(int idTipoMovimiento) {
        this.idTipoMovimiento = idTipoMovimiento;
    }

    public String getNombreTipoMovimiento() {
        return nombreTipoMovimiento;
    }

    public void setNombreTipoMovimiento(String nombreTipoMovimiento) {
        this.nombreTipoMovimiento = nombreTipoMovimiento;
    }
}
