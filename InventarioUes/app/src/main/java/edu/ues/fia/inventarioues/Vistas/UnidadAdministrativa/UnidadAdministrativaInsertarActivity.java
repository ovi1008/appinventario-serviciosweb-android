package edu.ues.fia.inventarioues.Vistas.UnidadAdministrativa;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;
import edu.ues.fia.inventarioues.R;

public class UnidadAdministrativaInsertarActivity extends AppCompatActivity {
    EditText editNombreUnidadAdministrativa;
    private static final int recordImput=1111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unidad_administrativa_insertar);
        editNombreUnidadAdministrativa=(EditText)findViewById(R.id.UnidadAdministrativaInsertarNombre);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unidad_administrativa,menu);
        return true;
    }
    public void insertarUnidadAdministrativa(View v){

        String nombre, mensaje;
        TablasL helper= TablasL.getInstance(getApplicationContext());
        nombre=editNombreUnidadAdministrativa.getText().toString().toUpperCase();
        if (nombre.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.unidadMsj1), Toast.LENGTH_LONG).show();
        }else{
            UnidadAdministrativa unidadAdministrativa= new UnidadAdministrativa();
            unidadAdministrativa.setNombreUnidadAdministrativa(nombre);
            helper.open();
            mensaje=helper.insertarUnidadAdministrativa(unidadAdministrativa);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editNombreUnidadAdministrativa.setText("");
        }


    }
    public void iniciarVozUnidad(View v){
        Intent intentVoz= new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intentVoz.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intentVoz.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intentVoz.putExtra(RecognizerIntent.EXTRA_PROMPT, "Dicta el tipo de la unidad administrativa");
        try {
            startActivityForResult(intentVoz,recordImput);
        }catch (ActivityNotFoundException e){
            e.getStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case recordImput:{
                if(resultCode==RESULT_OK && null!=data){
                    ArrayList<String> result=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editNombreUnidadAdministrativa.setText(result.get(0));
                }
                break;
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_unidad_administrativa_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_unidad_administrativa_menu_insertar:
                Intent intent0= new Intent(this, UnidadAdministrativaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_unidad_administrativa_menu_consultar:
                Intent intent1= new Intent(this, UnidadAdministrativaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_unidad_administrativa_menu_Editar:
                Intent intente= new Intent(this, UnidadAdministrativaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_unidad_administrativa_menu_eliminar:
                Intent intent2= new Intent(this, UnidadAdministrativaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_unidad_administrativa_listar:
                Intent intent3= new Intent(this, UnidadAdministrativaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
