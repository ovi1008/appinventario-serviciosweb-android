package edu.ues.fia.inventarioues.Vistas.TipoEquipo;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoEquipo;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoEquipoInsertarActivity extends AppCompatActivity {

    EditText editNombre;
    private static final int recordImput=1111;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_tipo_equipo_insertar);
        editNombre=(EditText)findViewById(R.id.TipoEquipoInsertarNombre);
    }

    public void insertarTipoEquipo(View v){
        String nombre, mensaje;
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        nombre=editNombre.getText().toString().toUpperCase();
        if (nombre.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.tipoDeEquipoMsj1), Toast.LENGTH_LONG).show();
        }else{
            TipoEquipo tipoEquipo= new TipoEquipo();
            tipoEquipo.setNombreTipoEquipo(nombre);
            helper.open();
            mensaje=helper.insertarTipoEquipo(tipoEquipo);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editNombre.setText("");
        }
    }

    public void insertarTipoEquipoServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombre.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/tipoEquipo/ws_tipoEquipo_insert.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_insert.php?";

                String url = url1+ip+url3+
                        "nombreTipoEquipo="+editNombre.getText().toString().toUpperCase();
                String msj= ServiciosWeb.insertarServidorLocal(url, this);
                Toast.makeText(this, msj+"el tipo equipo: "+editNombre.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();
                editNombre.setText("");
            }

        }
    }

    public void iniciarVozTipoEquipo(View v){
        Intent intentVoz= new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intentVoz.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intentVoz.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intentVoz.putExtra(RecognizerIntent.EXTRA_PROMPT, "Dicta un tipo de equipo");
        try {
            startActivityForResult(intentVoz,recordImput);
        }catch (ActivityNotFoundException e){
            e.getStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case recordImput:{
                if(resultCode==RESULT_OK && null!=data){
                    ArrayList<String> result=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editNombre.setText(result.get(0));
                }
                break;
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_equipo_menu_insertar:
                Intent intent0= new Intent(this, TipoEquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_menu_menu_Editar:
                Intent intentEd= new Intent(this, TipoEquipoEditarActivity.class);
                this.startActivity(intentEd);
                return true;

            case R.id.action_tipo_equipo_menu_consultar:
                Intent intent1= new Intent(this, TipoEquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_tipo_equipo_menu_eliminar:
                Intent intent2= new Intent(this, TipoEquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_equipo_listar:
                Intent intent3= new Intent(this, TipoEquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
            case R.id.action_tipo_equipo_listarServidor:
                Intent intent4= new Intent(this, TipoEquipoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_equipo_listarSincronizar:
                Intent intent5= new Intent(this, TipoEquipoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
