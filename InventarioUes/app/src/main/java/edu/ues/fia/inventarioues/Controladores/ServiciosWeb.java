package edu.ues.fia.inventarioues.Controladores;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import edu.ues.fia.inventarioues.Modelos.Marca;

public class ServiciosWeb {

    public static String obtenerRespuestaPeticion(String url, Context ctx) {

        String respuesta = " ";

        // Estableciendo tiempo de espera del servicio
        HttpParams parametros = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(parametros, 3000);
        HttpConnectionParams.setSoTimeout(parametros, 5000);

        // Creando objetos de conexion
        HttpClient cliente = new DefaultHttpClient(parametros);
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpRespuesta = cliente.execute(httpGet);
            StatusLine estado = httpRespuesta.getStatusLine();
            int codigoEstado = estado.getStatusCode();
            if (codigoEstado == 200) {
                HttpEntity entidad = httpRespuesta.getEntity();
                respuesta = EntityUtils.toString(entidad);
            }
        } catch (Exception e) {
            Toast.makeText(ctx, "Error en la conexion", Toast.LENGTH_LONG)
                    .show();
            // Desplegando el error en el LogCat
            Log.v("Error de Conexion", e.toString());
        }
        return respuesta;
    }

    public static String obtenerRespuestaPost(String url, JSONObject obj,
                                              Context ctx) {
        String respuesta = " ";
        try {
            HttpParams parametros = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(parametros, 3000);
            HttpConnectionParams.setSoTimeout(parametros, 5000);

            HttpClient cliente = new DefaultHttpClient(parametros);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("content-type", "application/json");

            StringEntity nuevaEntidad = new StringEntity(obj.toString());
            httpPost.setEntity(nuevaEntidad);
            Log.v("Peticion",url);
            Log.v("POST", httpPost.toString());
            HttpResponse httpRespuesta = cliente.execute(httpPost);
            StatusLine estado = httpRespuesta.getStatusLine();

            int codigoEstado = estado.getStatusCode();
            if (codigoEstado == 200) {
                respuesta = Integer.toString(codigoEstado);
                Log.v("respuesta",respuesta);
            }
            else{
                Log.v("respuesta",Integer.toString(codigoEstado));
            }
        } catch (Exception e) {
            Toast.makeText(ctx, "Error en la conexion", Toast.LENGTH_LONG)
                    .show();
            // Desplegando el error en el LogCat
            Log.v("Error de Conexion", e.toString());
        }

        return respuesta;
    }

//    tablaMarca servicio web

    public static String insertarServidorLocal(String peticion, Context ctx) {
        String json = obtenerRespuestaPeticion(peticion, ctx);
        String []resultado=json.split(";");
        String resultadoParseado1=resultado[1].replace("{","");
        String resultadiParseado2=resultadoParseado1.replace("}","");
        String resultadoParseado3=resultadiParseado2.replace("\"","");
        if (resultadoParseado3.equals("resultado:1")){
            String msj="Se ingreso con exito en el servidor ";
            return msj;
        }else{
            String msj="Se detecto el siguiente error: \n"+
                    resultadoParseado3+
                    "\n\nAl tratar de ingresar al servidor ";
            return  msj;
        }
    }

    public static String consultarServidorLocal(String peticion, Context ctx) {
        Marca marcaS=new Marca();
        String json = obtenerRespuestaPeticion(peticion, ctx);
        String jsonParseado1=json.replace("{","");
        String jsonParseado2=jsonParseado1.replace("}","");
        String jsonParseado3=jsonParseado2.replace("\"","");
        String []datos=jsonParseado3.split(",");
        if (datos[0].equals("No existe")){
            return null;
        }else {
            return jsonParseado3;
        }
    }

    public static String editarServidorLocal(String peticion, Context ctx) {
        String json = obtenerRespuestaPeticion(peticion, ctx);
        String []resultado=json.split(";");
        String resultadoParseado1=resultado[1].replace("{","");
        String resultadiParseado2=resultadoParseado1.replace("}","");
        String resultadoParseado3=resultadiParseado2.replace("\"","");
        if (resultadoParseado3.equals("resultado:0")){
            String msj="No existe en el servidor ";
            return msj;
        }else{
            String msj="Se edito con exito en el servidor ";
            return  msj;
        }
    }

    public static String eliminarServidorLocal(String peticion, Context ctx) {
        String json = obtenerRespuestaPeticion(peticion, ctx);
        String []resultado=json.split(";");
        String resultadoParseado1=resultado[1].replace("{","");
        String resultadiParseado2=resultadoParseado1.replace("}","");
        String resultadoParseado3=resultadiParseado2.replace("\"","");
        if (resultadoParseado3.equals("resultado:0")){
            String msj="No existe en el servidor ";
            return msj;
        }else{
            String msj="Se elimino con exito ";
            return  msj;
        }

    }

    public static String consultarFechaLocal(String peticion, Context ctx){
        String json = obtenerRespuestaPeticion(peticion, ctx);
        String resultadoParseado1=json.replace("{","");
        String resultadiParseado2=resultadoParseado1.replace("},","&&");
        String resultadoParseado3=resultadiParseado2.replace("\"","");
        String resultadoParseado4=resultadoParseado3.replace("[","");
        String resultadoParseado5=resultadoParseado4.replace("]","");
        return  resultadoParseado5;

    }



}
