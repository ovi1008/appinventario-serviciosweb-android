package edu.ues.fia.inventarioues.Modelos;

public class Marca {
    int idMarca;
    String nombreMarca, fechaModificadaMarca;

    public Marca() {
    }

    public Marca(int idMarca, String nombreMarca, String fechaModificadaMarca) {
        this.idMarca = idMarca;
        this.nombreMarca = nombreMarca;
        this.fechaModificadaMarca = fechaModificadaMarca;
    }

    public String getFechaModificadaMarca() {
        return fechaModificadaMarca;
    }

    public void setFechaModificadaMarca(String fechaModificadaMarca) {
        this.fechaModificadaMarca = fechaModificadaMarca;
    }

    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    public String getNombreMarca() {
        return nombreMarca;
    }

    public void setNombreMarca(String nombreMarca) {
        this.nombreMarca = nombreMarca;
    }
}
