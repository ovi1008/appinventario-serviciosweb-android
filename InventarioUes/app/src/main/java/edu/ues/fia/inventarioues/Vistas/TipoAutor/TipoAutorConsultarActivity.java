package edu.ues.fia.inventarioues.Vistas.TipoAutor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoAutor;
import edu.ues.fia.inventarioues.R;

public class TipoAutorConsultarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    EditText editId,editNombre;
    SearchView searchConsultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_autor_consultar);
        searchConsultar=(SearchView) findViewById(R.id.TipoAutor_searchConsultar);
        editId=(EditText)findViewById(R.id.TipoAutorConsultarId);
        editNombre=(EditText)findViewById(R.id.TipoAutorConsultarNombre);
        searchConsultar.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasV helper= TablasV.getInstance(getApplicationContext());
        TipoAutor tipoAutor= new TipoAutor();
        helper.open();
        tipoAutor=helper.tipoAutorConsultar(newText.toUpperCase());
        helper.close();
        if (tipoAutor==null){
            editId.setText("");
            editNombre.setText("");
        }else {
            editId.setText(String.valueOf(tipoAutor.getIdTipoAutor()));
            editNombre.setText(tipoAutor.getNombreTipoAutor());
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void buscarEditarTipoAutor(View v){
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            Intent intentEditar= new Intent(this, TipoAutorEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarTipoAutor(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            TipoAutor tipoAutor= new TipoAutor();
            tipoAutor.setIdTipoAutor(Integer.valueOf(editId.getText().toString()));
            tipoAutor.setNombreTipoAutor(editNombre.getText().toString());
            String mensaje= helper.eliminar1TipoAutor(tipoAutor);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,TipoAutorEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", tipoAutor.getNombreTipoAutor());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_autor,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_autor_menu_insertar:
                Intent intent0= new Intent(this, TipoAutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_autor_menu_consultar:
                Intent intent1= new Intent(this, TipoAutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_autor_menu_Editar:
                Intent intente= new Intent(this, TipoAutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_autor_menu_eliminar:
                Intent intent2= new Intent(this, TipoAutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_autor_listar:
                Intent intent3= new Intent(this, TipoAutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
