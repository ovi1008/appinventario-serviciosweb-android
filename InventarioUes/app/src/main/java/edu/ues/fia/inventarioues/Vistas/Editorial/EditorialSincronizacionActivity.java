package edu.ues.fia.inventarioues.Vistas.Editorial;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class EditorialSincronizacionActivity extends AppCompatActivity {
    TextView txtTitulo;
    ListView listSincro;
    Button local, servidor;
    List<String>editorialsNoRepetidas=new ArrayList<>();

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorial_sincronizacion);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        txtTitulo=findViewById(R.id.EditorialSincronizarTitulo);
        listSincro=findViewById(R.id.listEditorialSincro);
        local=findViewById(R.id.EditorialSincronizarGuardarL);
        servidor=findViewById(R.id.EditorialSincronizarGuardarS);
    }

    public void EditorialSincroLocal(View v){
        TablasV helper= TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>editorialsLocal=listadoEditorialsLocal();
            List<String>editorialsServidor=listadoEditorialsServidor();
            editorialsNoRepetidas=editorialsServidor;
            editorialsNoRepetidas.removeAll(editorialsLocal);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, editorialsNoRepetidas);
            listSincro.setAdapter(adapter);
            if (editorialsNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                local.setVisibility(View.VISIBLE);
                servidor.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void EditorialSincroServidor(View v){
        TablasV helper= TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>editorialsLocal=listadoEditorialsLocal();
            List<String>editorialsServidor=listadoEditorialsServidor();
            editorialsNoRepetidas=editorialsLocal;
            editorialsNoRepetidas.removeAll(editorialsServidor);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, editorialsNoRepetidas);
            listSincro.setAdapter(adapter);
            if (editorialsNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                servidor.setVisibility(View.VISIBLE);
                local.setVisibility(View.INVISIBLE);
            }
        }
    }

    public List<String> listadoEditorialsLocal(){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        Cursor c =helper.obtenerEditoriales();
        List<String> editorialNombreList= new ArrayList<>();
        while (c.moveToNext()){
            //camposEditorial= new String[]{"idEditorial", "nombreEditorial"}
            editorialNombreList.add(c.getString(1));
        }
        return editorialNombreList;
    }

    public List<String> listadoEditorialsServidor(){
        TablasV helper= TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/editorial/ws_editorial_queryAll.php";
//            String urlLocal = "http://192.168.0.11/inventarioUes/editorial/ws_editorial_queryDate.php?";

        String url = url1+ip+url3;
        String json= ServiciosWeb.consultarFechaLocal(url,this);
        List<String> editorialNombreList= new ArrayList<>();
        if (json.length()==0){}else{
            String[]datosEditorial=json.split("&&");
            for (String dato:datosEditorial){
                String []datoP=dato.split(",");
                String []nombreEditorial=datoP[1].split(":");
                //camposEditorial= new String[]{"idEditorial", "nombreEditorial"}
                editorialNombreList.add(nombreEditorial[1]);
            }
        }
        return editorialNombreList;
    }

    public void guardarEditorialsLocal(View v){
        TablasV helper=TablasV.getInstance(getApplicationContext());
        helper.open();
        for (String editorial:editorialsNoRepetidas){
            Editorial editorialNlocal=new Editorial();
            editorialNlocal.setNombreEditorial(editorial);
            helper.insertarEditorial(editorialNlocal);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        editorialsNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        local.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    public void guardarEditorialsServidor(View v){
        TablasV helper=TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        for (String editorial:editorialsNoRepetidas){
            Editorial editorialNservidor=new Editorial();
            editorialNservidor.setNombreEditorial(editorial);
            String url1="http://";
            String url3="/inventarioUes/editorial/ws_editorial_insert.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/editorial/ws_editorial_insert.php?";

            String url = url1+ip+url3+
                    "nombreEditorial="+editorialNservidor.getNombreEditorial();
            ServiciosWeb.insertarServidorLocal(url, this);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        editorialsNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        servidor.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editorial,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_editorial_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_editorial_menu_insertar:
                Intent intent0= new Intent(this, EditorialInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_editorial_menu_Editar:
                Intent intente= new Intent(this, EditorialEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_editorial_menu_consultar:
                Intent intent1= new Intent(this, EditorialBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_editorial_menu_eliminar:
                Intent intent2= new Intent(this, EditorialEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_editorial_listar:
                Intent intent3= new Intent(this, EditorialMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_editorial_listarServidor:
                Intent intent4= new Intent(this, EditorialListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_editorial_listarSincronizar:
                Intent intent5= new Intent(this, EditorialSincronizacionActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
