package edu.ues.fia.inventarioues.Vistas.Autor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Autor;
import edu.ues.fia.inventarioues.R;

public class AutorEliminarActivity extends AppCompatActivity {

    EditText editNombreAutor,editApellidoAutor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autor_eliminar);
        editNombreAutor=findViewById(R.id.AutorEliminarNombre1);
        editApellidoAutor=findViewById(R.id.AutorEliminarApellido1);
        
    }

    public void eliminarDeleteAutor(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if (editNombreAutor.getText().toString().isEmpty() ||editApellidoAutor.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.autorMsj1a),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Autor autor = new Autor();
            autor.setNombreAutor(editNombreAutor.getText().toString().toUpperCase().replaceFirst("\\s++$", ""));
            autor.setApellidoAutor(editApellidoAutor.getText().toString().toUpperCase().replaceFirst("\\s++$", ""));
            String mensaje= helper.eliminar1Autor(autor);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,AutorEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre",autor.getNombreAutor());
                intentEliminar.putExtra("apellido",autor.getApellidoAutor());
                this.startActivity(intentEliminar);
            }else {


                editNombreAutor.setText("");
                editApellidoAutor.setText("");
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;



            case R.id.action_autor_menu_insertar:
                Intent intent0= new Intent(this, AutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_autor_menu_Editar:
                Intent intente= new Intent(this, AutorEditarActivity.class);
                this.startActivity(intente);
                return true;

            case R.id.action_autor_menu_consultar:
                Intent intent1= new Intent(this, AutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;


            case R.id.action_autor_menu_eliminar:
                Intent intent2= new Intent(this, AutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_autor_listar:
                Intent intent3= new Intent(this, AutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
