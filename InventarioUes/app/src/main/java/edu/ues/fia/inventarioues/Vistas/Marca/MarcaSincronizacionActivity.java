package edu.ues.fia.inventarioues.Vistas.Marca;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import edu.ues.fia.inventarioues.Adaptador.MarcaAdaptador;
import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class MarcaSincronizacionActivity extends AppCompatActivity {
    TextView txtTitulo;
    ListView listSincro;
    Button local, servidor;
    List<String>marcasNoRepetidas=new ArrayList<>();

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marca_sincronizacion);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        txtTitulo=findViewById(R.id.MarcaSincronizarTitulo);
        listSincro=findViewById(R.id.listMarcaSincro);
        local=findViewById(R.id.MarcaSincronizarGuardarL);
        servidor=findViewById(R.id.MarcaSincronizarGuardarS);
    }

    public void MarcaSincroLocal(View v){
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>marcasLocal=listadoMarcasLocal();
            List<String>marcasServidor=listadoMarcasServidor();
            marcasNoRepetidas=marcasServidor;
            marcasNoRepetidas.removeAll(marcasLocal);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, marcasNoRepetidas);
            listSincro.setAdapter(adapter);
            if (marcasNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                local.setVisibility(View.VISIBLE);
                servidor.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void MarcaSincroServidor(View v){
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>marcasLocal=listadoMarcasLocal();
            List<String>marcasServidor=listadoMarcasServidor();
            marcasNoRepetidas=marcasLocal;
            marcasNoRepetidas.removeAll(marcasServidor);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, marcasNoRepetidas);
            listSincro.setAdapter(adapter);
            if (marcasNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                servidor.setVisibility(View.VISIBLE);
                local.setVisibility(View.INVISIBLE);
            }
        }
    }

    public List<String> listadoMarcasLocal(){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        Cursor c =helper.obtenerMarcas();
        List<String> marcaNombreList= new ArrayList<>();
        while (c.moveToNext()){
            //camposMarca= new String[]{"idMarca", "nombreMarca"}
            marcaNombreList.add(c.getString(1));
        }
        return marcaNombreList;
    }

    public List<String> listadoMarcasServidor(){
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/marca/ws_marca_queryAll.php";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_queryDate.php?";

        String url = url1+ip+url3;
        String json= ServiciosWeb.consultarFechaLocal(url,this);
        List<String> marcaNombreList= new ArrayList<>();
        if (json.length()==0){}else{
            String[]datosMarca=json.split("&&");
            for (String dato:datosMarca){
                String []datoP=dato.split(",");
                String []nombreMarca=datoP[1].split(":");
                //camposMarca= new String[]{"idMarca", "nombreMarca"}
                marcaNombreList.add(nombreMarca[1]);
            }
        }
        return marcaNombreList;
    }

    public void guardarMarcasLocal(View v){
        TablasJ helper=TablasJ.getInstance(getApplicationContext());
        helper.open();
        for (String marca:marcasNoRepetidas){
            Marca marcaNlocal=new Marca();
            marcaNlocal.setNombreMarca(marca);
            helper.insertarMarca(marcaNlocal);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        marcasNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        local.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    public void guardarMarcasServidor(View v){
        TablasJ helper=TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        for (String marca:marcasNoRepetidas){
            Marca marcaNservidor=new Marca();
            marcaNservidor.setNombreMarca(marca);
            String url1="http://";
            String url3="/inventarioUes/marca/ws_marca_insert.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_insert.php?";

            String url = url1+ip+url3+
                    "nombreMarca="+marcaNservidor.getNombreMarca();
            ServiciosWeb.insertarServidorLocal(url, this);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        marcasNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        servidor.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_marca,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_marca_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_marca_menu_insertar:
                Intent intent0= new Intent(this, MarcaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_marca_menu_Editar:
                Intent intente= new Intent(this, MarcaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_marca_menu_consultar:
                Intent intent1= new Intent(this, MarcaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_marca_menu_eliminar:
                Intent intent2= new Intent(this, MarcaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_marca_listar:
                Intent intent3= new Intent(this, MarcaMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_marca_listarServidor:
                Intent intent4= new Intent(this, MarcaListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_marca_listarSincronizar:
                Intent intent5= new Intent(this, MarcaSincronizacionActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
