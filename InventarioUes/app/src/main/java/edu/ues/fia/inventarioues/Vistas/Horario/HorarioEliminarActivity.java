package edu.ues.fia.inventarioues.Vistas.Horario;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Horario;
import edu.ues.fia.inventarioues.R;

public class HorarioEliminarActivity extends AppCompatActivity {
    EditText editHorario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario_eliminar);
        editHorario=(EditText)findViewById(R.id.HorarioEliminarNombre1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_horario,menu);
        return true;
    }

    public void eliminarDeleteHorario(View v){
        if (editHorario.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.horarioMsj1a),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper = TablasM.getInstance(getApplicationContext());
            helper.open();
            Horario horario= new Horario();
            horario.setIdHorario(Integer.valueOf(editHorario.getText().toString().toUpperCase()));
            String mensaje= helper.eliminar1Horario(horario);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, HorarioEliminarAdvActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("id", horario.getIdHorario());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editHorario.setText("");
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_horario_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_horario_menu_insertar:
                Intent intent0= new Intent(this, HorarioInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_horario_menu_consultar:
                Intent intent1= new Intent(this, HorarioConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_horario_menu_Editar:
                Intent intente= new Intent(this, HorarioEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_horario_menu_eliminar:
                Intent intent2= new Intent(this, HorarioEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_horario_listar:
                Intent intent3= new Intent(this, HorarioMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
