package edu.ues.fia.inventarioues.Vistas.TipoMovimiento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoMovimiento;
import edu.ues.fia.inventarioues.R;


public class TipoMovimientoEditarActivity extends AppCompatActivity {

    EditText editId, editNombre, editBusca;
    String clave="";
    Button busca;
    int opcion=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_movimiento_editar);
        editId=(EditText)findViewById(R.id.TipoMovimientoEditarId);
        editNombre=(EditText)findViewById(R.id.TipoMovimientoEditarNombre);
        editBusca=(EditText) findViewById(R.id.TipoMovimientoEditarSearch);
        busca=(Button)findViewById(R.id.TipoMovimientoEditarBtnBuscar);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            busca.setVisibility(View.INVISIBLE);
            editBusca.setVisibility(View.INVISIBLE);

        }
    }

    public void actualizarTipoMovimiento(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasM helper= TablasM.getInstance(getApplicationContext());
            TipoMovimiento tipoMovimiento = new TipoMovimiento();
            tipoMovimiento.setIdTipoMovimiento(Integer.valueOf(editId.getText().toString()));
            tipoMovimiento.setNombreTipoMovimiento(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarTipoMovimiento(tipoMovimiento, clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intentEdi=new Intent(this, TipoMovimientoMenuActivity.class);
                this.startActivity(intentEdi);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);

            }
        }

    }

    public void buscarTipoMovimientoEditar(View v){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        helper.open();
        TipoMovimiento tipoMovimiento=helper.tipoMovimientoConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (tipoMovimiento==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(tipoMovimiento.getIdTipoMovimiento()));
            editNombre.setText(tipoMovimiento.getNombreTipoMovimiento());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_movimiento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_tipo_movimiento_menu_insertar:
                Intent intent0= new Intent(this, TipoMovimientoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_movimiento_menu_consultar:
                Intent intent1= new Intent(this, TipoMovimientoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_movimiento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_tipo_movimiento_menu_Editar:
                Intent intentEd= new Intent(this, TipoMovimientoEditarActivity.class);
                this.startActivity(intentEd);
                return true;


            case R.id.action_tipo_movimiento_menu_eliminar:
                Intent intent2= new Intent(this, TipoMovimientoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_movimiento_listar:
                Intent intent3= new Intent(this, TipoMovimientoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
