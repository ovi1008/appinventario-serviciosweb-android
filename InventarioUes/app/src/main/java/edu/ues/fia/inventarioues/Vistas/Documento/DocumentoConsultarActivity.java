package edu.ues.fia.inventarioues.Vistas.Documento;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.blikoon.qrcodescanner.QrCodeActivity;

import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Documento;
import edu.ues.fia.inventarioues.NuevoCodigoActivity;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Equipo.EquipoConsultarActivity;

public class DocumentoConsultarActivity extends AppCompatActivity{

    SearchView searchDocumento;
    TextView txtEstado, txtAutores;
    int valorPrestamo,valorSelectTipoDocumento, valorSelectEditorial, valorSelectIdioma;
    EditText editLocal,editIdDocumento, editIsbn, editNombre, editEdicion,editPublicacionDocumento,editTipoDocumento,
    editEditorial, editIdioma;
    TextToSpeech toSpeech;
    int result;
    private static final int REQUEST_CODE_QR_SCAN = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_consultar);
        editLocal=findViewById(R.id.DocumentoIdLocal);




        txtEstado = findViewById(R.id.DocumentoConsultarPrestamo);

        editIdDocumento =findViewById(R.id.DocumentoConsultarIdDocumento);
        editIsbn = findViewById(R.id.DocumentoConsultarIsbn);
        editNombre = findViewById(R.id.DocumentoConsultarNombre);
        editEdicion = findViewById(R.id.DocumentoConsultarEdicion);
        editPublicacionDocumento = findViewById(R.id.DocumentoConsultarPublicacion);
        editTipoDocumento = findViewById(R.id.DocumentoConsultarTipoDocumento);
        editEditorial = findViewById(R.id.DocumentoConsultarEditorial);
        editIdioma = findViewById(R.id.DocumentoConsultarIdioma);
        txtAutores = findViewById(R.id.DocumentoConsultarAutores);

        toSpeech=new TextToSpeech(DocumentoConsultarActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    result=toSpeech.setLanguage(Locale.getDefault());
                }else{
                    Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void playDocumento(View v){
        if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
            Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
        }else {
            String textoHablar;
            if (editIdDocumento.getText().toString().isEmpty()){
                textoHablar="Realice una consulta primero";
            }else {
                textoHablar="Se consulta el siguiente documento:\n\n" +
                        "El documento esta: "+txtEstado.getText().toString()+"\n\n"+
                "El codigo del documento es: "+editIdDocumento.getText().toString()+"\n\n"+
                "El isbm del documento es: "+editIsbn.getText().toString()+"\n\n"+
                "El documento es de tipo: "+editTipoDocumento.getText().toString()+"\n\n"+
                "El documento es de la editorial: "+editEditorial.getText().toString()+"\n\n"+
                "El documento pose el idioma: "+editIdioma.getText().toString()+"\n\n"+
                "El documento tiene el nombre de: "+editNombre.getText().toString()+"\n\n"+
                "La edicion del documento es: "+editEdicion.getText().toString()+"\n\n"+
                "Los autores del documento es: "+txtAutores.getText().toString()+"\n\n";
                if (editPublicacionDocumento.getText().toString().isEmpty()){}
                else {
                    textoHablar=textoHablar+"Fecha de publicacion del documento es:"+editPublicacionDocumento.getText().toString();
                }
            }
            toSpeech.speak(textoHablar,TextToSpeech.QUEUE_FLUSH,null);
        }
    }

    public void stopDocumento(View v){
        if (toSpeech!=null){
            toSpeech.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (toSpeech!=null){
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }



    public void documentoBuscarLocal(View v) {
        String newText=editLocal.getText().toString();
        TablasV helper= TablasV.getInstance(getApplicationContext());
        Documento documento = new Documento();
        helper.open();
        try{
            documento=helper.documentoConsultar(Integer.parseInt(newText));
        }catch (Exception e){e.printStackTrace();}


        helper.close();
        if (documento==null){

            editIdDocumento.setText("");
            editIsbn.setText("");
            editTipoDocumento.setText("");

            editEditorial.setText("");
            editIdioma.setText("");
            editNombre.setText("");
            editEdicion.setText("");
            editPublicacionDocumento.setText("");

            txtEstado.setText(getResources().getString(R.string.documentoCampo10));
            txtEstado.setTextColor(Color.rgb(164,164,164));
        }else {
            if (documento.getPrestadoDocumento()==1){
                txtEstado.setText(getResources().getString(R.string.documentoCampo10e1));
                txtEstado.setTextColor(Color.rgb(100,221,23));
                valorPrestamo=1;
            }else {
                txtEstado.setText(getResources().getString(R.string.documentoCampo10e2));
                txtEstado.setTextColor(Color.rgb(213,0,0));
                valorPrestamo=0;
            }

            editIdDocumento.setText(String.valueOf(documento.getIdDocumento()));

            Cursor tipoDocumento = helper.obtenerTipoDocumentos();
            while (tipoDocumento.moveToNext() && editTipoDocumento.getText().toString().isEmpty()){
                if (tipoDocumento.getInt(0)==documento.getIdTipoDocumento()){
                    editTipoDocumento.setText(tipoDocumento.getString(1));
                    valorSelectTipoDocumento=tipoDocumento.getInt(0);
                }
            }
            Cursor editorial=helper.obtenerEditoriales();
            while (editorial.moveToNext() && editEditorial.getText().toString().isEmpty()){
                if (editorial.getInt(0)==documento.getIdEditorial()){
                    editEditorial.setText(editorial.getString(1));
                    valorSelectEditorial=editorial.getInt(0);
                }
            }

            Cursor idioma=helper.obtenerIdiomas();
            while (idioma.moveToNext() && editIdioma.getText().toString().isEmpty()){
                if (idioma.getInt(0)==documento.getCodIdioma()){
                    editIdioma.setText(idioma.getString(1));
                    valorSelectIdioma=idioma.getInt(0);
                }
            }

            /*
            if (equipo.getEstadoEquipo()==1){
                editEstado.setText("Funcional");
                valorSelectOpcion=1;
            }else {
                editEstado.setText("Arruinado");
                valorSelectOpcion=0;
            }*/

            //editDescripcion.setText(equipo.getDescripcionEquipo());
            editPublicacionDocumento.setText(documento.getPublicacionDocumento());
            editNombre.setText(documento.getNombreDocumento());
            editEdicion.setText(String.valueOf(documento.getEdicionDocumento()));
            editIsbn.setText(documento.getIsbnDocumento());
            txtAutores.setText(documento.getAutoresDocumento());

        }
    }


    public void equipoConsultarEdicion(View v){
        if(editIdDocumento.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            /*
              editIdDocumento.setText(bundle.getString("idDocumento"));
            editIsbnDocumento.setText(bundle.getString("isbn"));
            editNombreDocumento.setText(bundle.getString("nombre"));
            editEdicionDocumento.setText(bundle.getString("edicion"));
            editFechaPublicacion.setText(bundle.getString("publicacion"));

            spTipoDocumento.setSelection(bundle.getInt("idTipoAutor"));
            spEditorial.setSelection(bundle.getInt("editorial"));
            spIdioma.setSelection(bundle.getInt("codIdioma"));
            txtAutores.setText(bundle.getString("autores"));

            */

            String idDocumento,isbnDocumento,NombreDocumento,EdicionDocumento,FechaPublicacion;

            idDocumento = editIdDocumento.getText().toString();
            isbnDocumento = editIsbn.getText().toString();
            NombreDocumento = editNombre.getText().toString().toUpperCase().replaceFirst("\\s++$", "");
            EdicionDocumento = editEdicion.getText().toString();
            FechaPublicacion = editPublicacionDocumento.getText().toString();

            Intent intentEditar= new Intent(this, DocumentoEditarActivity.class);

            intentEditar.putExtra("idDocumento",idDocumento);
            intentEditar.putExtra("isbn",isbnDocumento);
            intentEditar.putExtra("nombre",NombreDocumento);
            intentEditar.putExtra("edicion",EdicionDocumento);
            intentEditar.putExtra("publicacion",FechaPublicacion);
            intentEditar.putExtra("idTipoDocumento",valorSelectTipoDocumento);
            intentEditar.putExtra("editorial",valorSelectEditorial);
            intentEditar.putExtra("codIdioma",valorSelectIdioma);
            intentEditar.putExtra("autores",txtAutores.getText().toString().toUpperCase());



            this.startActivity(intentEditar);
        }
    }

    public void scanearCodigoDocumento(View v){


        Intent i = new Intent(DocumentoConsultarActivity.this, QrCodeActivity.class);
        startActivityForResult(i, REQUEST_CODE_QR_SCAN);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(getApplicationContext(), "No se pudo obtener una respuesta", Toast.LENGTH_SHORT).show();
            String resultado = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (resultado != null) {
                Toast.makeText(getApplicationContext(), "No se pudo escanear el código QR", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data != null) {
                String lectura = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
                String[] resultadoDatos=lectura.split(",");
                String[] datoCodigo=resultadoDatos[0].split(":");
                String validacion=lectura.substring(0,9);
                if (validacion.equals("El codigo")){
                    editLocal.setText(datoCodigo[1]);
                    Toast.makeText(getApplicationContext(), "Leído: " + lectura, Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(), "Codigio Qr no valido", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }


    public void generarCodigoDocumento(View v){
        String textoCodigo;
        if (editIdDocumento.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            textoCodigo=
                    "El codigo del documento es :"+editIdDocumento.getText().toString()+",\n\n"+
                    "El isbm del documento es: "+editIsbn.getText().toString()+"\n\n"+
                    "El documento es de tipo: "+editTipoDocumento.getText().toString()+"\n\n"+
                    "El documento es de la editorial: "+editEditorial.getText().toString()+"\n\n"+
                    "El documento pose el idioma: "+editIdioma.getText().toString()+"\n\n"+
                    "El documento tiene el nombre de: "+editNombre.getText().toString()+"\n\n"+
                    "La edicion del documento es: "+editEdicion.getText().toString()+"\n\n"+
                    "Los autores del documento es: "+txtAutores.getText().toString()+"\n\n";
            if (editPublicacionDocumento.getText().toString().isEmpty()){}
            else {
                textoCodigo=textoCodigo+"Fecha de publicacion del documento es:"+editPublicacionDocumento.getText().toString();
            }
            Intent codigo = new Intent(this, NuevoCodigoActivity.class);
            codigo.putExtra("codigoQr",textoCodigo);
            codigo.putExtra("titulo", "Documento");
            this.startActivity(codigo);
        }
    }


    public void buscarEliminarDocumento(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if(editIdDocumento.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Documento documento= new Documento();
            documento.setIdDocumento(Integer.parseInt(editIdDocumento.getText().toString()));
            String mensaje= helper.eliminar1Documento(documento);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, DocumentoEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", documento.getIdDocumento());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_documento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_documento_menu_principal:
                Intent intentP = new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_documento_menu_insertar:
                Intent intent0 = new Intent(this, DocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_documento_menu_consultar:
                Intent intent1 = new Intent(this, DocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_documento_menu_Editar:
                Intent intente = new Intent(this, DocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_documento_menu_eliminar:
                Intent intent2 = new Intent(this, DocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_documento_listar:
                Intent intent3 = new Intent(this, DocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
