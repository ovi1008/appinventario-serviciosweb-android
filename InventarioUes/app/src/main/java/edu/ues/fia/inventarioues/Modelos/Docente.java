package edu.ues.fia.inventarioues.Modelos;

public class Docente {

    String carnetDocente;
    String nombreDocente;
    String apellidoDocente;
    String telefonoDocente;
    String direccionDocente;

    public Docente() {
    }

    public Docente(String carnetDocente, String nombreDocente, String apellidoDocente, String telefonoDocente, String direccionDocente) {
        this.carnetDocente = carnetDocente;
        this.nombreDocente = nombreDocente;
        this.apellidoDocente = apellidoDocente;
        this.telefonoDocente = telefonoDocente;
        this.direccionDocente = direccionDocente;
    }

    public String getCarnetDocente() {
        return carnetDocente;
    }

    public void setCarnetDocente(String carnetDocente) {
        this.carnetDocente = carnetDocente;
    }

    public String getNombreDocente() {
        return nombreDocente;
    }

    public void setNombreDocente(String nombreDocente) {
        this.nombreDocente = nombreDocente;
    }

    public String getApellidoDocente() {
        return apellidoDocente;
    }

    public void setApellidoDocente(String apellidoDocente) {
        this.apellidoDocente = apellidoDocente;
    }

    public String getTelefonoDocente() {
        return telefonoDocente;
    }

    public void setTelefonoDocente(String telefonoDocente) {
        this.telefonoDocente = telefonoDocente;
    }

    public String getDireccionDocente() {
        return direccionDocente;
    }

    public void setDireccionDocente(String direccionDocente) {
        this.direccionDocente = direccionDocente;
    }
}
