package edu.ues.fia.inventarioues.Vistas.UnidadAdministrativa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;

import edu.ues.fia.inventarioues.Modelos.UnidadAdministrativa;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoEditarActivity;
import edu.ues.fia.inventarioues.Vistas.TipoMovimiento.TipoMovimientoEliminarAdvActivity;

public class UnidadAdministrativaBuscarActivity extends AppCompatActivity implements SearchView.OnQueryTextListener  {

    EditText editId, editNombre;
    SearchView searchConsultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unidad_administrativa_buscar);
        editId=(EditText)findViewById(R.id.UnidadAdministrativaConsultarId);
        editNombre=(EditText)findViewById(R.id.UnidadAdministrativaConsultarNombre);
        searchConsultar = (SearchView) findViewById(R.id.UnidadAdministrativa_searchConsultar);
        searchConsultar.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        TablasL helper= TablasL.getInstance(getApplicationContext());
        UnidadAdministrativa unidadAdministrativa= new UnidadAdministrativa();
        helper.open();
        unidadAdministrativa=helper.unidadAdministrativaConsultar(newText.toUpperCase());
        helper.close();
        if (unidadAdministrativa==null){
            editNombre.setText("");
            editId.setText("");
        }else {
            editId.setText(String.valueOf(unidadAdministrativa.getIdUnidadAdministrativa()));
            editNombre.setText(unidadAdministrativa.getNombreUnidadAdministrativa());
        }
        return false;
    }

    public void buscarEditarUnidadAdministrativa(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else{
            Intent intentEditar= new Intent(this, UnidadAdministrativaEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }


    public void buscarEliminarUnidadAdministrativa(View v){
        if (editNombre.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasL helper = TablasL.getInstance(getApplicationContext());
            helper.open();
            UnidadAdministrativa unidadAdministrativa= new UnidadAdministrativa();
            unidadAdministrativa.setIdUnidadAdministrativa(Integer.valueOf(editId.getText().toString()));
            unidadAdministrativa.setNombreUnidadAdministrativa(editNombre.getText().toString().toUpperCase());
            String mensaje= helper.eliminar1UnidadAdministrativa(unidadAdministrativa);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this, UnidadAdministrativaEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", unidadAdministrativa.getNombreUnidadAdministrativa());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unidad_administrativa,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_unidad_administrativa_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_unidad_administrativa_menu_insertar:
                Intent intent0= new Intent(this, UnidadAdministrativaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_unidad_administrativa_menu_consultar:
                Intent intent1= new Intent(this, UnidadAdministrativaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_unidad_administrativa_menu_Editar:
                Intent intente= new Intent(this, UnidadAdministrativaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_unidad_administrativa_menu_eliminar:
                Intent intent2= new Intent(this, UnidadAdministrativaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_unidad_administrativa_listar:
                Intent intent3= new Intent(this, UnidadAdministrativaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
