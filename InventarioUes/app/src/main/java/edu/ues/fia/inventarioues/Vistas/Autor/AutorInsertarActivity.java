package edu.ues.fia.inventarioues.Vistas.Autor;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Autor;
import edu.ues.fia.inventarioues.R;

public class AutorInsertarActivity extends AppCompatActivity {

    EditText editNombre, editApellido, editDireccion, editTelefono;
    Spinner spTipoAutor;
    Integer valorSelectTipoAutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autor_insertar);
        TablasV helper= TablasV.getInstance(getApplicationContext());
        editNombre =findViewById(R.id.AutorInsertarNombre);
        editApellido=findViewById(R.id.AutorInsertarApellido);
        editDireccion = findViewById(R.id.AutorInsertarDireccion);
        editTelefono = findViewById(R.id.AutorInsertarTelefono);
        spTipoAutor =findViewById(R.id.AutorInsertarTipoAutorId);


        Cursor autor=helper.obtenerTipoAutores();
        final List<Integer> TipoAutorIdList = new ArrayList<>();
        TipoAutorIdList.add(0);
        List<String> TipoAutorNombreList = new ArrayList<>();
        TipoAutorNombreList.add(getResources().getString(R.string.autorCampo6s));
        while (autor.moveToNext()){
            TipoAutorIdList.add(autor.getInt(0));
            TipoAutorNombreList.add(autor.getString(1));
        }
        spTipoAutor.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,TipoAutorIdList));
        spTipoAutor.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,TipoAutorNombreList));
        spTipoAutor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectTipoAutor=null;
                }else {
                    valorSelectTipoAutor=TipoAutorIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectTipoAutor=null;

            }
        });

    }

    public void AutorIngresar(View v){

        //{"idAutor", "idTipoAutor",
        //"nombreAutor","apellidoAutor","direccionAutor","telefonoAutor"};


        String idTipoAutor,nombreAutor, apellidoAutor, direccionAutor, telefonoAutor, mensaje;
        Integer tipoAutor;


        nombreAutor=editNombre.getText().toString().toUpperCase();
        tipoAutor=valorSelectTipoAutor;
        apellidoAutor = editApellido.getText().toString().toUpperCase();
        direccionAutor=editDireccion.getText().toString().toUpperCase();
        telefonoAutor = editTelefono.getText().toString();


        if (!(tipoAutor instanceof Integer)|| nombreAutor.isEmpty() || apellidoAutor.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.autorMsj1), Toast.LENGTH_LONG).show();
        }else {
            TablasV helper = TablasV.getInstance(getApplicationContext());
            Autor autor = new Autor();
            autor.setIdTipoAutor(tipoAutor);
            //prueba con guardar nombres y apellidos sin espacios en blanco
            autor.setNombreAutor(nombreAutor.replaceFirst("\\s++$", ""));
            autor.setApellidoAutor(apellidoAutor.replaceFirst("\\s++$", ""));
            autor.setTelefonoAutor(telefonoAutor);
            autor.setDireccionAutor(direccionAutor);

            helper.open();
            mensaje=helper.insertarAutor(autor);
            helper.close();


            editNombre.setText("");
            editApellido.setText("");
            editDireccion.setText("");
            editTelefono.setText("");
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_autor_menu_insertar:
                Intent intent0= new Intent(this, AutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_autor_menu_consultar:
                Intent intent1= new Intent(this, AutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_autor_menu_Editar:
                Intent intente= new Intent(this, AutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_autor_menu_eliminar:
                Intent intent2= new Intent(this, AutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_autor_listar:
                Intent intent3= new Intent(this, AutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
