package edu.ues.fia.inventarioues;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.glxn.qrgen.android.QRCode;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


public class NuevoCodigoActivity extends AppCompatActivity {
    TextView titulo;
    ImageView imgCod;
    String codigo;
    Bitmap bitmap;
    String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Backup/";
    OutputStream outputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT>=23){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},2);
        }
        File file = new File(directory_path);
        if (!file.exists()) {
            Log.v("File Created", String.valueOf(file.mkdirs()));
        }
        setContentView(R.layout.activity_nuevo_codigo);
        titulo=findViewById(R.id.codigoTitulo);
        imgCod=findViewById(R.id.codigoImg);
        Bundle bundle=getIntent().getExtras();
        titulo.setText(bundle.getString("titulo"));
        codigo=bundle.getString("codigoQr");
        bitmap=QRCode.from(bundle.getString("codigoQr")).withSize(300,300).bitmap();
        imgCod.setImageBitmap(bitmap);
    }


    public void guardarGaleriaCodigo(View v){

        File filepath = Environment.getExternalStorageDirectory();
        File dir = new File(filepath.getAbsolutePath() +"/Qr/");
        dir.mkdir();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String nombreCod="QR_"+timeStamp;
        File file = new File(dir,nombreCod + ".jpg");
        try {
            outputStream = new FileOutputStream(file);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
        Toast.makeText(getApplicationContext(),"Image save to internall!",
                Toast.LENGTH_SHORT).show();

        try {
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }









}
