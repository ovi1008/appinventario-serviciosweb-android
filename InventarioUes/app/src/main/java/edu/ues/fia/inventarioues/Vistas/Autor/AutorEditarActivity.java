package edu.ues.fia.inventarioues.Vistas.Autor;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Autor;
import edu.ues.fia.inventarioues.R;

public class AutorEditarActivity extends AppCompatActivity {
    EditText nombreBusqueda, apellidoBusqueda;
    EditText editNombre, editApellido, editDireccion, editTelefono;
    Spinner spTipoAutor;
    Integer valorSelectTipoAutor;
    String clave="";
    int idAutor;
    int opcion=0;
    Button botonBuscar;

    //ADDED
    final List<Integer> TipoAutorIdList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autor_editar);
        TablasV helper= TablasV.getInstance(getApplicationContext());

        nombreBusqueda =findViewById(R.id.AutorEditarNombre);
        apellidoBusqueda=findViewById(R.id.AutorEditarApellido);
        botonBuscar =findViewById(R.id.AutorEditarBtnBuscar);

        editNombre =findViewById(R.id.AutorEditarNombreCampo);
        editApellido=findViewById(R.id.AutorEditarApellidoCampo);
        editDireccion = findViewById(R.id.AutorEditarDireccionCampo);
        editTelefono = findViewById(R.id.AutorEditarTelefonoCampo);
        spTipoAutor =findViewById(R.id.AutorEditarTipoAutorId);

        editNombre.setEnabled(false);
        editApellido.setEnabled(false);
        editDireccion.setEnabled(false);
        editTelefono.setEnabled(false);



        //editNombre.setKeyListener(null);

        //DEfinir quien le esta enviando esto

        helper.open();
        Cursor autor=helper.obtenerTipoAutores();

        //final List<Integer> TipoAutorIdList = new ArrayList<>();
        TipoAutorIdList.add(0);
        List<String> TipoAutorNombreList = new ArrayList<>();
        TipoAutorNombreList.add(getResources().getString(R.string.autorCampo6s));
        while (autor.moveToNext()){
            TipoAutorIdList.add(autor.getInt(0));
            TipoAutorNombreList.add(autor.getString(1));
        }
        helper.close();
        spTipoAutor.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,TipoAutorIdList));
        spTipoAutor.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,TipoAutorNombreList));
        spTipoAutor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectTipoAutor=null;
                }else {
                    valorSelectTipoAutor=TipoAutorIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectTipoAutor=null;
                //editar para que tenga por defecto el valor que ya tenia.

            }
        });

        Bundle bundle=getIntent().getExtras();
        if (bundle!=null){
            idAutor= bundle.getInt("id");
            editNombre.setText(bundle.getString("nombre"));

            editApellido.setText(bundle.getString("apellido"));
            editDireccion.setText(bundle.getString("direccion"));
            editTelefono.setText(bundle.getString("telefono"));
            //spTipoAutor.setSelection(bundle.getInt("idTipoAutor"));
            clave=bundle.getString("nombre")+ bundle.getString("apellido");
            opcion=1;
            nombreBusqueda.setVisibility(View.INVISIBLE);
            apellidoBusqueda.setVisibility(View.INVISIBLE);
            botonBuscar.setVisibility(View.INVISIBLE);
            editNombre.setEnabled(true);
            editApellido.setEnabled(true);
            editDireccion.setEnabled(true);
            editTelefono.setEnabled(true);

            valorSelectTipoAutor=bundle.getInt("idTipoAutor");

            int posicionEnListaTipoAutores = TipoAutorIdList.indexOf(bundle.getInt("idTipoAutor"));
            spTipoAutor.setSelection(posicionEnListaTipoAutores);


            //editBusca.setVisibility(View.INVISIBLE);
            //busca.setVisibility(View.INVISIBLE)

        }
    }

    public void buscaAutorEditar(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());

        System.out.println(nombreBusqueda.getText().toString().toUpperCase());
        System.out.println(apellidoBusqueda.getText().toString().toUpperCase());
        helper.open();
        Autor autor=helper.AutorConsultar(nombreBusqueda.getText().toString().toUpperCase().replaceFirst("\\s++$", ""),apellidoBusqueda.getText().toString().toUpperCase().replaceFirst("\\s++$", ""));
        helper.close();
        if (autor==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            //obtener el id para saber que autor actualizar
            idAutor=autor.getIdAutor();

            editNombre.setText(autor.getNombreAutor());
            editNombre.setEnabled(true);
            //activar los campos
            editApellido.setText(autor.getApellidoAutor());
            editApellido.setEnabled(true);
            editDireccion.setText(autor.getDireccionAutor());
            editDireccion.setEnabled(true);
            editTelefono.setText(autor.getTelefonoAutor());
            editTelefono.setEnabled(true);
            valorSelectTipoAutor =autor.getIdTipoAutor();

            int posicionEnListaTipoAutores = TipoAutorIdList.indexOf(valorSelectTipoAutor);
            spTipoAutor.setSelection(posicionEnListaTipoAutores);

            nombreBusqueda.setText("");
            apellidoBusqueda.setText("");

            //recuperar id seleccionado
        }
    }

    public void actualizarAutor(View v){
        if (editNombre.getText().toString().isEmpty()){
            // tal vez cambiar el mensaje para cuando se este dejando en blanco un campo al editar
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasV helper= TablasV.getInstance(getApplicationContext());
            Autor autor = new Autor();
            autor.setIdAutor(idAutor);
            autor.setNombreAutor(editNombre.getText().toString().toUpperCase().replaceFirst("\\s++$", ""));
            autor.setApellidoAutor(editApellido.getText().toString().toUpperCase().replaceFirst("\\s++$", ""));
            autor.setDireccionAutor(editDireccion.getText().toString());
            autor.setTelefonoAutor(editTelefono.getText().toString());
            autor.setIdTipoAutor(valorSelectTipoAutor);

            helper.open();
            //String mensaje=helper.actualizarAutor(autor,autor.getNombreAutor(),autor.getApellidoAutor());
            String mensaje=helper.actualizarAutorTest(autor,autor.getIdAutor());
            helper.close();
            if (opcion==1){
                Intent intOp1=new Intent(this,AutorMenuActivity.class);
                this.startActivity(intOp1);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();

                editNombre.setText("");
                editApellido.setText("");
                editDireccion.setText("");
                editTelefono.setText("");
                //prueba
                spTipoAutor.setSelection(0);
                clave="";
                //editId.setEnabled(false);
                editNombre.setEnabled(false);
                //busca.setEnabled(true);
                //editBusca.setEnabled(true);
            }
        }


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_autor_menu_insertar:
                Intent intent0= new Intent(this, AutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_autor_menu_consultar:
                Intent intent1= new Intent(this, AutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_autor_menu_Editar:
                Intent intente= new Intent(this, AutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_autor_menu_eliminar:
                Intent intent2= new Intent(this, AutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_autor_listar:
                Intent intent3= new Intent(this, AutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
