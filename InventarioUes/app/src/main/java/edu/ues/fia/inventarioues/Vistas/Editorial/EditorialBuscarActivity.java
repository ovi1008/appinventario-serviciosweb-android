package edu.ues.fia.inventarioues.Vistas.Editorial;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.TipoEquipo.TipoEquipoConsultarActivity;


@SuppressLint("NewApi")
public class EditorialBuscarActivity  extends AppCompatActivity implements SearchView.OnQueryTextListener {

    EditText editId,editNombre, editNombreServer;;
    SearchView searchConsultar;
    TextToSpeech toSpeech;
    int result;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorial_buscar);
        searchConsultar=(SearchView) findViewById(R.id.Editorial_searchConsultar);
        editId=(EditText)findViewById(R.id.EditorialConsultarId);
        editNombreServer=findViewById(R.id.EditorialConsultarNombreServidor);
        editNombre=(EditText)findViewById(R.id.EditorialConsultarNombre);
        searchConsultar.setOnQueryTextListener(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        toSpeech=new TextToSpeech(EditorialBuscarActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    result=toSpeech.setLanguage(Locale.getDefault());
                }else{
                    Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void playEditorial(View v){
        if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
            Toast.makeText(getApplicationContext(), "No soprta la libreria el dispositivo", Toast.LENGTH_LONG).show();
        }else {
            String textoHablar;
            if (editId.getText().toString().isEmpty()){
                textoHablar="Realice una consulta primero";
            }else {
                textoHablar="Se consulta la siguiente editorial:\n\n" +
                        "Codigo de la editorial es: " +editId.getText().toString()+"\n\n"+
                        "El nombre de la editorial es: "+editNombre.getText().toString();
            }
            toSpeech.speak(textoHablar,TextToSpeech.QUEUE_FLUSH,null);
        }
    }

    public void stopEditorial(View v){
        if (toSpeech!=null){
            toSpeech.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (toSpeech!=null){
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }

    public void consultarEditorialServidor(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.editorialMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/editorial/ws_editorial_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/Editorial/ws_Editorial_query.php?";

                String url = url1+ip+url3+
                        "nombreEditorial="+editNombreServer.getText().toString().toUpperCase();
                String jsonEditorial= ServiciosWeb.consultarServidorLocal(url, this);
                editNombreServer.setText("");
                if (jsonEditorial==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datosEditorial=jsonEditorial.split(",");
                    String []idEditorial=datosEditorial[0].split(":");
                    String []nombreEditorial=datosEditorial[1].split(":");
                    Editorial EditorialS = new Editorial();
                    EditorialS.setIdEditorial(Integer.valueOf(idEditorial[1]));
                    EditorialS.setNombreEditorial(nombreEditorial[1]);
                    editId.setText(String.valueOf(EditorialS.getIdEditorial()));
                    editNombre.setText(EditorialS.getNombreEditorial());
                }


            }

        }
    }


    @Override
    public boolean onQueryTextChange(String newText) {
        TablasV helper= TablasV.getInstance(getApplicationContext());
        Editorial editorial= new Editorial();
        helper.open();
        editorial=helper.editorialConsultar(newText.toUpperCase());
        helper.close();
        if (editorial==null){
            editId.setText("");
            editNombre.setText("");
        }else {
            editId.setText(String.valueOf(editorial.getIdEditorial()));
            editNombre.setText(editorial.getNombreEditorial());
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void buscarEditarEditorial(View v){
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            Intent intentEditar= new Intent(this, EditorialEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("id",editId.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarEditorial(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if(editId.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Editorial editorial= new Editorial();
            editorial.setIdEditorial(Integer.valueOf(editId.getText().toString()));
            editorial.setNombreEditorial(editNombre.getText().toString());
            String mensaje= helper.eliminar1Editorial(editorial);
            helper.close();
            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,EditorialEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", editorial.getNombreEditorial());
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }

    public void eliminarBuscarEditorialServidor(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombre.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.editorialMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/editorial/ws_editorial_delete.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/editorial/ws_editorial_delete.php?";

                String url = url1+ip+url3+
                        "nombreEditorial="+editNombre.getText().toString().toUpperCase();
                String msj= ServiciosWeb.eliminarServidorLocal(url, this);

                Toast.makeText(this, msj+"la editorial: "+editNombre.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();

                editNombre.setText("");
                editId.setText("");
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editorial,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_editorial_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_editorial_menu_insertar:
                Intent intent0= new Intent(this, EditorialInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_editorial_menu_Editar:
                Intent intente= new Intent(this, EditorialEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_editorial_menu_consultar:
                Intent intent1= new Intent(this, EditorialBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_editorial_menu_eliminar:
                Intent intent2= new Intent(this, EditorialEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_editorial_listar:
                Intent intent3= new Intent(this, EditorialMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_editorial_listarServidor:
                Intent intent4= new Intent(this, EditorialListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_editorial_listarSincronizar:
                Intent intent5= new Intent(this, EditorialSincronizacionActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
