package edu.ues.fia.inventarioues.Vistas.TipoDocumento;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasM;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.TipoDocumento;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class TipoDocumentoSincronizarActivity extends AppCompatActivity {

    TextView txtTitulo;
    ListView listSincro;
    Button local, servidor;
    List<String> tipoDocumentosNoRepetidas=new ArrayList<>();

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_sincronizar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        txtTitulo=findViewById(R.id.TipoDocumentoSincronizarTitulo);
        listSincro=findViewById(R.id.listTipoDocumentoSincro);
        local=findViewById(R.id.TipoDocumentoSincronizarGuardarL);
        servidor=findViewById(R.id.TipoDocumentoSincronizarGuardarS);
    }

    public void TipoDocumentoSincroLocal(View v){
        TablasM helper= TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>tipoDocumentosLocal=listadoTipoDocumentosLocal();
            List<String>tipoDocumentosServidor=listadoTipoDocumentosServidor();
            tipoDocumentosNoRepetidas=tipoDocumentosServidor;
            tipoDocumentosNoRepetidas.removeAll(tipoDocumentosLocal);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, tipoDocumentosNoRepetidas);
            listSincro.setAdapter(adapter);
            if (tipoDocumentosNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                local.setVisibility(View.VISIBLE);
                servidor.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void TipoDocumentoSincroServidor(View v){
        TablasM helper= TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }else {
            List<String>tipoDocumentosLocal=listadoTipoDocumentosLocal();
            List<String>tipoDocumentosServidor=listadoTipoDocumentosServidor();
            tipoDocumentosNoRepetidas=tipoDocumentosLocal;
            tipoDocumentosNoRepetidas.removeAll(tipoDocumentosServidor);
            ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, tipoDocumentosNoRepetidas);
            listSincro.setAdapter(adapter);
            if (tipoDocumentosNoRepetidas.isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.sincronizado01), Toast.LENGTH_LONG).show();
            }else {
                txtTitulo.setText(getResources().getString(R.string.btnSincroL));
                servidor.setVisibility(View.VISIBLE);
                local.setVisibility(View.INVISIBLE);
            }
        }
    }

    public List<String> listadoTipoDocumentosLocal(){
        TablasM helper = TablasM.getInstance(getApplicationContext());
        Cursor c =helper.obtenerTiposDocumento();
        List<String> tipoDocumentoNombreList= new ArrayList<>();
        while (c.moveToNext()){
            //camposTipoDocumento= new String[]{"idTipoDocumento", "nombreTipoDocumento"}
            tipoDocumentoNombreList.add(c.getString(1));
        }
        return tipoDocumentoNombreList;
    }

    public List<String> listadoTipoDocumentosServidor(){
        TablasM helper= TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_queryAll.php";
        //            String urlLocal = "http://192.168.0.11/inventarioUes/tipoDocumento/ws_tipoDocumento_queryDate.php?";

        String url = url1+ip+url3;
        String json= ServiciosWeb.consultarFechaLocal(url,this);
        List<String> tipoDocumentoNombreList= new ArrayList<>();
        if (json.length()==0){}else{
            String[]datosTipoDocumento=json.split("&&");
            for (String dato:datosTipoDocumento){
                String []datoP=dato.split(",");
                String []nombreTipoDocumento=datoP[1].split(":");
                //camposTipoDocumento= new String[]{"idTipoDocumento", "nombreTipoDocumento"}
                tipoDocumentoNombreList.add(nombreTipoDocumento[1]);
            }
        }
        return tipoDocumentoNombreList;
    }

    public void guardarTipoDocumentosLocal(View v){
        TablasM helper=TablasM.getInstance(getApplicationContext());
        helper.open();
        for (String tipoDocumento:tipoDocumentosNoRepetidas){
            TipoDocumento tipoDocumentoNlocal=new TipoDocumento();
            tipoDocumentoNlocal.setNombreTipoDocumento(tipoDocumento);
            helper.insertarTipoDocumento(tipoDocumentoNlocal);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        tipoDocumentosNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        local.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }

    public void guardarTipoDocumentosServidor(View v){
        TablasM helper=TablasM.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        for (String tipoDocumento:tipoDocumentosNoRepetidas){
            TipoDocumento tipoDocumentoNservidor=new TipoDocumento();
            tipoDocumentoNservidor.setNombreTipoDocumento(tipoDocumento);
            String url1="http://";
            String url3="/inventarioUes/tipoDocumento/ws_tipoDocumento_insert.php?";
            //            String urlLocal = "http://192.168.0.11/inventarioUes/tipoDocumento/ws_tipoDocumento_insert.php?";

            String url = url1+ip+url3+
                    "nombreTipoDocumento="+tipoDocumentoNservidor.getNombreTipoDocumento();
            ServiciosWeb.insertarServidorLocal(url, this);
        }
        helper.close();
        txtTitulo.setText("");
        List<String>vacia=new ArrayList<>();
        tipoDocumentosNoRepetidas=vacia;
        ArrayAdapter adapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, vacia);
        listSincro.setAdapter(adapter);
        servidor.setVisibility(View.INVISIBLE);
        Toast.makeText(this,getResources().getString(R.string.sincronizado02),Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tipo_documento,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_tipo_documento_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_tipo_documento_menu_insertar:
                Intent intent0= new Intent(this, TipoDocumentoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_tipo_documento_menu_consultar:
                Intent intent1= new Intent(this, TipoDocumentoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_tipo_documento_menu_Editar:
                Intent intente= new Intent(this, TipoDocumentoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_tipo_documento_menu_eliminar:
                Intent intent2= new Intent(this, TipoDocumentoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_tipo_documento_listar:
                Intent intent3= new Intent(this, TipoDocumentoMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_tipo_documento_listarServidor:
                Intent intent4= new Intent(this, TipoDocumentoListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_tipo_documento_listarSincronizar:
                Intent intent5= new Intent(this, TipoDocumentoSincronizarActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
