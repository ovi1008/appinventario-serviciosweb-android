package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.Modelos.Materia;
import edu.ues.fia.inventarioues.R;

public class MateriaAdaptador extends ArrayAdapter<Materia> {

    public MateriaAdaptador(@NonNull Context context, @NonNull List<Materia> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_materia,parent,false);
        }
        TextView idMateria=convertView.findViewById(R.id.materiaListarId);
        TextView nombreMateria=convertView.findViewById(R.id.materiaListarNombre);

        Materia materia=getItem(position);
        idMateria.setText(materia.getIdMateria());
        nombreMateria.setText(materia.getNombreMateria());

        return convertView;
    }
}
