package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;

import edu.ues.fia.inventarioues.R;
@SuppressLint("NewApi")
public class IdiomaInsertarActivity extends AppCompatActivity {
    EditText editNombreIdioma;
    private static final int recordImput=1111;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_idioma_insertar);
        editNombreIdioma=(EditText)findViewById(R.id.IdiomaInsertarNombre);
    }

    public void insertarIdioma(View v){
        String nombre, mensaje;
        TablasL helper= TablasL.getInstance(getApplicationContext());
        nombre=editNombreIdioma.getText().toString().toUpperCase();
        if (nombre.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.idiomaMsj1), Toast.LENGTH_LONG).show();
        }else{
            Idioma idioma= new Idioma();
            idioma.setNombreIdioma(nombre);
            helper.open();
            mensaje=helper.insertarIdioma(idioma);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            editNombreIdioma.setText("");
        }


    }

    public void iniciarVozIdioma(View v){
        Intent intentVoz= new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intentVoz.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intentVoz.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intentVoz.putExtra(RecognizerIntent.EXTRA_PROMPT, "Dicta el idioma");
        try {
            startActivityForResult(intentVoz,recordImput);
        }catch (ActivityNotFoundException e){
            e.getStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case recordImput:{
                if(resultCode==RESULT_OK && null!=data){
                    ArrayList<String> result=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editNombreIdioma.setText(result.get(0));
                }
                break;
            }
        }
    }


    public void insertarIdiomaServidor(View v){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreIdioma.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.idiomaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/idioma/ws_idioma_insert.php?";
//            String urlLocal = "http://192.168.0.12/inventarioUes/idioma/ws_idioma_insert.php?";

                String url = url1+ip+url3+
                        "nombreIdioma="+editNombreIdioma.getText().toString().toUpperCase();
                String msj= ServiciosWeb.insertarServidorLocal(url, this);
                Toast.makeText(this, msj+"el idioma: "+editNombreIdioma.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();
                editNombreIdioma.setText("");
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_idioma_listarServidor:
                Intent intent4= new Intent(this, IdiomaListarServidor2Activity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_idioma_listarSincronizar:
                Intent intent5= new Intent(this, IdiomaSincronizar2Activity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
