package edu.ues.fia.inventarioues.Vistas.Materia;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Adaptador.MateriaAdaptador;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Materia;
import edu.ues.fia.inventarioues.R;

public class MateriaMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_menu);
        ListView listView =(ListView)findViewById(R.id.listMateria);
        List<Materia> lista= listadoMaterias();
        MateriaAdaptador materiaAdaptador=new MateriaAdaptador(this,lista);
        listView.setAdapter(materiaAdaptador);
    }

    public List<Materia> listadoMaterias(){
        TablasL helper = TablasL.getInstance(getApplicationContext());
        Cursor c =helper.obtenerMaterias();
        List<Materia> materiaList= new ArrayList<>();
        while (c.moveToNext()){
            Materia materia = new Materia();
            //camposMateria= new String[]{"idMateria", "nombreMateria"}
            materia.setIdMateria(c.getString(0));
            materia.setNombreMateria(c.getString(1));
            materiaList.add(materia);
        }
        return materiaList;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_materia,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_materia_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_materia_menu_insertar:
                Intent intent0= new Intent(this, MateriaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_materia_menu_consultar:
                Intent intent1= new Intent(this, MateriaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_materia_menu_Editar:
                Intent intente= new Intent(this, MateriaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_materia_menu_eliminar:
                Intent intent2= new Intent(this, MateriaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_materia_listar:
                Intent intent3= new Intent(this, MateriaMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
