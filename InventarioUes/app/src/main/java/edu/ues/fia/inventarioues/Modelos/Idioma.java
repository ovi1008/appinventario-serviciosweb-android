package edu.ues.fia.inventarioues.Modelos;

public class Idioma {
    //{"codIdioma", "nombreIdioma"};
    int codIdioma;
    String nombreIdioma, fechaModificadaIdioma;

    public Idioma() {
    }

    public Idioma(int codIdioma, String nombreIdioma, String fechaModificadaIdioma) {
        this.codIdioma = codIdioma;
        this.nombreIdioma = nombreIdioma;
        this.fechaModificadaIdioma = fechaModificadaIdioma;
    }

    public int getCodIdioma() {
        return codIdioma;
    }

    public void setCodIdioma(int codIdioma) {
        this.codIdioma = codIdioma;
    }

    public String getNombreIdioma() {
        return nombreIdioma;
    }

    public void setNombreIdioma(String nombreIdioma) {
        this.nombreIdioma = nombreIdioma;
    }

    public String getFechaModificadaIdioma() {
        return fechaModificadaIdioma;
    }

    public void setFechaModificadaIdioma(String fechaModificadaIdioma) {
        this.fechaModificadaIdioma = fechaModificadaIdioma;
    }
}
