package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;

public class EditorialAdaptador extends ArrayAdapter<Editorial> {

    public EditorialAdaptador(@NonNull Context context, @NonNull List<Editorial> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_editorial,parent,false);
        }
        TextView idEditorial=convertView.findViewById(R.id.editorialListarId);
        TextView nombreEditorial=convertView.findViewById(R.id.editorialListarNombre);

        Editorial editorial=getItem(position);
        idEditorial.setText(String.valueOf(editorial.getIdEditorial()));
        nombreEditorial.setText(editorial.getNombreEditorial());

        return convertView;
    }
}
