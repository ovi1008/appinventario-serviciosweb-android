package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.Modelos.Prestamo;
import edu.ues.fia.inventarioues.R;

public class PrestamoAdaptador001 extends ArrayAdapter<Prestamo> {

    public PrestamoAdaptador001(@NonNull Context context, @NonNull List<Prestamo> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.listado_personalizado_prestamo, parent, false);
        }

        TextView idPrestamo = convertView.findViewById(R.id.prestamoIdListar);
        TextView objetiPrestamo = convertView.findViewById(R.id.prestamoObjetoListar);
        TextView estadoPrestamo = convertView.findViewById(R.id.prestamoEstadoListar);
        Prestamo prestamo = getItem(position);
        idPrestamo.setText(String.valueOf(prestamo.getIdMovimientoInventario()));
        objetiPrestamo.setText("");
        if (prestamo.getEstadoMovInv() == 1) {
            estadoPrestamo.setText("ACTIVO");
            estadoPrestamo.setTextColor(Color.rgb(100,221,23));
        }else {
            estadoPrestamo.setText("FINALIZADO");
            estadoPrestamo.setTextColor(Color.rgb(213,0,0));
        }

        TablasV helper = TablasV.getInstance(getContext());
        helper.open();
        Cursor c=helper.obtenerDocumentos();
        while (c.moveToNext() && objetiPrestamo.getText().toString().isEmpty()){
            if (c.getInt(0)==prestamo.getIdDocumento()){
                objetiPrestamo.setText("IDLib:"+prestamo.getIdDocumento()+" Nombre: "+c.getString(5));
            }
        }
        helper.close();
        return convertView;
    }

}

