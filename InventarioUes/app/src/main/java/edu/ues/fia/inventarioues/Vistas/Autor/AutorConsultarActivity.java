package edu.ues.fia.inventarioues.Vistas.Autor;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.TablasV;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Autor;
import edu.ues.fia.inventarioues.Modelos.Editorial;
import edu.ues.fia.inventarioues.R;

public class AutorConsultarActivity extends AppCompatActivity {

    EditText editNombre,editApellido,editDireccion,editTelefono,editTipoAutor;
    int idAutor;
    int idTipoAutor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autor_consultar);
        editNombre =findViewById(R.id.AutorConsultarNombre);
        editApellido=findViewById(R.id.AutorConsultarApellido);
        editDireccion=findViewById(R.id.AutorConsultarDireccion);
        editTelefono=findViewById(R.id.AutorConsultarTelefono);
        editTipoAutor=findViewById(R.id.AutorConsultarTipoAutor);

    }

    public void buscarAutor(View view){
        if (editNombre.getText().toString().isEmpty() ||editApellido.getText().toString().isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.autorMsj1a), Toast.LENGTH_LONG).show();
        }
        else{
            TablasV helper= TablasV.getInstance(getApplicationContext());
            String nombre =editNombre.getText().toString().toUpperCase().replaceFirst("\\s++$", "");
            String apellido=editApellido.getText().toString().toUpperCase().replaceFirst("\\s++$", "");
            Autor autor =new Autor();
            helper.open();
            autor =helper.AutorConsultar(nombre,apellido);
            helper.close();

            //codigo para saber tipo de autor
            if(autor==null){
                Toast.makeText(this,getResources().getString(R.string.msj2Estad),Toast.LENGTH_LONG).show();
                editDireccion.setText("");
                editTelefono.setText("");
                editTipoAutor.setText("");
            }

            else{
                idTipoAutor =autor.getIdTipoAutor();
                helper.open();
                String tipoAutor =helper.consultarTipoAutor(String.valueOf(autor.getIdTipoAutor()));
                helper.close();

//                if(autor.getTelefonoAutor().isEmpty()){
//                    editTelefono.setText("Desconocido");
//                }
//                else{
                    editTelefono.setText(autor.getTelefonoAutor());
//                }
//
//                if(autor.getDireccionAutor().isEmpty()){
//                    editDireccion.setText("Desconocido");
//                }
//                else{
                    editDireccion.setText(autor.getDireccionAutor());
//                }

                editTipoAutor.setText(tipoAutor);

                idAutor =autor.getIdAutor();
            }

        }
    }

    public void buscarEditarAutor(View v){
        if(editTipoAutor.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            Intent intentEditar= new Intent(this, AutorEditarActivity.class);
            intentEditar.putExtra("nombre", editNombre.getText().toString());
            intentEditar.putExtra("apellido",editApellido.getText().toString());
            intentEditar.putExtra("id",idAutor);
            intentEditar.putExtra("idTipoAutor",idTipoAutor);
            intentEditar.putExtra("direccion",editDireccion.getText().toString());
            intentEditar.putExtra("telefono",editTelefono.getText().toString());
            this.startActivity(intentEditar);
        }
    }

    public void buscarEliminarAutor(View v){
        TablasV helper = TablasV.getInstance(getApplicationContext());
        if(editNombre.getText().toString().isEmpty() || editApellido.getText().toString().isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj1Estad),Toast.LENGTH_LONG).show();
        }else {
            helper.open();
            Autor autor= new Autor();
            autor.setNombreAutor(editNombre.getText().toString().toUpperCase().replaceFirst("\\s++$", ""));
            autor.setApellidoAutor(editApellido.getText().toString().toUpperCase().replaceFirst("\\s++$", ""));
            //Necesitamos el tipo autor
            String mensaje= helper.eliminar1Autor(autor);
            helper.close();

            if(mensaje.length()<5){
                Intent intentEliminar = new Intent(this,AutorEliminarAdvertenciaActivity.class);
                intentEliminar.putExtra("msj", mensaje);
                intentEliminar.putExtra("nombre", autor.getNombreAutor());
                //falta poner el apellido
                this.startActivity(intentEliminar);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_autor,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_autor_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_autor_menu_insertar:
                Intent intent0= new Intent(this, AutorInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_autor_menu_Editar:
                Intent intente= new Intent(this, AutorEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_autor_menu_consultar:
                Intent intent1= new Intent(this, AutorConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_autor_menu_eliminar:
                Intent intent2= new Intent(this, AutorEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_autor_listar:
                Intent intent3= new Intent(this, AutorMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
