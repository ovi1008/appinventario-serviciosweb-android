package edu.ues.fia.inventarioues.Vistas.Excel;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ajts.androidmads.library.SQLiteToExcel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.inventarioues.Controladores.DatabaseOpenHelper;
import edu.ues.fia.inventarioues.R;

public class ExcelExportActivity extends AppCompatActivity {
    TextView txt;
    String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Backup/";
    SQLiteToExcel sqliteToExcel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excel_export);
        txt=findViewById(R.id.excelExportText);
        //Creacion de carpeta donde se guarda
        File file = new File(directory_path);
        if (!file.exists()) {
            Log.v("File Created", String.valueOf(file.mkdirs()));
        }

    }

    public void sacarBase(View v){
        sqliteToExcel = new SQLiteToExcel(this, DatabaseOpenHelper.getDatabseName(), directory_path);
        List<String>tablas=new ArrayList<String>();
        tablas.add("autor");
        tablas.add("docente");
        tablas.add("documento");
        tablas.add("editorial");
        tablas.add("equipo");
        tablas.add("horario");
        tablas.add("idioma");
        tablas.add("marca");
        tablas.add("materia");
        tablas.add("motivo");
        tablas.add("movimientoInventario");
        tablas.add("tipoAutor");
        tablas.add("tipoDocumento");
        tablas.add("tipoEquipo");
        tablas.add("tipoMovimiento");
        tablas.add("unidadAdministrativa");
        tablas.add("usuario");
        tablas.add("usuarioAccesos");
        sqliteToExcel.exportSpecificTables(tablas,"inventario.xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onCompleted(String filePath) {
                txt.setText("Se guardo un archivo llamado \"inventario.xls\" en la siguiente ruta:\n\n\n" +
                        "MisArchivos\\AlmacenamientoInterno\\Backup\\");
            }

            @Override
            public void onError(Exception e) {
                txt.setText(e.getMessage());
            }
        });
    }
}
