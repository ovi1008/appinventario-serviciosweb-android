package edu.ues.fia.inventarioues.Modelos;

public class Prestamo {
    Integer idMovimientoInventario, idTipoMovimiento, estadoMovInv, idMotivo, idHorario, idUnidadAdministrativa,
            idDocumento;
    String carnetDocente, fechaMovimientoInventario, fechaDevolucionMovInv ,serialEquipo,idMateria;

    public Prestamo() {
    }

    public Prestamo(Integer idMovimientoInventario, Integer idTipoMovimiento, Integer estadoMovInv, Integer idMotivo, Integer idHorario, Integer idUnidadAdministrativa, Integer idDocumento, String carnetDocente, String fechaMovimientoInventario, String fechaDevolucionMovInv, String serialEquipo, String idMateria) {
        this.idMovimientoInventario = idMovimientoInventario;
        this.idTipoMovimiento = idTipoMovimiento;
        this.estadoMovInv = estadoMovInv;
        this.idMotivo = idMotivo;
        this.idHorario = idHorario;
        this.idUnidadAdministrativa = idUnidadAdministrativa;
        this.idDocumento = idDocumento;
        this.carnetDocente = carnetDocente;
        this.fechaMovimientoInventario = fechaMovimientoInventario;
        this.fechaDevolucionMovInv = fechaDevolucionMovInv;
        this.serialEquipo = serialEquipo;
        this.idMateria = idMateria;
    }

    public Integer getIdMovimientoInventario() {
        return idMovimientoInventario;
    }

    public void setIdMovimientoInventario(Integer idMovimientoInventario) {
        this.idMovimientoInventario = idMovimientoInventario;
    }

    public Integer getIdTipoMovimiento() {
        return idTipoMovimiento;
    }

    public void setIdTipoMovimiento(Integer idTipoMovimiento) {
        this.idTipoMovimiento = idTipoMovimiento;
    }

    public Integer getEstadoMovInv() {
        return estadoMovInv;
    }

    public void setEstadoMovInv(Integer estadoMovInv) {
        this.estadoMovInv = estadoMovInv;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public Integer getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Integer idHorario) {
        this.idHorario = idHorario;
    }

    public Integer getIdUnidadAdministrativa() {
        return idUnidadAdministrativa;
    }

    public void setIdUnidadAdministrativa(Integer idUnidadAdministrativa) {
        this.idUnidadAdministrativa = idUnidadAdministrativa;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getCarnetDocente() {
        return carnetDocente;
    }

    public void setCarnetDocente(String carnetDocente) {
        this.carnetDocente = carnetDocente;
    }

    public String getFechaMovimientoInventario() {
        return fechaMovimientoInventario;
    }

    public void setFechaMovimientoInventario(String fechaMovimientoInventario) {
        this.fechaMovimientoInventario = fechaMovimientoInventario;
    }

    public String getFechaDevolucionMovInv() {
        return fechaDevolucionMovInv;
    }

    public void setFechaDevolucionMovInv(String fechaDevolucionMovInv) {
        this.fechaDevolucionMovInv = fechaDevolucionMovInv;
    }

    public String getSerialEquipo() {
        return serialEquipo;
    }

    public void setSerialEquipo(String serialEquipo) {
        this.serialEquipo = serialEquipo;
    }

    public String getIdMateria() {
        return idMateria;
    }

    public void setIdMateria(String idMateria) {
        this.idMateria = idMateria;
    }
}
