package edu.ues.fia.inventarioues.Vistas.Excel;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ajts.androidmads.library.ExcelToSQLite;

import java.io.File;

import edu.ues.fia.inventarioues.Controladores.DatabaseOpenHelper;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.R;

public class ExcelImportActivity extends AppCompatActivity {
    EditText editRuta;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excel_import);
        editRuta=findViewById(R.id.excelImporRouta);
        txt=findViewById(R.id.excelImportText);
    }

    public void meterBase(View v){

        String directory_path = Environment.getExternalStorageDirectory().getPath() +editRuta.getText().toString();
        File file = new File(directory_path);
        if (!file.exists()) {
            txt.setText("No se encuentra el archivo en:\n\n "+editRuta.getText().toString());
            return;
        }

        TablasJ helper =TablasJ.getInstance(getApplicationContext());
        helper.open();
        // Is used to import data from excel without dropping table
        // ExcelToSQLite excelToSQLite = new ExcelToSQLite(getApplicationContext(), DBHelper.DB_NAME);

        // if you want to add column in excel and import into DB, you must drop the table
        ExcelToSQLite excelToSQLite = new ExcelToSQLite(getApplicationContext(), DatabaseOpenHelper.getDatabseName(), false);
        // Import EXCEL FILE to SQLite
        excelToSQLite.importFromFile(directory_path, new ExcelToSQLite.ImportListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onCompleted(String dbName) {
                txt.setText("Se importo con exito dentro de:\n\n"+dbName);
            }

            @Override
            public void onError(Exception e) {
                txt.setText("Error:\n\n"+e.getMessage());
            }
        });
        helper.close();
    }
}