package edu.ues.fia.inventarioues;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;

@SuppressLint("NewApi")
public class IPActivity extends AppCompatActivity {

    EditText editIp;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ip);
        editIp=findViewById(R.id.InsertarIp);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }

    public void ingresarIp(View v){
        if (editIp.getText().toString().isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpCampo), Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper= TablasJ.getInstance(getApplicationContext());
            helper.open();
            String msj=helper.ingresarMiIpServer(editIp.getText().toString());
            helper.close();

            String url1="http://";
            String url3="/inventarioUes/marca/ws_marca_insert.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_insert.php?";
            String url = url1+editIp.getText().toString()+url3+
                    "nombreMarca=HP";

            String json= ServiciosWeb.obtenerRespuestaPeticion(url, this);
            Integer cantidad=json.length();
            if (cantidad==1){}else {
                Toast.makeText(this, "Conexion exitosa con "+msj, Toast.LENGTH_LONG).show();
            }

            editIp.setText("");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_prestamo000,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_prestamo001:
                Intent intentMeP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentMeP);
        }
        return super.onOptionsItemSelected(item);
    }

}
