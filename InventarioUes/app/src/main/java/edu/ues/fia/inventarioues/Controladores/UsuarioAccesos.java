package edu.ues.fia.inventarioues.Controladores;

public class UsuarioAccesos {

    // idUsuarioAccesos, idUsuario, tMarca, tTipoEquipo, tEquipo, tEditorial
    //tDocumento, tAutor, tMateria, tTipoMovimiento, tMotivo,tTipoAutor,tTipoDocumento
    // tUnidadAdmin, tIdioma, tDocente tHorario, tPrestamo
    Integer idUsuarioAccesos, idUsuario, tMarca, tTipoEquipo, tEquipo, tEditorial,
            tDocumento, tAutor, tMateria, tTipoMovimiento, tMotivo,tTipoAutor,tTipoDocumento;
    Integer tUnidadAdmin, tIdioma, tDocente, tHorario, tPrestamo;

    public UsuarioAccesos() {
    }

    public UsuarioAccesos(Integer idUsuarioAccesos, Integer idUsuario, Integer tMarca, Integer tTipoEquipo, Integer tEquipo, Integer tEditorial, Integer tDocumento, Integer tAutor, Integer tMateria, Integer tTipoMovimiento, Integer tMotivo, Integer tTipoAutor, Integer tTipoDocumento, Integer tUnidadAdmin, Integer tIdioma, Integer tDocente, Integer tHorario, Integer tPrestamo) {
        this.idUsuarioAccesos = idUsuarioAccesos;
        this.idUsuario = idUsuario;
        this.tMarca = tMarca;
        this.tTipoEquipo = tTipoEquipo;
        this.tEquipo = tEquipo;
        this.tEditorial = tEditorial;
        this.tDocumento = tDocumento;
        this.tAutor = tAutor;
        this.tMateria = tMateria;
        this.tTipoMovimiento = tTipoMovimiento;
        this.tMotivo = tMotivo;
        this.tTipoAutor = tTipoAutor;
        this.tTipoDocumento = tTipoDocumento;
        this.tUnidadAdmin = tUnidadAdmin;
        this.tIdioma = tIdioma;
        this.tDocente = tDocente;
        this.tHorario = tHorario;
        this.tPrestamo = tPrestamo;
    }

    public Integer getIdUsuarioAccesos() {
        return idUsuarioAccesos;
    }

    public void setIdUsuarioAccesos(Integer idUsuarioAccesos) {
        this.idUsuarioAccesos = idUsuarioAccesos;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer gettMarca() {
        return tMarca;
    }

    public void settMarca(Integer tMarca) {
        this.tMarca = tMarca;
    }

    public Integer gettTipoEquipo() {
        return tTipoEquipo;
    }

    public void settTipoEquipo(Integer tTipoEquipo) {
        this.tTipoEquipo = tTipoEquipo;
    }

    public Integer gettEquipo() {
        return tEquipo;
    }

    public void settEquipo(Integer tEquipo) {
        this.tEquipo = tEquipo;
    }

    public Integer gettEditorial() {
        return tEditorial;
    }

    public void settEditorial(Integer tEditorial) {
        this.tEditorial = tEditorial;
    }

    public Integer gettDocumento() {
        return tDocumento;
    }

    public void settDocumento(Integer tDocumento) {
        this.tDocumento = tDocumento;
    }

    public Integer gettAutor() {
        return tAutor;
    }

    public void settAutor(Integer tAutor) {
        this.tAutor = tAutor;
    }

    public Integer gettMateria() {
        return tMateria;
    }

    public void settMateria(Integer tMateria) {
        this.tMateria = tMateria;
    }

    public Integer gettTipoMovimiento() {
        return tTipoMovimiento;
    }

    public void settTipoMovimiento(Integer tTipoMovimiento) {
        this.tTipoMovimiento = tTipoMovimiento;
    }

    public Integer gettMotivo() {
        return tMotivo;
    }

    public void settMotivo(Integer tMotivo) {
        this.tMotivo = tMotivo;
    }

    public Integer gettTipoAutor() {
        return tTipoAutor;
    }

    public void settTipoAutor(Integer tTipoAutor) {
        this.tTipoAutor = tTipoAutor;
    }

    public Integer gettTipoDocumento() {
        return tTipoDocumento;
    }

    public void settTipoDocumento(Integer tTipoDocumento) {
        this.tTipoDocumento = tTipoDocumento;
    }

    public Integer gettUnidadAdmin() {
        return tUnidadAdmin;
    }

    public void settUnidadAdmin(Integer tUnidadAdmin) {
        this.tUnidadAdmin = tUnidadAdmin;
    }

    public Integer gettIdioma() {
        return tIdioma;
    }

    public void settIdioma(Integer tIdioma) {
        this.tIdioma = tIdioma;
    }

    public Integer gettDocente() {
        return tDocente;
    }

    public void settDocente(Integer tDocente) {
        this.tDocente = tDocente;
    }

    public Integer gettHorario() {
        return tHorario;
    }

    public void settHorario(Integer tHorario) {
        this.tHorario = tHorario;
    }

    public Integer gettPrestamo() {
        return tPrestamo;
    }

    public void settPrestamo(Integer tPrestamo) {
        this.tPrestamo = tPrestamo;
    }
}
