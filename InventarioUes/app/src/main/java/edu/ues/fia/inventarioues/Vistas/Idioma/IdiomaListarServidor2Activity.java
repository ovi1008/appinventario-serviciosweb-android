package edu.ues.fia.inventarioues.Vistas.Idioma;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import edu.ues.fia.inventarioues.Adaptador.IdiomaAdaptador;
import edu.ues.fia.inventarioues.Adaptador.MarcaAdaptador;
import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasL;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Idioma;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Marca.MarcaListarServidorActivity;

@SuppressLint("NewApi")
public class IdiomaListarServidor2Activity extends AppCompatActivity {

    EditText editFecha;
    ListView listIdioma;
    NumberPicker horaMinima, minutoMinimo, segundoMinimo;
    String h="0",m="0",s="0";
    String CERO = "0", DOS_PUNTOS = ":";
    Calendar calendar=Calendar.getInstance();
    List<Idioma> listadoIdioma=new ArrayList<>();
    List<Idioma> vaciar=new ArrayList<>();

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_idioma_listar_servidor2);
        editFecha=findViewById(R.id.IdiomaServidorListarFecha);
        horaMinima=findViewById(R.id.IdiomaServidorListarHoraMinima);
        minutoMinimo=findViewById(R.id.IdiomaServidorListarMinutoMinima);
        segundoMinimo=findViewById(R.id.IdiomaServidorListaSegundoMinima);

        listIdioma=findViewById(R.id.listIdiomaServidor);

        editFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(IdiomaListarServidor2Activity.this,fechaMinima, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //Hora
        horaMinima.setMinValue(0);
        horaMinima.setMaxValue(23);
        horaMinima.setWrapSelectorWheel(true);
        horaMinima.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                h =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });

        //Minuto
        minutoMinimo.setMinValue(0);
        minutoMinimo.setMaxValue(59);
        minutoMinimo.setWrapSelectorWheel(true);
        minutoMinimo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                m =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });
        //Segundo
        segundoMinimo.setMinValue(0);
        segundoMinimo.setMaxValue(59);
        segundoMinimo.setWrapSelectorWheel(true);
        segundoMinimo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                s =  (newVal < 10)? String.valueOf(CERO + newVal) : String.valueOf(newVal);
            }
        });

    }


    DatePickerDialog.OnDateSetListener fechaMinima = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            String formatoFecha = "yyyy-MM-dd";
            SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.getDefault());
            editFecha.setText(format.format(calendar.getTime()));
        }
    };

    public void consultarIdiomaPorFechaServidor(View v){
        listadoIdioma.removeAll(vaciar);
        String tiempoFormateado="%20"+h+":"+m+":"+s;
        TablasL helper = TablasL.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editFecha.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.idiomaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/idioma/ws_idioma_queryDate.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/idioma/ws_idioma_queryDate.php?";

                String url = url1+ip+url3+
                        "fechaModificadaIdioma="+editFecha.getText().toString()+tiempoFormateado;
                String json= ServiciosWeb.consultarFechaLocal(url,this);
                if (json.length()==0){}else{
                    String[]datosIdioma=json.split("&&");
                    for (String dato:datosIdioma){
                        String []datoP=dato.split(",");
                        String []idIdioma=datoP[0].split(":");
                        String []nombreIdioma=datoP[1].split(":");
                        Idioma idioma=new Idioma();
                        //camposIdioma= new String[]{"idIdioma", "nombreIdioma"}
                        idioma.setCodIdioma(Integer.valueOf(idIdioma[1]));
                        idioma.setNombreIdioma(nombreIdioma[1]);
                        listadoIdioma.add(idioma);
                        vaciar.add(idioma);
                    }
                    IdiomaAdaptador idiomaAdaptador=new IdiomaAdaptador(this,listadoIdioma);
                    listIdioma.setAdapter(idiomaAdaptador);
                }
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_idioma,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_idioma_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_idioma_menu_insertar:
                Intent intent0= new Intent(this, IdiomaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_idioma_menu_consultar:
                Intent intent1= new Intent(this, IdiomaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_idioma_menu_Editar:
                Intent intente= new Intent(this, IdiomaEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_idioma_menu_eliminar:
                Intent intent2= new Intent(this, IdiomaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_idioma_listar:
                Intent intent3= new Intent(this, IdiomaMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_idioma_listarServidor:
                Intent intent4= new Intent(this, IdiomaListarServidor2Activity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_idioma_listarSincronizar:
                Intent intent5= new Intent(this, IdiomaSincronizar2Activity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
