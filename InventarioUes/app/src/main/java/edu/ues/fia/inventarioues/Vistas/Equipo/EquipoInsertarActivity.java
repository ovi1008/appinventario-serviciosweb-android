package edu.ues.fia.inventarioues.Vistas.Equipo;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.R;
import edu.ues.fia.inventarioues.Vistas.Excel.ExcelExportActivity;
import edu.ues.fia.inventarioues.Vistas.Excel.ExcelPrincipalActivity;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class EquipoInsertarActivity extends AppCompatActivity {

    EditText editSerial, editDescripcion, editFechaIngreso, editFechaInactividad;
    Calendar calendar=Calendar.getInstance();
    Date fecha1=calendar.getTime();
    Spinner spMarca, spTipo;
    Integer valorSelectMarca, valorSelectTipo, valorSelectOpcion=null;
    RadioButton opcion1, opcion2;
    RadioGroup grupo;
    ImageView previewImg;
    private static final String carpetaPrincipal="/Backup/";
    private static final String carpetaImagen="media";
    private static final String directoriImagen=carpetaPrincipal+carpetaImagen;
    private String path;
    Bitmap bitmapFoto=null;
    private final int foto_code=100;
    private final int select_foto=200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT>=23){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},2);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_insertar);
        TablasJ helper= TablasJ.getInstance(getApplicationContext());
        editSerial=(EditText)findViewById(R.id.EquipoInsertarSerial);
        grupo=(RadioGroup)findViewById(R.id.EquipoInsertarEstadoGrupo);
        spMarca=(Spinner) findViewById(R.id.EquipoInsertarMarcaId);
        spTipo=(Spinner) findViewById(R.id.EquipoInsertarTipoId);
        editDescripcion=(EditText)findViewById(R.id.EquipoInsertarDescripcion);
        opcion1=(RadioButton)findViewById(R.id.EquipoInsertarEstadoOpcion1);
        opcion2=(RadioButton)findViewById(R.id.EquipoInsertarEstadoOpcion2);
        editFechaIngreso=(EditText)findViewById(R.id.EquipoInsertarFechaIngreso);
        editFechaInactividad=(EditText)findViewById(R.id.EquipoInsertarFechaInactividad);
        previewImg=findViewById(R.id.EquipoInsertarImg);

        File file = new File(directoriImagen);
        if (!file.exists()) {
            Log.v("File Created", String.valueOf(file.mkdirs()));
        }

        Cursor marca=helper.obtenerMarcas();
        final List<Integer> marcaIdList = new ArrayList<>();
        marcaIdList.add(0);
        List<String> marcaNombreList = new ArrayList<>();
        marcaNombreList.add(getResources().getString(R.string.equipoCampo2s));
        while (marca.moveToNext()){
            marcaIdList.add(marca.getInt(0));
            marcaNombreList.add(marca.getString(1));
        }
        spMarca.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,marcaIdList));
        spMarca.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,marcaNombreList));
        spMarca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectMarca=null;
                }else {
                    valorSelectMarca=marcaIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectMarca=null;

            }
        });

        Cursor tipo=helper.obtenerTiposEquipo();
        final List<Integer> tipoIdList = new ArrayList<>();
        tipoIdList.add(0);
        List<String> tipoNombreList = new ArrayList<>();
        tipoNombreList.add(getResources().getString(R.string.equipoCampo3s));
        while (tipo.moveToNext()){
            tipoIdList.add(tipo.getInt(0));
            tipoNombreList.add(tipo.getString(1));
        }
        spTipo.setAdapter(new ArrayAdapter<Integer>(this,android.R.layout.simple_dropdown_item_1line,tipoIdList));
        spTipo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,tipoNombreList));
        spTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    valorSelectTipo=null;
                }else {
                    valorSelectTipo=tipoIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                valorSelectTipo=null;

            }
        });

        opcion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean marcado =((RadioButton) v).isChecked();
                if (marcado){
                    valorSelectOpcion=1;
                }
            }
        });

        opcion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean marcado =((RadioButton) v).isChecked();
                if (marcado){
                    valorSelectOpcion=0;
                }
            }
        });



        editFechaIngreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EquipoInsertarActivity.this,fechaIngreso, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        editFechaInactividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EquipoInsertarActivity.this,fechaInactiva, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }


    DatePickerDialog.OnDateSetListener fechaIngreso = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            fecha1=calendar.getTime();
            actualizarFechaIngreso();
        }
    };

    private void actualizarFechaIngreso(){
        String formatoFecha = "dd/MM/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.getDefault());
        editFechaIngreso.setText(format.format(calendar.getTime()));
    }

    DatePickerDialog.OnDateSetListener fechaInactiva = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DATE,dayOfMonth);
            Date fecha2=calendar.getTime();
            if(fecha1.compareTo(fecha2)<=0){
                actualizarFechaInactiva();
            }
        }
    };

    private void actualizarFechaInactiva(){
        String formatoFecha = "dd/MM/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(formatoFecha, Locale.US);
        editFechaInactividad.setText(format.format(calendar.getTime()));
    }

    public void EquipoIngresar(View v){
        String serial,descripcion, ingreso, inactividad, mensaje;
        Integer marca, tipo, estado;
        serial=editSerial.getText().toString().toUpperCase();
        marca=valorSelectMarca;
        tipo=valorSelectTipo;
        descripcion=editDescripcion.getText().toString().toUpperCase();
        estado=valorSelectOpcion;
        ingreso=editFechaIngreso.getText().toString();
        inactividad=editFechaInactividad.getText().toString();
        if (serial.isEmpty()|| !(marca instanceof Integer) || !(tipo instanceof Integer) || descripcion.isEmpty() || !(estado instanceof Integer) | ingreso.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.equipoMsj1), Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper = TablasJ.getInstance(getApplicationContext());
            Equipo equipo = new Equipo();
            equipo.setSerialEquipo(serial);
            equipo.setIdMarca(marca);
            equipo.setIdTipoEquipo(tipo);
            equipo.setDescripcionEquipo(descripcion);
            equipo.setEstadoEquipo(estado);
            equipo.setFechaEquipo(ingreso);
            equipo.setFechaInactivoEquipo(inactividad);
            equipo.setPrestamo(1);
            if (bitmapFoto==null){}else {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapFoto.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] bitmapdata = stream.toByteArray();
                equipo.setFotoEquipo(bitmapdata);
            }
            helper.open();
            mensaje=helper.insertarEquipo(equipo);
            helper.close();
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();

            editSerial.setText("");
            spMarca.setSelection(0);
            spTipo.setSelection(0);
            editDescripcion.setText("");
            grupo.clearCheck();
            editFechaIngreso.setText("");
            editFechaInactividad.setText("");
            previewImg.setImageDrawable(null);
            bitmapFoto=null;

        }
    }

    public void botonImagen(View v){
        final CharSequence[] options={"Tomar Foto", "Elegir de Galeria", "Cancelar"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(EquipoInsertarActivity.this);
        builder.setTitle("Elige una opcion: ");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (options[which].equals("Tomar Foto")){
                    abrirCamara();
                }else if(options[which].equals("Elegir de Galeria")){
                    Intent intentG=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intentG.setType("image/*");
                    startActivityForResult(intentG.createChooser(intentG,"Selecciona una app de Imagen"),select_foto);
                }else if(options[which].equals("Cancelar")){
                    dialog.dismiss();
                }

            }
        });
        android.app.AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }
    public void abrirCamara(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            photoFile = createImageFile();
            if (photoFile != null) {
                path=photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(EquipoInsertarActivity.this,
                        "edu.ues.fia.inventarioues.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, foto_code);
            }
        }

    }

    private File createImageFile() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=null;
        try {
            image=File.createTempFile(timeStamp,".jpg",storageDir);

        }catch (IOException e){
            Log.d("my log","Excep: "+e.toString());
        }
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case foto_code:
                if (resultCode==RESULT_OK){
                    galleryAddPic();
                    setPic();
                }
                break;
            case select_foto:
                if (resultCode==RESULT_OK){
                    Uri path=data.getData();
                    previewImg.setImageURI(path);
                    previewImg.buildDrawingCache();
                    bitmapFoto=previewImg.getDrawingCache();
                }
                break;
        }
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = previewImg.getWidth();
        int targetH = previewImg.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        bitmapFoto = BitmapFactory.decodeFile(path, bmOptions);
        previewImg.setImageBitmap(bitmapFoto);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_equipo,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_equipo_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;


            case R.id.action_equipo_menu_insertar:
                Intent intent0= new Intent(this, EquipoInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_equipo_menu_consultar:
                Intent intent1= new Intent(this, EquipoConsultarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_equipo_menu_Editar:
                Intent intente= new Intent(this, EquipoEditarActivity.class);
                this.startActivity(intente);
                return true;


            case R.id.action_equipo_menu_eliminar:
                Intent intent2= new Intent(this, EquipoEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_equipo_listar:
                Intent intent3= new Intent(this, EquipoMenuActivity.class);
                this.startActivity(intent3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
