package edu.ues.fia.inventarioues.Adaptador;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.Modelos.Equipo;
import edu.ues.fia.inventarioues.R;

public class EquipoAdaptador extends ArrayAdapter<Equipo> {

    public EquipoAdaptador(@NonNull Context context, @NonNull List<Equipo> objects){
        super(context,0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable  View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(null==convertView){
            convertView=inflater.inflate(R.layout.listado_personalizado_equipo,parent,false);
        }
        TextView serialEquipo=convertView.findViewById(R.id.equipoSerialListar);
        TextView marcaEquipo=convertView.findViewById(R.id.equipoMarcaListar);
        marcaEquipo.setText("");
        TextView tipoEquipo=convertView.findViewById(R.id.equipoTipoListar);
        tipoEquipo.setText("");

        TablasJ helper = TablasJ.getInstance(getContext());

        Equipo equipo=getItem(position);
        serialEquipo.setText(equipo.getSerialEquipo());
        helper.open();

        Cursor c=helper.obtenerMarcas();
        while (c.moveToNext() && (marcaEquipo.getText().toString().isEmpty())){
            if (c.getInt(0)==equipo.getIdMarca()){
                marcaEquipo.setText(c.getString(1));
            }
        }

        Cursor c1=helper.obtenerTiposEquipo();
        while (c1.moveToNext() && (tipoEquipo.getText().toString().isEmpty())){
            if (c1.getInt(0)==equipo.getIdTipoEquipo()){
                tipoEquipo.setText(c1.getString(1));
            }
        }

        helper.close();

        return convertView;
    }
}
