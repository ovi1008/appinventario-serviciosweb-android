package edu.ues.fia.inventarioues.Vistas.Marca;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.inventarioues.Controladores.ServiciosWeb;
import edu.ues.fia.inventarioues.Controladores.TablasJ;
import edu.ues.fia.inventarioues.MenuPrincipalActivity;
import edu.ues.fia.inventarioues.Modelos.Marca;
import edu.ues.fia.inventarioues.R;

@SuppressLint("NewApi")
public class MarcaEditarActivity extends AppCompatActivity {

    EditText editId, editNombre, editBusca, editNombreServer;
    String clave="";
    int opcion=0;
    Button busca, buscaServer;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marca_editar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        editId = (EditText) findViewById(R.id.MarcaEditarId);
        editNombre = (EditText) findViewById(R.id.MarcaEditarNombre);
        editBusca=(EditText) findViewById(R.id.MarcaEditarSearch);
        busca=(Button)findViewById(R.id.MarcaEditarBtnConsultar);
        buscaServer=findViewById(R.id.MarcaEditarBusBtnServer);
        editNombreServer=findViewById(R.id.MarcaEditarBusqNombreServidor);
        Bundle bundle=getIntent().getExtras();
        if (bundle==null){
            editId.setEnabled(false);
            editNombre.setEnabled(false);
        }else {
            editId.setText(bundle.getString("id"));
            editNombre.setText(bundle.getString("nombre"));
            editNombreServer.setText(bundle.getString("nombre"));
            clave=bundle.getString("nombre");
            opcion=1;
            editBusca.setVisibility(View.INVISIBLE);
            busca.setVisibility(View.INVISIBLE);
            editNombreServer.setVisibility(View.INVISIBLE);
            buscaServer.setVisibility(View.INVISIBLE);

        }
    }

    public void buscarMarcaServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        if (ip.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.IpMsj), Toast.LENGTH_LONG).show();
        }
        else {
            if (editNombreServer.getText().toString().isEmpty()){
                Toast.makeText(this, getResources().getString(R.string.marcaMsj1), Toast.LENGTH_LONG).show();
            }else {
                String url1="http://";
                String url3="/inventarioUes/marca/ws_marca_query.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_query.php?";

                String url = url1+ip+url3+
                        "nombreMarca="+editNombreServer.getText().toString().toUpperCase();
                String jsonMarca= ServiciosWeb.consultarServidorLocal(url, this);
                if (jsonMarca==null){
                    Toast.makeText(this, getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
                }else{
                    String []datosMarca=jsonMarca.split(",");
                    String []idMarca=datosMarca[0].split(":");
                    String []nombreMarca=datosMarca[1].split(":");
                    Marca marcaS = new Marca();
                    marcaS.setIdMarca(Integer.valueOf(idMarca[1]));
                    marcaS.setNombreMarca(nombreMarca[1]);
                    editId.setText(String.valueOf(marcaS.getIdMarca()));
                    editNombre.setText(marcaS.getNombreMarca());
                    editNombre.setEnabled(true);
                    clave=editNombreServer.getText().toString().toUpperCase();
                    editBusca.setEnabled(false);
                    busca.setEnabled(false);
                    editNombreServer.setEnabled(false);
                    buscaServer.setEnabled(false);
                }


            }

        }
    }

    public void editarMarcaServidor(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        String ip=helper.ipDelServidor();
        helper.close();
        String url1="http://";
        String url3="/inventarioUes/marca/ws_marca_update.php?";
//            String urlLocal = "http://192.168.0.11/inventarioUes/marca/ws_marca_update.php?";

        String url = url1+ip+url3+
                "nombreMarca="+editNombreServer.getText().toString().toUpperCase()+
                "&nombreMarcaN="+editNombre.getText().toString().toUpperCase();
        String msj= ServiciosWeb.editarServidorLocal(url, this);
        Toast.makeText(this,msj+"la marca: "+editNombreServer.getText().toString().toUpperCase(),Toast.LENGTH_LONG).show();
        editId.setText("");
        editNombre.setText("");
        editBusca.setText("");
        clave="";
        editId.setEnabled(false);
        editNombre.setEnabled(false);
        busca.setEnabled(true);
        editBusca.setEnabled(true);
        editNombreServer.setText("");
        editNombreServer.setEnabled(true);
        buscaServer.setEnabled(true);


    }

    public void buscaMarcaEditar(View v){
        TablasJ helper = TablasJ.getInstance(getApplicationContext());
        helper.open();
        Marca marca=helper.marcaConsultar(editBusca.getText().toString().toUpperCase());
        helper.close();
        if (marca==null){
            Toast.makeText(this,getResources().getString(R.string.msj2Estad), Toast.LENGTH_LONG).show();
        }else {
            editId.setText(String.valueOf(marca.getIdMarca()));
            editNombre.setText(marca.getNombreMarca());
            editId.setEnabled(true);
            editNombre.setEnabled(true);
            clave=editBusca.getText().toString().toUpperCase();
            editBusca.setEnabled(false);
            busca.setEnabled(false);
            editNombreServer.setEnabled(false);
            buscaServer.setEnabled(false);
        }
    }


    public void actualizarMarca(View v){
        if (clave.isEmpty()){
            Toast.makeText(this,getResources().getString(R.string.msj3Estad),Toast.LENGTH_LONG).show();
        }else {
            TablasJ helper= TablasJ.getInstance(getApplicationContext());
            Marca marca = new Marca();
            marca.setIdMarca(Integer.valueOf(editId.getText().toString()));
            marca.setNombreMarca(editNombre.getText().toString().toUpperCase());
            helper.open();
            String mensaje=helper.actualizarMarca(marca,clave.toUpperCase());
            helper.close();
            if (opcion==1){
                Intent intOp1=new Intent(this,MarcaMenuActivity.class);
                this.startActivity(intOp1);
            }else {
                Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
                editId.setText("");
                editNombre.setText("");
                editBusca.setText("");
                clave="";
                editId.setEnabled(false);
                editNombre.setEnabled(false);
                busca.setEnabled(true);
                editBusca.setEnabled(true);
                editNombreServer.setText("");
                editNombreServer.setEnabled(true);
                buscaServer.setEnabled(true);
            }
        }


    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_marca,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_marca_menu_principal:
                Intent intentP= new Intent(this, MenuPrincipalActivity.class);
                this.startActivity(intentP);
                return true;

            case R.id.action_marca_menu_insertar:
                Intent intent0= new Intent(this, MarcaInsertarActivity.class);
                this.startActivity(intent0);
                return true;

            case R.id.action_marca_menu_consultar:
                Intent intent1= new Intent(this, MarcaBuscarActivity.class);
                this.startActivity(intent1);
                return true;

            case R.id.action_marca_menu_Editar:
                Intent intente= new Intent(this, MarcaEditarActivity.class);
                this.startActivity(intente);
                return true;

            case R.id.action_marca_menu_eliminar:
                Intent intent2= new Intent(this, MarcaEliminarActivity.class);
                this.startActivity(intent2);
                return true;

            case R.id.action_marca_listar:
                Intent intent3= new Intent(this, MarcaMenuActivity.class);
                this.startActivity(intent3);
                return true;

            case R.id.action_marca_listarServidor:
                Intent intent4= new Intent(this, MarcaListarServidorActivity.class);
                this.startActivity(intent4);
                return true;

            case R.id.action_marca_listarSincronizar:
                Intent intent5= new Intent(this, MarcaSincronizacionActivity.class);
                this.startActivity(intent5);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
