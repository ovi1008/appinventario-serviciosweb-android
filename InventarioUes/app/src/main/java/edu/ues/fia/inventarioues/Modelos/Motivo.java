package edu.ues.fia.inventarioues.Modelos;

public class Motivo {

    int idMotivo;
    String nombreMotivo;

    public Motivo() {
    }

    public Motivo(int idMotivo, String nombreMotivo) {
        this.idMotivo = idMotivo;
        this.nombreMotivo = nombreMotivo;
    }

    public int getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(int idMotivo) {
        this.idMotivo = idMotivo;
    }

    public String getNombreMotivo() {
        return nombreMotivo;
    }

    public void setNombreMotivo(String nombreMotivo) {
        this.nombreMotivo = nombreMotivo;
    }
}
