BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "idioma" (
	"codIdioma"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreIdioma"	varchar(20) NOT NULL UNIQUE,
	"fechaModificadaIdioma"	timestamp NOT NULL
);
INSERT INTO "idioma" ("codIdioma","nombreIdioma","fechaModificadaIdioma") VALUES (1,'ESPANOL','2019-06-02 18:14:30');
INSERT INTO "idioma" ("codIdioma","nombreIdioma","fechaModificadaIdioma") VALUES (2,'INGLES','2019-06-02 18:14:32');
INSERT INTO "idioma" ("codIdioma","nombreIdioma","fechaModificadaIdioma") VALUES (3,'FRANCES','2019-06-02 18:14:35');
COMMIT;

ALTER TABLE idioma ADD fechaModificadaIdioma timestamp null
UPDATE idioma SET fechaModificadaidioma= CURRENT_TIMESTAMP


