BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "tipoDocumento" (
	"idTipoDocumento"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreTipoDocumento"	VARCHAR(20) NOT NULL UNIQUE,
	"fechaModificadaTipoDocumento"	timestamp
);
INSERT INTO "tipoDocumento" ("idTipoDocumento","nombreTipoDocumento","fechaModificadaTipoDocumento") VALUES (1,'TESIS','2019-06-02 18:14:30');
INSERT INTO "tipoDocumento" ("idTipoDocumento","nombreTipoDocumento","fechaModificadaTipoDocumento") VALUES (2,'LIBRO','2019-06-02 18:14:33');
COMMIT;


ALTER TABLE tipoDocumento ADD fechaModificadaTipoDocumento timestamp null
UPDATE tipoDocumento SET fechaModificadaTipoDocumento= '2019-06-02 18:14:30'
