BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "tipoEquipo" (
	"idTipoEquipo"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreTipoEquipo"	VARCHAR(20) NOT NULL UNIQUE,
	"fechaModificadaTipoEquipo"	timestamp
);
INSERT INTO "tipoEquipo" ("idTipoEquipo","nombreTipoEquipo","fechaModificadaTipoEquipo") VALUES (1,'LAPTOP','2019-06-02 18:14:30');
INSERT INTO "tipoEquipo" ("idTipoEquipo","nombreTipoEquipo","fechaModificadaTipoEquipo") VALUES (2,'CAÑON','2019-06-02 18:14:30');
COMMIT;


ALTER TABLE tipoEquipo ADD fechaModificadaTipoEquipo timestamp null
UPDATE tipoEquipo SET fechaModificadaTipoEquipo= '2019-06-02 18:14:30'
