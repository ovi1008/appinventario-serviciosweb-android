BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "marca" (
	"idMarca"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreMarca"	varchar(20) NOT NULL UNIQUE,
	"fechaModificadaMarca"	timestamp NOT NULL
);
INSERT INTO "marca" ("idMarca","nombreMarca","fechaModificadaMarca") VALUES (1,'DELL','2019-06-02 18:14:30');
INSERT INTO "marca" ("idMarca","nombreMarca","fechaModificadaMarca") VALUES (2,'HP','2019-06-02 18:14:32');
INSERT INTO "marca" ("idMarca","nombreMarca","fechaModificadaMarca") VALUES (3,'TOSHIBA','2019-06-02 18:14:35');
COMMIT;

ALTER TABLE marca ADD fechaModificadaMarca timestamp null
UPDATE marca SET fechaModificadaMarca= CURRENT_TIMESTAMP


