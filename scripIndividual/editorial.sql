BEGIN TRANSACTION;
CREATE TABLE "editorial" (
	"idEditorial"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"nombreEditorial"	varchar(20) NOT NULL UNIQUE,
    "fechaModificadaEditorial"	timestamp NOT NULL
)

INSERT INTO `editorial` (`idEditorial`,`nombreEditorial`,`fechaModificadaEditorial`) VALUES (1,'GREDOS','2019-06-02 18:14:30');
INSERT INTO `editorial` (`idEditorial`,`nombreEditorial`,`fechaModificadaEditorial`) VALUES (2,'NEVSKY','2019-06-02 18:14:32');
INSERT INTO `editorial` (`idEditorial`,`nombreEditorial`,`fechaModificadaEditorial`) VALUES (3,'LUMEN','2019-06-02 18:14:35');
COMMIT;

ALTER TABLE editorial ADD fechaModificadaEditorial timestamp null
UPDATE editorial SET fechaModificadaEditorial= CURRENT_TIMESTAMP


